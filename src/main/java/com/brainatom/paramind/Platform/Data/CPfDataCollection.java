package com.brainatom.paramind.Platform.Data;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class CPfDataCollection {


    public static byte[] setArraylistStringToByte(ArrayList<String> arrayListObj) {
        // write to byte array
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(baos);

        for (String element : arrayListObj) {
            try {
                out.writeUTF(element);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return baos.toByteArray();
    }

    public static ArrayList<String> setByteToArraylistString(byte[] bytes) throws IOException {
        ArrayList<String> arrayListStringRes = new ArrayList<>();

        ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
        DataInputStream in = new DataInputStream(bais);
        while (in.available() > 0) {
            arrayListStringRes.add(in.readUTF());
        }

        return arrayListStringRes;
    }

}
