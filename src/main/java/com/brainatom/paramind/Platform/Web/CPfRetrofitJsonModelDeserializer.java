package com.brainatom.paramind.Platform.Web;

import android.graphics.ColorSpace;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

public class CPfRetrofitJsonModelDeserializer implements JsonDeserializer<ColorSpace.Model> {
    @Override
    public ColorSpace.Model deserialize(JsonElement json, Type typeOfT,
                                        JsonDeserializationContext context)
                             throws JsonParseException {
        JsonObject jsonObject = json.getAsJsonObject();

        // get the "contents" field
        JsonElement contents = jsonObject.get("Contents");
        if (contents != null) {
            if (contents.isJsonArray()) { // if this is a list, parse as ListModel
                return context.deserialize(jsonObject,
                    CPfRetrofitJsonListModel.class);
            }
            if (contents.isJsonPrimitive()) { // json primitives are number, string, bool, character
                if (contents.getAsJsonPrimitive().isString()) {
                    return context.deserialize(jsonObject, CPfRetrofitJsonStringModel.class);
                }
            }
            // do the same for every other type you might have
        }
        return null;
    }
}


