package com.brainatom.paramind.Platform.Web;

import android.os.AsyncTask;
import android.util.Log;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.net.URL;

public class CPfWebAnalyzer{

    org.jsoup.nodes.Document mDoc = null;
    String mWebpageHtml = "";

    String  mBufUrl = "";
    String  mBufIconLink = "";
    URL     mBufUrlFull = null;

    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////

    public String setFetchIconFromUrl(String url) {
        mBufUrl = url;
        boolean res = getWebpageMethodV3ViaJsoup(mBufUrl);
        if (!res) {
            Log.d("UrlRead", "Failed to read url" + mBufUrl);
        }

        return getLinkOfShortcutIconOfUrl(mBufUrl);
        //setDownloadIconFromLink(mBufIconLink);
    }

    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////


    private boolean getWebpageMethodV3ViaJsoup(String url) {
        try {
            org.jsoup.nodes.Document doc = Jsoup.connect(url).get();

            mDoc = doc;
            mWebpageHtml = doc.html();
            mBufUrl = url;
            return true;

        } catch (IOException e) {
            mWebpageHtml = "";
            e.printStackTrace();
            return false;
        }
    }


    private String getLinkOfShortcutIconOfUrl(String url) {
        try {
            org.jsoup.nodes.Element elements = mDoc.head().select("link[rel=shortcut icon]").first();
            String strRefLink = elements.attr("href");

            mBufUrlFull = new URL(new URL(mBufUrl), strRefLink);
            mBufIconLink = mBufUrlFull.toString();

            return mBufIconLink;
        } catch (Exception e) {
            Log.d("Web", "Exception in getting shortcut");
            return "";
        }

    }

    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////

    private void setDownloadIconFromLink(String url) {
        return;
    }


    public Document getmDoc() {
        return mDoc;
    }

    public void setmDoc(Document mDoc) {
        this.mDoc = mDoc;
    }

    public String getmWebpageHtml() {
        return mWebpageHtml;
    }

    public void setmWebpageHtml(String mWebpageHtml) {
        this.mWebpageHtml = mWebpageHtml;
    }

    public String getmBufUrl() {
        return mBufUrl;
    }

    public void setmBufUrl(String mBufUrl) {
        this.mBufUrl = mBufUrl;
    }
}
