package com.brainatom.paramind.Platform.Web;

import com.google.gson.annotations.SerializedName;

// for when content is a String
public class CPfRetrofitJsonStringModel extends CPfRetrofitJsonModel {
    @SerializedName("Contents")
    String content;
}
