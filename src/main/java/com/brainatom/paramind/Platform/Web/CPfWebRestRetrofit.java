package com.brainatom.paramind.Platform.Web;

import com.brainatom.paramind.App.Network.IAppNetRestUserRequest;
import com.brainatom.paramind.App.Setting.CAppGlobalSetting;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CPfWebRestRetrofit {

    static private IAppNetRestUserRequest instance = null;

    static private String serAddr;
    static private String serPort;

    ////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////

    public CPfWebRestRetrofit(String serAddr, String serPort) {
        this.serAddr = serAddr;
        this.serPort = serPort;
    }

    public CPfWebRestRetrofit() {
    }

    public String getSerAddr() {
        return serAddr;
    }

    public void setSerAddr(String serAddr) {
        this.serAddr = serAddr;
    }

    public String getSerPort() {
        return serPort;
    }

    public void setSerPort(String serPort) {
        this.serPort = serPort;
    }

    ////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////

    static public IAppNetRestUserRequest getInstance() {
        if(instance == null) {
            instance = setBuildDefaultRetrofitObject();
        }
        return instance;
    }

    static public IAppNetRestUserRequest setBuildDefaultRetrofitObject() {
        Gson gson = new GsonBuilder().serializeNulls().create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://" + serAddr + ":" + serPort + "/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        return retrofit.create(IAppNetRestUserRequest.class);
    }

    static public IAppNetRestUserRequest setBuildDefaultRetrofitObjectV1() {
        Gson gson = new GsonBuilder().registerTypeAdapter(CPfRetrofitJsonModel.class, new CPfRetrofitJsonModelDeserializer()).create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://" + serAddr + ":" + serPort + "/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        return retrofit.create(IAppNetRestUserRequest.class);
    }

    static public IAppNetRestUserRequest setBuildDefaultRetrofitObjectV2() throws IOException {
        OkHttpClient okHttpClient = setCreateHttpClientOfLogIntercepter();

        Gson gson = new GsonBuilder().registerTypeAdapter(CPfRetrofitJsonModel.class, new CPfRetrofitJsonModelDeserializer()).create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://" + serAddr + ":" + serPort + "/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(okHttpClient)
                .build();

        return retrofit.create(IAppNetRestUserRequest.class);
    }

    public static OkHttpClient setCreateHttpClientOfLogIntercepter() throws IOException {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        return new OkHttpClient.Builder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Chain chain) throws IOException {
                        Request originalRequest = chain.request();

                        Request newRequest = originalRequest.newBuilder()
                                .header("Interceptor-Header", "xyz")
                                .build();

                        return chain.proceed(newRequest);
                    }
                })
                .addInterceptor(loggingInterceptor)
                .build();
    }



}
