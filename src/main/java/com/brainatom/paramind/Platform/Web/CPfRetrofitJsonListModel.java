package com.brainatom.paramind.Platform.Web;

import com.google.gson.annotations.SerializedName;

import java.util.List;

// for when content is a list
public class CPfRetrofitJsonListModel extends CPfRetrofitJsonModel {
    @SerializedName("Contents")
    List<String> content;
}
