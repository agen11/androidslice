
package com.brainatom.paramind.Platform.Network;

import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.util.Log;

import com.brainatom.paramind.App.Setting.CAppGlobalSetting;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Reader;
import java.net.Socket;

public class CPfSocketClient extends AsyncTask<String, Void, Void> {
    private Exception mException;
    public int port;
    public String addr;

    byte[] mByte = new byte[1024];
    String mDataBuffer = "";

    Socket mSocket = null;

    //////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////

    public CPfSocketClient() {
    }


    //////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////

    @Override
    protected Void doInBackground(String... strings) {
        return setSendMsgToServer(strings[0]);
    }

    @Nullable
    public Void setSendMsgToServer(String string) {
        try {

            try {
                Socket socket = new Socket(addr, port);
                PrintWriter outToServer = new PrintWriter(
                    new OutputStreamWriter(socket.getOutputStream()));

                outToServer.write(string);
                outToServer.flush();

                boolean cont = true;
                while(socket.isConnected() || cont) {

                    int iByteToRead = socket.getInputStream().read(mByte);
                    if (iByteToRead > 0) {
                        mDataBuffer += new String(mByte, 0, iByteToRead);
                        Log.d("msg from server", mDataBuffer);
                    }
                    else {
                        cont = false;
                        break;
                    }
                }

                outToServer.close();
                socket.close();

            } catch (IOException e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            this.mException = e;
            return null;
        }
        return null;
    }

    //////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////

    public Void setSendMsgToServerNoWaiting(String string) {
        try {

            try {
                Socket socket = getInstance();
                PrintWriter outToServer = new PrintWriter(
                    new OutputStreamWriter(socket.getOutputStream()));

                outToServer.write(string);
                outToServer.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            this.mException = e;
            return null;
        }
        return null;
    }


     public Void setSendMsgToServerNoWaitingSingle(String string) {
        try {
            setSendMsgToServerNoWaiting(string);
            setClearSocket();
        } catch (Exception e) {
            this.mException = e;
            return null;
        }
        return null;
    }


    public String setRecvMsgFromServer() {
        try {
            Socket socket = getInstance();
            StringBuilder buffer = new StringBuilder();;
            char[] cbuf = new char[100000];

            for(int i=0; i < 10; i++) {
                final BufferedReader input = new BufferedReader(
                        new InputStreamReader(socket.getInputStream()));

                buffer.append(input.read(cbuf));
                //buffer.appeninput.readLine());
                Log.d("string build", Integer.toString(i));
            }

            return buffer.toString();

        } catch (Exception e) {
            return null;
        }
    }


    public String setRecvMsgFromServerV1() {
        try {
            Socket socket = getInstance();
            InputStream is = null;
            is = socket.getInputStream();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();

            int nRead;
            byte[] data = new byte[32768*32];

            while ((nRead = is.read(data, 0, data.length)) != -1) {
                baos.write(data, 0, nRead);
            }
            return new String(baos.toByteArray());

        } catch (Exception e) {
            return null;
        }
    }


    public String setRecvMsgFromServerV2() {
        try {
            Socket socket = getInstance();

            final int BUFFER_SIZE = 1024; // Or other reasonable value

            byte[] buffer = new byte[BUFFER_SIZE];
            int bytesRead = 0;

            // // assuming you got the InputStream as "input"
            // while ( (bytesRead = input.read(buffer)) > 0 ){ // -1 indicates EOF
            //     // read bytes are now in buffer[0..bytesRead-1]
            //     // analyse bytes to maybe add up multiple reads to a complete message.
            // }
            return new String("");

        } catch (Exception e) {
            return null;
        }
    }


    public String setRecvMsgFromServerV3() {
        String response = null;

        try {
            Socket socket = getInstance();

            InputStream inputStream = socket.getInputStream();
            int iAvailableBytes = inputStream.available();
            int lockSeconds = 10*1000;
            long lockThreadCheckpoint = System.currentTimeMillis();
            StringBuilder buffer1 = new StringBuilder();;

            while(iAvailableBytes < 0 || (System.currentTimeMillis() < lockThreadCheckpoint + lockSeconds)){
                try{Thread.sleep(10);}catch(InterruptedException ie){ie.printStackTrace();}
                iAvailableBytes = inputStream.available();
                if (iAvailableBytes > 0) {
                    byte[] buffer = new byte[iAvailableBytes];
                    inputStream.read(buffer, 0, iAvailableBytes);
                    buffer1.append(buffer);
                }
            }

            //response = new String(buffer);

            inputStream.close();


            //StringBuilder buffer = new StringBuilder();;

            //final BufferedReader input = new BufferedReader(
            //        new InputStreamReader(socket.getInputStream()));



            // boolean bContinueRead = true;
            // int j = 0;
            // while (bContinueRead || j <= 60) {
            //     String mRes = input.readLine();
            //     if (mRes == null) {
            //         bContinueRead = false;
            //         Log.d("null recv", Integer.toString(j));
            //     }
            //     else {
            //         buffer.append(mRes);
            //         j++;
            //         Log.d("new recv: ", mRes);
            //     }
            // }

            //buffer.append(input.readLine());

            // for( int i=0; i< 10; i++ ) {
            //     buffer.append(input.readLine());
            // }

            return buffer1.toString();

        } catch (Exception e) {
            return null;
        }
    }


    public String setRecvMsgFromServerV5() {
        String response = null;

        try {
            Socket socket = getInstance();

            InputStream inputStream = socket.getInputStream();
            Reader reader = new InputStreamReader(inputStream);

            int lockSeconds = 10*1000;
            long lockThreadCheckpoint = System.currentTimeMillis();
            StringBuilder buffer1 = new StringBuilder();;
            int c = 0;

            for(int data; (data = reader.read()) != -1;){
                char inchar = (char)data;
                buffer1.append(inchar);
                c++;

            }


            //response = new String(buffer);

            inputStream.close();

            return buffer1.toString();

        } catch (Exception e) {
            return null;
        }
    }


    public String setRecvMsgFromServerV6() {
        try {
            Socket socket = getInstance();


            boolean end = false;
            String dataString = "";
            DataInputStream in = new DataInputStream(socket.getInputStream());
            int bytesRead = 0;

            while(!end)
            {
                byte[] messageByte = new byte[1000000];
                bytesRead = in.read(messageByte);
                if (bytesRead <= 0) {
                    break;
                }

                dataString += new String(messageByte, 0, bytesRead);

                Log.d("Buf context:", Integer.toString(bytesRead) + dataString);
                //break;
                if (dataString.length() > 1000000)
                {
                    end = true;
                }
            }

            Log.d("Read is done", dataString);

            return dataString;

        } catch (Exception e) {
            Log.d("except:", "recv module");
            return null;
        }
    }

    public String setRecvMsgFromServerV7() {

        try {
            Socket socket = getInstance();


            return "";
        } catch (Exception e){
            return null;
        }
    }

    public void setCloseSocket() throws IOException {
        mSocket.close();
    }

    private Socket getInstance() throws IOException {
        if (mSocket == null) {
            mSocket = new Socket(addr, port);
        }
        return mSocket;
    }

    public void setClearSocket() throws IOException {
        if (this.mSocket != null) {
            if (this.mSocket.isConnected()) {
                this.mSocket.close();
            }
        }
        this.mSocket = null;
    }


    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }
}




