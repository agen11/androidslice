package com.brainatom.paramind.Platform.Type;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CPfString extends Object{

    //////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////

    public boolean ifContainSpace(String obj) {
        if (obj != null) {
            for (int i=0; i<obj.length(); i++) {
                if ( Character.isWhitespace(obj.charAt(i)) ) {
                    return true;
                }
            }

        }
        return false;
    }

    public boolean ifContainSpaceV1(String obj) {
        Pattern pattern = Pattern.compile("\\s");
        Matcher matcher = pattern.matcher(obj);
        return matcher.find();
    }

    public static boolean ifContainSpaceV2(String obj) {
        Pattern pattern = Pattern.compile("\\s");
        Matcher matcher = pattern.matcher(obj);
        return matcher.find();
    }

    public boolean ifSpaceOnly(String obj) {
        return obj.matches("^\\s*$");
    }

    public static boolean ifSpaceOnlyV2(String obj) {
        return obj.matches("^\\s*$");
    }

    public ArrayList setAddJoinedString(String joinedString) {
        return setAddJoinedStringByToken(joinedString, " ");
    }

    public ArrayList setAddJoinedStringByToken(String joinedString, String token) {
        String[] strArrParts = joinedString.split(token, -1);

        ArrayList<String> arrayListString = new ArrayList<>(strArrParts.length);
        for (String elem:strArrParts
                ) {
            if (elem.length() > 0) {
                arrayListString.add(elem);
            }
        }

        return arrayListString;
    }
}
