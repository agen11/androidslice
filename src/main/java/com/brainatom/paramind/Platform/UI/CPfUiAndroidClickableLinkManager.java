package com.brainatom.paramind.Platform.UI;

import android.annotation.SuppressLint;
import android.text.Spannable;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.brainatom.paramind.Platform.Type.CPfString;

import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.Locale;

public class CPfUiAndroidClickableLinkManager {
    private TextView mTvObj;


    private ArrayList<String> mStrArrObjText = new ArrayList<String>();

    ///////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////


    public CPfUiAndroidClickableLinkManager(TextView mTvObj) {
        this.mTvObj = mTvObj;
    }

    public CPfUiAndroidClickableLinkManager(TextView mTvObj, String text) {
        this.mTvObj = mTvObj;
        if(text.length()>0) {
            this.setAddJoinedString(text);
        }
        this.setProduceClickableLinks();
    }

    ///////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////
    public void setAddNewToLinks(String newword) {
        if (newword.length() == 0) {
            return;
        }
        setAddJoinedStringByToken(newword, " ");
        //setAddText(newword);
        setProduceClickableLinks();
    }

    public void setAddStringToLinkManager(String curEtFm) {
        CPfString mStr = new CPfString();

        if (mStr.ifContainSpace(curEtFm)) {
            ArrayList<String> lsString = mStr.setAddJoinedString(curEtFm);
            for (String elem: lsString) {
                setAddNewToLinks(elem);
            }
        }
        else {
            setAddNewToLinks(curEtFm);
        }
    }

    @SuppressLint("ResourceAsColor")
    public void setProduceClickableLinks() {
        if (mStrArrObjText.size() == 0) {
            mTvObj.setText("");
            return;
        }

        mTvObj.setMovementMethod(LinkMovementMethod.getInstance());
        String strJoin = TextUtils.join(" ", mStrArrObjText);
        mTvObj.setText(strJoin, TextView.BufferType.SPANNABLE);
        //mTvObj.setHighlightColor(R.color.ActiveGreen);

        Spannable spans = (Spannable) mTvObj.getText();

        BreakIterator iterator = BreakIterator.getWordInstance(Locale.US);
        iterator.setText(strJoin);

        int start = iterator.first();
        for (int end = iterator.next(); end != BreakIterator.DONE;
             start = end, end = iterator.next()) {

            String possibleWord = strJoin.substring(start, end);
            if (Character.isLetterOrDigit(possibleWord.charAt(0))) {
                ClickableSpan clickSpan = getClickableSpan(possibleWord);
                spans.setSpan(clickSpan, start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
        }
    }

    private ClickableSpan getClickableSpan(final String word) {
        return new ClickableSpan() {
            final String mWord;
            {
                mWord = word;
            }

            @Override
            public void onClick(View widget) {
                Log.d("tapped on:", mWord);
                Toast.makeText(widget.getContext(), mWord, Toast.LENGTH_SHORT).show();

                setRemoveText(mWord);
                setProduceClickableLinks();
            }

            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
            }
        };
    }


    //////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////

    public TextView getmTvObj() {
        return mTvObj;
    }

    public void setmTvObj(TextView mTvObj) {
        this.mTvObj = mTvObj;
    }

    public ArrayList<String> getArrListText() {
        return mStrArrObjText;
    }

    public void setAddJoinedString(String joinedString) {
        setAddJoinedStringByToken(joinedString, " ");
    }

    public void setAddJoinedStringByToken(String joinedString, String token) {
        String[] strArrParts = joinedString.split(token, -1);
        for (String elem:strArrParts
             ) {
            setAddText(elem);
        }
    }

    public void setAddText(String name) {
        mStrArrObjText.add(name);
    }

    public void setRemoveText(String name) {
        mStrArrObjText.remove(name);
    }

    public ArrayList<String> getmStrArrObjText() {
        return mStrArrObjText;
    }


}


