package com.brainatom.paramind.Platform.UI;

import android.content.Context;
import android.util.DisplayMetrics;
import android.util.TypedValue;

public class CPfUiDispUtils {

	private CPfUiDispUtils() throws InstantiationException {
		throw new InstantiationException("This class is not for instantiation");
	}

	public static int dipToPx(Context c,float dipValue) {
		DisplayMetrics metrics = c.getResources().getDisplayMetrics();
		return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dipValue, metrics);
	}
}
