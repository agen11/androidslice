
package com.brainatom.paramind.Platform.Time;

import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class CPfBaseTime {
    public static long getTimeStamp() {
        Long lTs = System.currentTimeMillis()/1000;
        return lTs;
    }

    public static String getTimeStampString() {
        Long lTs = System.currentTimeMillis()/1000;
        return lTs.toString();
    }

    public static String getCurrentTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        return sdf.format(new Date());
    }

    public static String setConvertTimeStampToDefaultFormat() {
        //Calendar calendar = Calendar.getInstance();
        //calendar.add(Calendar.MILLISECOND,
        //    TimeZone.getDefault().getOffset(calendar.getTimeInMillis()));

        //SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd HH:mm", Locale.getDefault());
        //Date currenTimeZone=new Date(getTimeStamp()*1000);
        //String mRes = sdf.toString();

        //Calendar cal = Calendar.getInstance(Locale.getDefault());
        //cal.setTimeInMillis(getTimeStamp() * 1000L);
        //String mRes = DateFormat.getDateTimeInstance("dd-MM-yyyy hh:mm", cal).toString();

        //return mRes;
        return "";
    }

    public static String setConvertTimeStampToDefaultFormatV1(long time) {
        Date date = new Date(time*1000L);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        sdf.setTimeZone(cal.getTimeZone());

        return sdf.format(date);
    }

    public static String setConvertTimeStampToDefaultFormatV3() {
        Date date = new Date(getTimeStamp()*1000L);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ");
        Calendar cal = Calendar.getInstance();
        sdf.setTimeZone(cal.getTimeZone());

        return sdf.format(date);
    }

    public static String setConvertTimeStampToDefaultFormatV2() {
        return setConvertTimeStampToDefaultFormatV1(getTimeStamp());
    }

    public static void setSleepForSecond(int iTimeInSecond) {
        if (iTimeInSecond <= 0) { return; }

        String timestamp = setConvertTimeStampToDefaultFormatV3();
        Log.d("Sleep", timestamp);
        try {
            TimeUnit.SECONDS.sleep(iTimeInSecond);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        timestamp = CPfBaseTime.setConvertTimeStampToDefaultFormatV3();
        Log.d("Sleep", timestamp);
    }


    public static void main(String[] args) {
        //setTestCreatingTime();

        setConvertTimeStampToDefaultFormatV2();

        //String mRes = setConvertTimeStampToDefaultFormat();
        //System.out.println(mRes);
    }

    //////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////

    private static void setTestCreatingTime() {
        Long lTs = System.currentTimeMillis()/1000;
        String ts = lTs.toString();
        System.out.println(ts);

        String timeStamp = String.valueOf(TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()));
        System.out.println(timeStamp);

        SimpleDateFormat s = new SimpleDateFormat("ddMMyyyyhhmmss");
        String format = s.format(new Date());
        System.out.println(format);

        s = new SimpleDateFormat("yyyy-MM-dd-hh:mm");
        format = s.format(new Date());
        System.out.println(format);


        Calendar calendar = Calendar.getInstance();
        TimeZone tz = TimeZone.getDefault();
        calendar.add(Calendar.MILLISECOND, tz.getOffset(calendar.getTimeInMillis()));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date currenTimeZone=new Date(lTs*1000);
        String res = currenTimeZone.toString();
        //Toast.makeText(TimeStampChkActivity.this, sdf.format(currenTimeZone), Toast.LENGTH_SHORT).show();
        System.out.println(format);


        // Calendar cal = Calendar.getInstance();
        // TimeZone tz = cal.getTimeZone();//get your local time zone.
        // SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
        // sdf.setTimeZone(tz);//set time zone.
        // String localTime = sdf.format(new Date(lTs) * 1000 );
        // Date date = new Date();
        // try {
        //     date = sdf.parse(localTime);//get local date
        // } catch (ParseException e) {
        //     e.printStackTrace();
        // }


        // Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        // cal.setTimeInMillis(Long.parseLong(timeStamp));
        // String date = DateFormat.format("dd-MM-yyyy", cal).toString();
    }

}
