package com.brainatom.paramind.Platform.File;

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class CPfFileOperationInternal {

    public void writeFile(String fn, String data, Context context) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput(fn, Context.MODE_PRIVATE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String readFile(String fn, Context context) throws IOException {
        String res = "";

        try {
            InputStream inputStream = context.openFileInput(fn);
            if (inputStream != null) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                String recvString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while( (recvString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append(recvString);
                }

                inputStream.close();
                res = stringBuilder.toString();
            }

        }
        catch (FileNotFoundException e) {
            Log.d("file operation", "file not found");
        }
        catch (IOException e) {
            Log.d("io operation", "can not read");
        }

        return res;
    }


}
