package com.brainatom.paramind.App.Setting;


public class CAppGlobalSetting {
    static private CAppSettingNetwork network = new CAppSettingNetwork();

    private static int      maxNrOfWordsInDisplay = 20;

    private static int      defaultMaxNrOfKeywordGroupInActivation = 3;
    private static int      defaultMaxNrOfSearchGroupInActivation = 3;



    /////////////////////////////////////////////////////

    public static CAppSettingNetwork getNetwork() {
        return network;
    }

    public static void setNetwork(CAppSettingNetwork network) {
        CAppGlobalSetting.network = network;
    }

    public static String getNetworkServerDefaultAddr() {
        return network.getDefaultServerAddr();
    }

    public static int getNetworkServerDefaultPort() {
        return network.getDefaultServerPort();
    }

    public static int getWebAppServerDefaultPort() {
        return network.getDefaultWebServerPort();
    }

    /////////////////////////////////////////////////////

    public static int getMaxNrOfWordsInDisplay() {
        return maxNrOfWordsInDisplay;
    }

    public static int getMaxNrOfActivatedKeywordGroup() {
        return defaultMaxNrOfKeywordGroupInActivation;
    }

    public static int getMaxNrOfActivatedSearchGroup() {
        return defaultMaxNrOfSearchGroupInActivation;
    }

}


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////


class CAppSettingNetwork {
    private String          defaultServerAddr = "192.168.3.100";
    private int             defaultServerPort = 9382;

    private int             defaultWebServerPort = 5000;

    public String getDefaultServerAddr() {
        return defaultServerAddr;
    }

    public void setDefaultServerAddr(String defaultServerAddr) {
        this.defaultServerAddr = defaultServerAddr;
    }

    public int getDefaultServerPort() {
        return defaultServerPort;
    }

    public void setDefaultServerPort(int defaultServerPort) {
        this.defaultServerPort = defaultServerPort;
    }

    public int getDefaultWebServerPort() {
        return defaultWebServerPort;
    }

    public void setDefaultWebServerPort(int defaultWebServerPort) {
        this.defaultWebServerPort = defaultWebServerPort;
    }
}



////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

class CStateMachineOfAppTransceiver {


}

