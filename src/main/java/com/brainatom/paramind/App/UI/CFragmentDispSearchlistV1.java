package com.brainatom.paramind.App.UI;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.brainatom.paramind.App.Database.CAppDbHandler;
import com.brainatom.paramind.App.Database.CAppDbNewSearchTargetGroup;
import com.brainatom.paramind.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CFragmentDispSearchlistV1 extends Fragment implements IAppFragRecvUser{

    CAppDbHandler mDbHdl = null;

    private RecyclerView mRecyView;
    private RecyclerView.Adapter mAdaptor;
    private ArrayList<CAppDbNewSearchTargetGroup> mStGroup = null;

    private CRecvDataAdapterSearchlistV1 mAdaptorPrac;
    private ArrayList<String> mArrUserSelectedSearchTargetGroupName = null;



    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_disp_searchlist_group_v1,
            container, false);

        setHasOptionsMenu(true);

        try {
            getSearchTargetGroupDataFromDB();
        } catch (IOException e) {
            e.printStackTrace();
        }



        mRecyView = view.findViewById(R.id.id_recvwDispSearchlistgroupV1);
        //mRecyView.setHasFixedSize(true);

        mRecyView.setLayoutManager(new LinearLayoutManager(
            this.getActivity().getBaseContext()));

        mAdaptorPrac = new CRecvDataAdapterSearchlistV1(mStGroup, getActivity());
        mAdaptor = mAdaptorPrac;
        mRecyView.setAdapter(mAdaptor);


        mArrUserSelectedSearchTargetGroupName = new ArrayList<>();
        // Toolbar toolbar = (Toolbar)getView().findViewById(R.id.id_searchtarget_group_item_remove);
        // toolbar.setOnMenuItemClickListener(this);



        return view;
    }



    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if ( menu != null ) {
            if ( !menu.hasVisibleItems() ) {
                getActivity().getMenuInflater().inflate(R.menu.menu_edit_listitem_searchtargetgroup, menu);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.id_searchtarget_group_item_remove:
                setCb_RemoveItemOfSearchtargetGroup();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        getDbHdlInst();

        setReloadData();
    }



    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////

    public boolean setReloadData() {
        try {
            getDataFromDB();
            mAdaptorPrac.setListItems(mStGroup);
            mAdaptorPrac.notifyDataSetChanged();
        } catch (IOException e) {}
        return true;
    }

    public void getDataFromDB() throws IOException {
        getDbHdlInst();
        mStGroup = mDbHdl.getAllDataItemsOfSearchTargetGroup();
        return;
    }

    public void getSearchTargetGroupDataFromDB() throws IOException {
        String strDb = "";

        getDbHdlInst();
        mStGroup = mDbHdl.getAllDataItemsOfSearchTargetGroup();

        return;
    }

    public void setCb_RemoveItemOfSearchtargetGroup() {
        List<CAppDbNewSearchTargetGroup> res = mAdaptorPrac.getmListItems();
        for(CAppDbNewSearchTargetGroup elem: res) {
            if(elem.isSelected) {
                mArrUserSelectedSearchTargetGroupName.add(elem.mSearchTargetGroupName);
            }
        }

        // delete it from Db
        getDbHdlInst();
        for (String item: mArrUserSelectedSearchTargetGroupName) {
            mDbHdl.setDeleteSearchTargetitemViaQueryViaName(item);
            for (CAppDbNewSearchTargetGroup elem: mStGroup) {
                if (elem.getSearchTargetGroupName().equals(item)) {
                    mStGroup.remove(elem);
                }

            }
        }

        mAdaptorPrac.notifyDataSetChanged();

    }


    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////

    private void getDbHdlInst() {
        if (mDbHdl == null) {
            //mDbHdl = new CAppDbHandler(getActivity(), null, null, 1 );
            mDbHdl = ((MainActivity)getActivity()).getDbInstance();
        }
        mDbHdl.getDbInstance();
    }



}
