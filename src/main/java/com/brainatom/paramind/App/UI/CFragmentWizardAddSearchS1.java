package com.brainatom.paramind.App.UI;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.brainatom.paramind.App.Database.CAppDbHandler;
import com.brainatom.paramind.App.Database.CAppDbNewKeywordsGroup;
import com.brainatom.paramind.R;

import java.io.IOException;
import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class CFragmentWizardAddSearchS1 extends Fragment {

    private ArrayList<CAppDbNewKeywordsGroup> mKwGroup = null;
    CAppDbHandler mDbHdl = null;

    public CFragmentWizardAddSearchS1() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_wizard_add_search_s1, container, false);
    }


    public ArrayList<CAppDbNewKeywordsGroup> getDataS1() throws IOException {
        getDataFromDB();
        return  mKwGroup;
    }


    public void getDataFromDB() throws IOException {
        getDbHdlInst();
        mKwGroup = mDbHdl.getAllDataItemsOfKeywordGroup();
        return;
    }


    private void getDbHdlInst() {
        if (mDbHdl == null) {
            //mDbHdl = new CAppDbHandler(getActivity(), null, null, 1 );
            mDbHdl = ((MainActivity)getActivity()).getDbInstance();
        }
        mDbHdl.getDbInstance();
    }

}
