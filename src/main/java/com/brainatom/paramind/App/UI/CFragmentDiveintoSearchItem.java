package com.brainatom.paramind.App.UI;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.brainatom.paramind.R;


public class CFragmentDiveintoSearchItem extends Fragment {

    String mUrl = "";
    String mItemContentInDetail = "";
    String mDateTime = "";

    WebView mWebView = null;

    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////

    public CFragmentDiveintoSearchItem() {
        // Required empty public constructor
    }


    @SuppressLint("ValidFragment")
    public CFragmentDiveintoSearchItem(String url, String itemContentInDetail, String dateTime) {
        mUrl = url;
        mItemContentInDetail = itemContentInDetail;
        mDateTime = dateTime;

    }


    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_diveinto_search_item, container, false);


        if (mUrl.length() > 0) {
            mWebView = (WebView)view.findViewById(R.id.id_wvDiveSearchItem);

            mWebView.setWebViewClient(new CWebviewClient());
            mWebView.getSettings().setLoadsImagesAutomatically(true);
            mWebView.getSettings().setJavaScriptEnabled(true);

            mWebView.loadUrl(mUrl);
        }


        TextView tvContent = (TextView)view.findViewById(R.id.id_tvDivContentDetail);
        tvContent.setText(mItemContentInDetail);

        TextView tvDate = (TextView)view.findViewById(R.id.id_tvDiveItemDatetime);
        tvDate.setText(mDateTime);

        TextView tvUrl = (TextView)view.findViewById(R.id.id_tvUrlDiveSearchItem);
        tvUrl.setText(mUrl);


        return view;
    }

    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////

    public void setNewItem(String url, String itemContentInDetail, String dateTime) {
        mUrl = url;
        mItemContentInDetail = itemContentInDetail;
        mDateTime = dateTime;
    }



    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////

    private class CWebviewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }




}
