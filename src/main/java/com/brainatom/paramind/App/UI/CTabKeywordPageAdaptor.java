package com.brainatom.paramind.App.UI;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class CTabKeywordPageAdaptor extends FragmentPagerAdapter {

    private final List<Fragment> mFragmentList = new ArrayList<>();
    private final List<String> mFragmentTitleList = new ArrayList<>();

    public CTabKeywordPageAdaptor(FragmentManager fm) {
        super(fm);
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mFragmentTitleList.get(position);
    }

    @Override
    public Fragment getItem(int position) {
        //Log.d("KeyA", "Pos:" + Integer.toString(mCurSelPosition) );
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        //Log.d("KeyA", "Count:" + Integer.toString(mFragmentList.size()));
        return mFragmentList.size();
    }

    public void setAddFragment(Fragment fragment, String title) {
        mFragmentList.add(fragment);
        mFragmentTitleList.add(title);
    }

}
