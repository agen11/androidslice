package com.brainatom.paramind.App.UI;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.brainatom.paramind.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class CFragmentWizardAddSearchS2 extends Fragment implements IPfUiData{

    View mView = null;
    EditText mEditText = null;

    String mSearchGroupName = "";

    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////


    public CFragmentWizardAddSearchS2() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_wizard_add_search_s2, container, false);
        mEditText = mView.findViewById(R.id.id_InputNameOfSearchGroupV1);

        return mView;
    }

    @Override
    public Object getData() {
        mSearchGroupName = mEditText.getText().toString();
        return (Object) mSearchGroupName;
    }

    public String getDataS2() {
        mSearchGroupName = mEditText.getText().toString();
        return mSearchGroupName;
    }
}
