


package com.brainatom.paramind.App.UI;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.brainatom.paramind.App.Database.CAppDbHandler;
import com.brainatom.paramind.App.Database.CAppDbNewKeywordsGroup;
import com.brainatom.paramind.App.Database.CAppDbNewSearchTargetGroup;
import com.brainatom.paramind.App.Network.CAppNetJsFullSearchSet;
import com.brainatom.paramind.App.Network.CAppNetJsKeywordGroup;
import com.brainatom.paramind.App.Network.CAppNetJsKeywordGroupList;
import com.brainatom.paramind.App.Network.CAppNetJsMsgReq;
import com.brainatom.paramind.App.Network.CAppNetJsTargetGroup;
import com.brainatom.paramind.App.Network.CAppNetJsTargetGroupList;
import com.brainatom.paramind.App.Network.CAppSocketClient;
import com.brainatom.paramind.App.Web.CAppWebTransceiver;
import com.brainatom.paramind.Platform.Network.CPfSocketClient;
import com.brainatom.paramind.Platform.Type.CPfString;
import com.brainatom.paramind.Platform.UI.CPfUiAndroidClickableLinkManager;
import com.brainatom.paramind.R;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class CFragmentManageSearchlist extends Fragment implements CFragmentSelectKeywordgroup.SetSendUserChoice{
    private EditText etUrl;
    private EditText etSearchGroupName;

    private TextView tvNewUrlToWatch;
    private TextView tvSelectedKeywordGroup;

    ArrayList<String> mArrStringObj = null;

    CPfUiAndroidClickableLinkManager mUrlLinkMng = null;
    CPfUiAndroidClickableLinkManager mSelectedKwGroup = null;
    CPfUiAndroidMsg mMsg = null;

    CPfString mStr = new CPfString();

    View mview;
    CAppDbHandler mDbHdl = null;


    /////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////

    public CFragmentManageSearchlist() {
        // Required empty public constructor
    }

    /////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (mview != null) {
            return mview;
        }

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_mng_add_searchlist, container, false);
        mview = view;

        etSearchGroupName = (EditText)view.findViewById(R.id.id_etNameOfUrlSearchGrup);
        etUrl = (EditText)view.findViewById(R.id.id_etInputUrl);

        tvNewUrlToWatch = (TextView)view.findViewById(R.id.id_tvUrlList);
        tvSelectedKeywordGroup = (TextView)view.findViewById(R.id.id_tvSelectedKeywordGroup);

        mUrlLinkMng = new CPfUiAndroidClickableLinkManager((TextView)view.findViewById(R.id.id_tvUrlList), "");
        mSelectedKwGroup = new CPfUiAndroidClickableLinkManager((TextView)view.findViewById(R.id.id_tvSelectedKeywordGroup), "");

        final Button butAddNewUrlToWatch = (Button)view.findViewById(R.id.id_ButAddNewUrl);
        final Button butSelectKeywordGroup = (Button)view.findViewById(R.id.id_ButSelectSearchGroup);
        final Button butConfirmAddUrlToMyInterest = (Button)view.findViewById(R.id.id_ButCreateMyInterest);
        final Button butShowMySearchTargetGroup = (Button)view.findViewById(R.id.id_ButSeemyUrllist);
        final Button butStartMyEngine = (Button)view.findViewById(R.id.id_ButStartingEngine);

        butAddNewUrlToWatch.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        setAddNewUrlToWatch(v);
                    }
                }
        );
        butSelectKeywordGroup.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        setSelectKeywordGroup(v);
                    }
                }
        );
        butConfirmAddUrlToMyInterest.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            setAddNewSearchTarget(v);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
        );
        butShowMySearchTargetGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSeeMySearchTargetGroup(v);
            }
        });
        butStartMyEngine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    setStartMyEngine(v);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });


        return view;
    }


    @Override
    public void setDeliverUserChoice(ArrayList<String> arrString) {
        mArrStringObj = arrString;
        for (String elem: arrString) {
            mSelectedKwGroup.setAddNewToLinks(elem);
            //setAddStringToLinkManager(elem, mSelectedKwGroup);
        }
    }


    /////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////
    public void setSelectKeywordGroup() {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        CFragmentSelectKeywordgroup fragmentSelectKeywordgroup = new CFragmentSelectKeywordgroup();
        fragmentSelectKeywordgroup.setReferenceToSendUserChoice(this);
        ft.replace(R.id.id_frameOfMainActivity, fragmentSelectKeywordgroup);
        ft.addToBackStack(null);
        ft.commit();

    }



    /////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////


    private void setStartMyEngine(View v) throws IOException {
        CAppDbHandler dbHdl = new CAppDbHandler(getActivity(), null, null, 1);
        ArrayList<CAppDbNewKeywordsGroup> kwGroup = dbHdl.getAllDataItemsOfKeywordGroup();
        ArrayList<CAppDbNewSearchTargetGroup> targetGroup = dbHdl.getAllDataItemsOfSearchTargetGroup();

        // if need to update it
        //setSendMsgUpdateAllInterests(targetGroup, kwGroup);


        //CAppWebTransceiver mWebAct = new CAppWebTransceiver("https://en.wikipedia.org/wiki/Machine_learning");
        CAppWebTransceiver mWebAct = new CAppWebTransceiver(getContext(), targetGroup, kwGroup, 2);
        mWebAct.execute("");


        Toast.makeText(this.getContext(), "Your search engine is working now!", Toast.LENGTH_SHORT).show();
    }

    private void setSendMsgUpdateAllInterests(ArrayList<CAppDbNewSearchTargetGroup> targetGroup, ArrayList<CAppDbNewKeywordsGroup> kwGroup) {
        CAppNetJsFullSearchSet fss = getObjectiveTargetObj(kwGroup, targetGroup);

        Gson gson = new Gson();
        String msg = setBuildTxMsg(gson, gson.toJson(fss));

        CAppSocketClient socketClient = new CAppSocketClient();
        socketClient.execute(msg);
    }

    private String setBuildTxMsg(Gson gson, String strStgJson) {
        CAppNetJsMsgReq msg = new CAppNetJsMsgReq("0.1", "UsrObj", strStgJson, "Testor");
        return gson.toJson(msg);
    }

    @NonNull
    private CAppNetJsFullSearchSet getObjectiveTargetObj(ArrayList<CAppDbNewKeywordsGroup> kwGroup, ArrayList<CAppDbNewSearchTargetGroup> targetGroup) {
        CAppNetJsKeywordGroupList kwgl = new CAppNetJsKeywordGroupList();

        for(CAppDbNewKeywordsGroup elem: kwGroup) {
            String gname = elem.mKeywordGroupName;
            List<String> fkwl = elem.mFullmatchKeywords;
            List<String> pkwl = elem.mPartmatchKeywords;

            kwgl.getArrKeywordGroup().add(new CAppNetJsKeywordGroup(gname, fkwl, pkwl));
            //kwgl.mArrKeywordGroup.add(new CAppNetJsKeywordGroup(gname, fkwl, pkwl));
        }
        //String strKwgJson = gson.toJson(kwgl);


        CAppNetJsTargetGroupList stgl = new CAppNetJsTargetGroupList();

        for(CAppDbNewSearchTargetGroup elem: targetGroup) {
            String gname = elem.mSearchTargetGroupName;
            List<String> urltarget = elem.mSearchUrlTarget;
            List<String> kwg = elem.mLinkedinKeywordsGroupId;

            stgl.getArrTargetGroup().add(new CAppNetJsTargetGroup(gname, urltarget, kwg));
            //stgl.mArrTargetGroup.add(new CAppNetJsTargetGroup(gname, urltarget, kwg));
        }

        return new CAppNetJsFullSearchSet(kwgl, stgl);
    }

    private void setSeeMySearchTargetGroup(View v) {
        try {
            setSwitchFragment();
        } catch (Exception e) {}

        Toast.makeText(this.getContext(), "See all my search target!", Toast.LENGTH_SHORT).show();
    }

    private void setSwitchFragment() {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.id_frameOfMainActivity, new CFragmentDispSearchlist());
        ft.addToBackStack(null);
        ft.commit();
    }

    private void setAddNewSearchTarget(View v) throws IOException {
        setCreateSearchTargetGroup();
        Toast.makeText(this.getContext(), "Add new search target!", Toast.LENGTH_SHORT).show();
    }

    private void setCreateSearchTargetGroup() throws IOException {
        String searchTargetGroupName = setCheckNameFormat();
        if (searchTargetGroupName == null) return;

        String strSearchTargets = tvNewUrlToWatch.getText().toString();
        if (strSearchTargets.length() == 0) {
            String title = "Search url specified?";
            String content = "Please add url to the url list boxes first";

            mMsg.setPopupTipMsgBox(title, content, this);
            return;
        }

        String strKeywordsGroup = tvSelectedKeywordGroup.getText().toString();
        if (strKeywordsGroup.length() == 0) {
            String title = "Keywords specified?";
            String content = "Please add your interested keyword group first";

            mMsg.setPopupTipMsgBox(title, content, this);
            return;
        }


        CAppDbNewSearchTargetGroup newSearchTargetGroup = new CAppDbNewSearchTargetGroup(
            searchTargetGroupName,
            mStr.setAddJoinedString(strSearchTargets),
            mStr.setAddJoinedString(strKeywordsGroup), 1);
        getDbHdlInst();
        mDbHdl.setAddSearchTargetGroup(newSearchTargetGroup);
        mDbHdl.closeInstance();
    }


    private String setCheckNameFormat() {
        return getIfNameLegal(etSearchGroupName.getText().toString());
    }

    @Nullable
    private String getIfNameLegal(String strName) {
        if (strName.length() == 0) {
            setTipNoNameInput();
            return null;
        }
        else if (!strName.matches("[a-zA-Z0-9]*")) {
            String title = "Only letter and number are allowed here?";
            String content = "Please check the name of current keyword search";

            mMsg.setPopupTipMsgBox(title, content, this);
            return null;
        }
        return strName;
    }

    private void setTipNoNameInput() {
        String title = "Name missing?";
        String content = "Please give the name for current search target group.";

        mMsg.setPopupTipMsgBox(title, content, this);
    }

    private void setSelectKeywordGroup(View v) {
        //Toast.makeText(this.getContext(), "Select keyword group!", Toast.LENGTH_SHORT).show();
        try {
            setSelectKeywordGroup();
        } catch (Exception e) {}

    }

    private void setAddNewUrlToWatch(View v) {
        //Toast.makeText(this.getContext(), "Add new Url to Watch!", Toast.LENGTH_SHORT).show();
        String curEtFm = etUrl.getText().toString();
        if (curEtFm.length() == 0) {
            String title = "No input found";
            String content = "Please give the full match word first!";

            CPfUiAndroidMsg msg = new CPfUiAndroidMsg();
            msg.setPopupTipMsgBox(title, content, this);
            return;
        }

        ArrayList<String> curUrlString = mUrlLinkMng.getArrListText();

        String strCurIn = etUrl.getText().toString();
        if(curUrlString.size() == 0) {
            String[] strArrParts = curEtFm.split("\\.", -1);
            if ( strArrParts[0].equalsIgnoreCase("www") && strArrParts.length > 1 ) {
                etSearchGroupName.setText(strArrParts[1]);
            }
            else {
                etSearchGroupName.setText(strArrParts[0]);
            }
        }

        mUrlLinkMng.setAddNewToLinks(curEtFm);
        etUrl.setText("");
    }


    private void setAddStringToLinkManager(String curEtFm, CPfUiAndroidClickableLinkManager lnMng) {
        if (mStr.ifContainSpace(curEtFm)) {
            ArrayList<String> lsString = mStr.setAddJoinedString(curEtFm);
            for (String elem: lsString) {
                lnMng.setAddNewToLinks(elem);
            }
        }
        else {
            lnMng.setAddNewToLinks(curEtFm);
        }
    }


    private void getDbHdlInst() {
        if (mDbHdl == null) {
            //mDbHdl = new CAppDbHandler(getActivity(), null, null, 1 );
            mDbHdl = ((MainActivity)getActivity()).getDbInstance();
        }
        mDbHdl.getDbInstance();
    }


}
