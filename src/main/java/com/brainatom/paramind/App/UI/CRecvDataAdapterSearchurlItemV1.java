package com.brainatom.paramind.App.UI;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.brainatom.paramind.App.Database.CAppDbHandler;
import com.brainatom.paramind.App.Database.CAppDbNewSearchTargetGroup;
import com.brainatom.paramind.Platform.Type.CPfString;
import com.brainatom.paramind.R;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;

import java.io.IOException;
import java.util.ArrayList;

public class CRecvDataAdapterSearchurlItemV1 extends RecyclerSwipeAdapter<CRecvDataAdapterSearchurlItemV1.SimpleViewHolder> implements IPfUiActOkCancel{
    private Context mContext;
    private String mGroupName = null;
    CPfString mStrFac = null;
    int mCurPos = -1;

    SimpleViewHolder mCurViewHolder = null;
    private ArrayList<CAppRecvItemSearchGroupUrlList> mArrUrls;
    CAppDbHandler mDbHdl = null;
    CPfUiAndroidMsg mMsg = null;


    ////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////
    public CRecvDataAdapterSearchurlItemV1(Context mContext, ArrayList<CAppRecvItemSearchGroupUrlList> lsRecvItemUrl) {
        this.mContext = mContext;
        this.mArrUrls = lsRecvItemUrl;
    }



    ////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////



    public static class SimpleViewHolder extends RecyclerView.ViewHolder{
        public SwipeLayout mSwipeLayout;

        public TextView mTvSearchUrl;
        public TextView mTvEdit;
        public TextView mTvDelete;


        public SimpleViewHolder(View itemView) {
            super(itemView);

            mSwipeLayout = (SwipeLayout) itemView.findViewById(R.id.id_recvitem_single_searchgroup_v1);

            mTvSearchUrl = (TextView) itemView.findViewById(R.id.id_keyword_content_slurlv1);

            mTvDelete = (TextView) itemView.findViewById(R.id.id_Delete_recv_slurlv1);
            mTvEdit = (TextView) itemView.findViewById(R.id.id_Edit_recv_slurlv1);
        }
    }

    public void setDbHdl(CAppDbHandler mDbHdl) {
        this.mDbHdl = mDbHdl;
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recvitem_single_searchgroup, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final SimpleViewHolder viewHolder, final int position) {
        final CAppRecvItemSearchGroupUrlList item = mArrUrls.get(position);

        viewHolder.mTvSearchUrl.setText(item.getUrlAddr());

        setBuildupSwipelayout(viewHolder, item);

        viewHolder.mTvEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setEditItemFromRecv(viewHolder, position);

                Toast.makeText(view.getContext(), "Clicked on Edit  " + viewHolder.mTvSearchUrl.getText().toString(), Toast.LENGTH_SHORT).show();
            }
        });

        viewHolder.mTvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //setDeleteItemFromRecv(viewHolder, position);
                setDeleteItemFromRecv(viewHolder, position);

                Toast.makeText(v.getContext(), "Deleted " + viewHolder.mTvSearchUrl.getText().toString(), Toast.LENGTH_SHORT).show();
            }
        });

        // model
        mItemManger.bindView(viewHolder.itemView, position);
    }

    @Override
    public int getItemCount() {
        return mArrUrls.size();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.id_recvitem_single_searchgroup_v1;
    }

    @Override
    public boolean setExecActOnOk(Object data) {
        String result = (String)data;

        mCurViewHolder.mTvSearchUrl.setText(result);
        mArrUrls.get(mCurPos).setUrlAddr(result);

        mItemManger.removeShownLayouts(mCurViewHolder.mSwipeLayout);
        mItemManger.closeAllItems();

        if (mGroupName != null) {
            setUpdateDbOnUrlItemOf();
        }
        return true;
    }

    @Override
    public boolean setExecActOnCancel() {
        mItemManger.closeAllItems();
        return true;
    }

    @Override
    public boolean setExecAct() {
        return false;
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////

    private void setBuildupSwipelayout(SimpleViewHolder viewHolder, final CAppRecvItemSearchGroupUrlList item) {
        viewHolder.mSwipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);

        //dari kanan
        viewHolder.mSwipeLayout.addDrag(SwipeLayout.DragEdge.Right, viewHolder.mSwipeLayout.findViewById(R.id.id_choices_recvitem_single_slurlv1));

        viewHolder.mSwipeLayout.addSwipeListener(new SwipeLayout.SwipeListener() {
            @Override
            public void onStartOpen(SwipeLayout layout) {

            }

            @Override
            public void onOpen(SwipeLayout layout) {

            }

            @Override
            public void onStartClose(SwipeLayout layout) {

            }

            @Override
            public void onClose(SwipeLayout layout) {

            }

            @Override
            public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {

            }

            @Override
            public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {

            }
        });

        viewHolder.mSwipeLayout.getSurfaceView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(mContext, " Click : " + item.getUrlAddr(), Toast.LENGTH_SHORT).show();
            }
        });


    }


    ///////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////

    private void setEditItemFromRecv(SimpleViewHolder viewHolder, int position) {
        viewHolder.mSwipeLayout.close();

        mCurViewHolder = viewHolder;
        mCurPos = position;

        String title = "Please give a new name";
        String content = mArrUrls.get(position).getUrlAddr();

        getMsgUi();
        mMsg.setPopupEditBox(title, content, mContext, this);

        return;
    }


    private void setDeleteItemFromRecv(SimpleViewHolder viewHolder, int position) {
        mItemManger.removeShownLayouts(viewHolder.mSwipeLayout);

        mArrUrls.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, mArrUrls.size());

        mItemManger.closeAllItems();

        if (mGroupName != null) {
            setUpdateDbOnUrlItemOf();
        }
    }

    private void setUpdateDbOnUrlItemOf() {
        getDbHdl();
        String strUrllist = setFormUrllistString();

        CAppDbNewSearchTargetGroup newSearchTargetGroup = new CAppDbNewSearchTargetGroup(
            mGroupName,
            getStrFac().setAddJoinedStringByToken(strUrllist, ","),
            getStrFac().setAddJoinedStringByToken("", ","), 1);

        mDbHdl.getDbInstance();
        mDbHdl.setUpdateSearchGroupUrl(newSearchTargetGroup);

        // try {
        //     setCheckPointOfDBResult(mDbHdl);
        // } catch (IOException e) {
        //     e.printStackTrace();
        // }

        mDbHdl.closeInstance();
    }

    @NonNull
    private String setFormUrllistString() {
        String strUrllist = "";
        for (int i = 0; i < mArrUrls.size(); i++) {
            strUrllist += mArrUrls.get(i).getUrlAddr() + ",";
        }

        if (strUrllist.length() > 0 && strUrllist.charAt(strUrllist.length() - 1) == ',') {
            strUrllist = strUrllist.substring(0, strUrllist.length() - 1);
        }
        return strUrllist;
    }



    /////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////

    private CPfString getStrFac() {
        if(mStrFac == null) {
            mStrFac = new CPfString();
        }
        return mStrFac;
    }

    private CAppDbHandler getDbHdl() {
        if (mDbHdl == null) {
            mDbHdl = new CAppDbHandler(mContext, null, null, 1 );
        }
        mDbHdl.getDbInstance();
        return mDbHdl;
    }

    public String getGroupName() {
        return mGroupName;
    }

    public void setGroupName(String mGroupName) {
        this.mGroupName = mGroupName;
    }

    private void setCheckPointOfDBResult(CAppDbHandler dbHdl) throws IOException {
        ArrayList<CAppDbNewSearchTargetGroup> result1 = dbHdl.getAllDataItemsOfSearchTargetGroup();
        Log.d("DBKeyword", "check result");
    }

    private CPfUiAndroidMsg getMsgUi() {
        if(mMsg == null) {
            mMsg = new CPfUiAndroidMsg();
        }
        return mMsg;
    }

}




































