package com.brainatom.paramind.App.UI;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.brainatom.paramind.App.Database.CAppDbHandler;
import com.brainatom.paramind.Platform.Type.CPfString;
import com.brainatom.paramind.R;
import com.daimajia.swipe.util.Attributes;

import java.util.ArrayList;

public class CFragmentManageSingleSearchgroup extends Fragment {

    private ArrayList<CAppRecvItemSearchGroupUrlList> mDataUrllist = null;
    private ArrayList<CAppRecvItemSearchGroupLinkedInterest> mDataLinkedin = null;

    CRecvDataAdapterSearchurlItemV1 mSuadapter = null;
    CRecvDataAdapterLinkedInterestItemV1 mLladapter = null;

    private String mNameOfSearchGroup;
    private TextView mTvNameOfSearchGroup;

    private RecyclerView mUrllistRecyclerView;
    private RecyclerView mLinkedinRecyclerView;

    private TextView mTvEmptyUrllistTextView;
    private TextView mTvEmptyLinkedinTextView;

    private ImageView mIvSgUrlAddnew;
    private ImageView mIvSgLlAddnew;

    private Button mButDeleteGroup;

    View mView;

    private CIHandlerOnAddNewToUrllistSet mHandlerOnProcessNewUrl;
    private CIHandlerOnAddNewToLinkedinSet mHandlerOnProcessNewLinkedin;

    private CPfUiAndroidMsg mMsg = null;
    private CAppDbHandler mDbHdl = null;

    private CPfString mStrFac = null;



    //////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////
    public CFragmentManageSingleSearchgroup() {
        // Required empty public constructor
    }


    //////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_mng_single_searchgroup, container, false);


        setLinkRecvElement();
        //setLoadingData();
        setSelectiveDisp();


        // adaptor building
        mSuadapter = new CRecvDataAdapterSearchurlItemV1(this.getContext(), mDataUrllist);
        mSuadapter.setMode(Attributes.Mode.Single);

        mLladapter = new CRecvDataAdapterLinkedInterestItemV1(this.getContext(), mDataLinkedin);
        mLladapter.setMode(Attributes.Mode.Single);

        // link adapter with recy view
        mUrllistRecyclerView.setAdapter(mSuadapter);
        mLinkedinRecyclerView.setAdapter(mLladapter);

        mTvNameOfSearchGroup.setText(mNameOfSearchGroup);

        mSuadapter.setGroupName(mNameOfSearchGroup);
        mLladapter.setGroupName(mNameOfSearchGroup);

        mIvSgLlAddnew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String title = "Please give a new Url for search";
                String content = "";

                getMsgUi();
                getInstOfNewUrllist();
                mMsg.setPopupEditBox(title, content, getContext(), mHandlerOnProcessNewUrl);
            }
        });

        mIvSgLlAddnew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String title = "Please give a new interest group name you want to scan for";
                String content = "";

                getMsgUi();
                getInstOfNewLinkedin();
                mMsg.setPopupEditBox(title, content, getContext(), mHandlerOnProcessNewLinkedin);
            }
        });

        mButDeleteGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDbHdl();
                mDbHdl.setDeleteSearchTargetitemViaQueryViaName(mNameOfSearchGroup);
                mDbHdl.closeInstance();

                getFragmentManager().popBackStack();
            }
        });

        return mView;
    }





    //////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////

    // for simulation only
    private void setLoadingData() {
        // data loading
        mDataUrllist = new ArrayList<>();
        loadData();
    }

    private void setLoadFullmatchData() {

    }

    private void setLoadPartmatchData() {

    }

    public void loadData() {
        setLoadDummyData();
    }

    private void setLoadDummyData() {
        for (int i = 0; i <= 20; i++) {
            mDataUrllist.add(new CAppRecvItemSearchGroupUrlList("Romario " + i));
        }
    }

    private void setSelectiveDisp() {
        // selective showing
        if(mDataUrllist.isEmpty()){
            mUrllistRecyclerView.setVisibility(View.GONE);
            mTvEmptyUrllistTextView.setVisibility(View.VISIBLE);
        }
        else{
            mUrllistRecyclerView.setVisibility(View.VISIBLE);
            mTvEmptyUrllistTextView.setVisibility(View.GONE);
        }


        // selective showing
        if(mDataLinkedin.isEmpty()){
            mLinkedinRecyclerView.setVisibility(View.GONE);
            mTvEmptyLinkedinTextView.setVisibility(View.VISIBLE);
        }
        else{
            mLinkedinRecyclerView.setVisibility(View.VISIBLE);
            mTvEmptyLinkedinTextView.setVisibility(View.GONE);
        }
    }


    private void setLinkRecvElement() {
        mTvNameOfSearchGroup = (TextView) mView.findViewById(R.id.id_sSearchGroupName);

        // url list
        mTvEmptyUrllistTextView = (TextView) mView.findViewById(R.id.id_empty_view_sg);

        mUrllistRecyclerView = (RecyclerView) mView.findViewById(R.id.id_recv_searchgroup_mark);
        mUrllistRecyclerView.setLayoutManager(new LinearLayoutManager(this.getActivity()));


        // interest group
        mTvEmptyLinkedinTextView = (TextView) mView.findViewById(R.id.id_empty_view_interestgroup);

        mLinkedinRecyclerView = (RecyclerView) mView.findViewById(R.id.id_recv_linkedinterest_mark);
        mLinkedinRecyclerView.setLayoutManager(new LinearLayoutManager(this.getActivity()));

        mIvSgUrlAddnew = (ImageView)mView.findViewById(R.id.id_IvAddNewToUrllist);
        mIvSgLlAddnew = (ImageView)mView.findViewById(R.id.id_IvAddNewToLinkedin);

        mButDeleteGroup = (Button)mView.findViewById(R.id.id_ButDeleteGroupOfSearch);
    }

    public class CIHandlerOnAddNewToUrllistSet implements IPfUiActOkCancel{

        @Override
        public boolean setExecActOnOk(Object data) {
            getDbHdl();

            // String strPartmatch = setFormPartmatchString() + "," + (String)data;

            // CAppDbNewKeywordsGroup newKeywordsGroup2 = new CAppDbNewKeywordsGroup(
            //     mNameOfKwGroup,
            //     getStrFac().setAddJoinedStringByToken(strPartmatch, ","),
            //     getStrFac().setAddJoinedStringByToken("", ","));
            // mDbHdl.setUpdateKeywordGroupPm(newKeywordsGroup2);

            // // try {
            // //     setCheckPointOfDBResult(mDbHdl);
            // // } catch (IOException e) {
            // //     e.printStackTrace();
            // // }

            // mDbHdl.close();

            // mDataPmSet.add(new CAppRecvItemKeywordGroupPartmatch((String)data));
            // mPmadapter.setArrPmKeywords(mDataPmSet);
            // mPmadapter.notifyDataSetChanged();

            return true;
        }

        @Override
        public boolean setExecActOnCancel() {
            return false;
        }

        @Override
        public boolean setExecAct() {
            return false;
        }
    }

    public class CIHandlerOnAddNewToLinkedinSet implements IPfUiActOkCancel{

        @Override
        public boolean setExecActOnOk(Object data) {
            // getDbHdl();

            // String strPartmatch = setFormPartmatchString() + "," + (String)data;

            // CAppDbNewKeywordsGroup newKeywordsGroup2 = new CAppDbNewKeywordsGroup(
            //         mNameOfKwGroup,
            //         getStrFac().setAddJoinedStringByToken(strPartmatch, ","),
            //         getStrFac().setAddJoinedStringByToken("", ","));
            // mDbHdl.setUpdateKeywordGroupPm(newKeywordsGroup2);

            // // try {
            // //     setCheckPointOfDBResult(mDbHdl);
            // // } catch (IOException e) {
            // //     e.printStackTrace();
            // // }

            // mDbHdl.close();

            // mDataPmSet.add(new CAppRecvItemKeywordGroupPartmatch((String)data));
            // mPmadapter.setArrPmKeywords(mDataPmSet);
            // mPmadapter.notifyDataSetChanged();

            return true;
        }

        @Override
        public boolean setExecActOnCancel() {
            return false;
        }

        @Override
        public boolean setExecAct() {
            return false;
        }
    }



    //////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////


    public String getNameOfSearchGroup() {
        return mNameOfSearchGroup;
    }

    public void setNameOfSearchGroup(String mNameOfSearchGroup) {
        this.mNameOfSearchGroup = mNameOfSearchGroup;
    }

    public ArrayList<CAppRecvItemSearchGroupUrlList> getDataUrllist() {
        return mDataUrllist;
    }

    public void setDataUrllist(ArrayList<CAppRecvItemSearchGroupUrlList> dataUrllist) {
        this.mDataUrllist = dataUrllist;
    }

    public ArrayList<CAppRecvItemSearchGroupLinkedInterest> getDataLinkedin() {
        return mDataLinkedin;
    }

    public void setDataLinkedin(ArrayList<CAppRecvItemSearchGroupLinkedInterest> dataLinkedin) {
        this.mDataLinkedin = dataLinkedin;
    }

    public TextView getTvEmptyLinkedinTextView() {
        return mTvEmptyLinkedinTextView;
    }

    public void setTvEmptyLinkedinTextView(TextView linkedinTextView) {
        this.mTvEmptyLinkedinTextView = linkedinTextView;
    }

    public RecyclerView getLinkedinRecyclerView() {
        return mLinkedinRecyclerView;
    }

    public void setLinkedinRecyclerView(RecyclerView linkedinRecyclerView) {
        this.mLinkedinRecyclerView = linkedinRecyclerView;
    }

    public void setDbHdl(CAppDbHandler mDbHdl) {
        this.mDbHdl = mDbHdl;
    }


    //////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////

    private void getInstOfNewUrllist() {
        if(mHandlerOnProcessNewUrl == null) {
            mHandlerOnProcessNewUrl = new CIHandlerOnAddNewToUrllistSet();
        }
    }

    private void getInstOfNewLinkedin() {
        if(mHandlerOnProcessNewLinkedin == null) {
            mHandlerOnProcessNewLinkedin = new CIHandlerOnAddNewToLinkedinSet();
        }
    }

    private CPfUiAndroidMsg getMsgUi() {
        if(mMsg == null) {
            mMsg = new CPfUiAndroidMsg();
        }
        return mMsg;
    }

    private CAppDbHandler getDbHdl() {
        if (mDbHdl == null) {
            //mDbHdl = new CAppDbHandler(getContext(), null, null, 1 );
            mDbHdl = ((MainActivity)getActivity()).getDbInstance();
        }
        mDbHdl.getDbInstance();
        return mDbHdl;
    }

    private CPfString getStrFac() {
        if(mStrFac == null) {
            mStrFac = new CPfString();
        }
        return mStrFac;
    }

}
