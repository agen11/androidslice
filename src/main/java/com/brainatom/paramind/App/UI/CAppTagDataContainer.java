package com.brainatom.paramind.App.UI;

import java.util.ArrayList;
import java.util.Random;


/**
 * Created by Cuneyt on 21.8.2015.
 *
 */
public class CAppTagDataContainer {

    private String mName;
    private String mColor;


    //////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////

    public CAppTagDataContainer(String name) {
        this.mName = name;
        this.mColor = getRandomColor();
    }

    public CAppTagDataContainer() {
    }

    //////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////

    public static String getRandomColorV1() {
        ArrayList<String> colors = new ArrayList<>();
        setLoadColors(colors);

        return colors.get(new Random().nextInt(colors.size()));
    }


    public String getRandomColor() {
        ArrayList<String> colors = new ArrayList<>();
        setLoadColors(colors);

        return colors.get(new Random().nextInt(colors.size()));
    }

    public static void setLoadColors(ArrayList<String> colors) {
        colors.add("#ED7D31");
        colors.add("#00B0F0");
        colors.add("#FF0000");
        colors.add("#D0CECE");
        colors.add("#00B050");
        colors.add("#9999FF");
        colors.add("#FF5FC6");
        colors.add("#FFC000");
        colors.add("#7F7F7F");
        colors.add("#4800FF");

        colors.add("#008577");
        colors.add("#00574B");
        colors.add("#00171B");
        colors.add("#D81B60");
        colors.add("#720d5d");
        colors.add("#430033");
        colors.add("#e30425");
        colors.add("#000000");
        colors.add("#e30425");
        colors.add("#ff554f");
        colors.add("#FF0000");
        colors.add("#c75522");
        colors.add("#0057b1");
        colors.add("#2cbb00");
        colors.add("#2cbb00");
        colors.add("#FFA07A");
        colors.add("#8B0000");
        colors.add("#FFD700");
        colors.add("#FFA500");
        colors.add("#FF8C00");
        colors.add("#FF4500");
        colors.add("#FF6347");
        colors.add("#FF7F50");
        colors.add("#FFE4B5");
        colors.add("#FFFF00");
        colors.add("#F0E68C");
        colors.add("#FFDAB9");
        colors.add("#808000");
        colors.add("#8FBC8F");
        colors.add("#4169E1");
        colors.add("#5169E1");
        colors.add("#6169E1");
        colors.add("#8169E1");
        colors.add("#9169E1");
        colors.add("#A169E1");
        colors.add("#B169E1");
        colors.add("#2139F1");
        colors.add("#7B68EE");
        colors.add("#6A5ACD");
        colors.add("#B0C4DE");
        colors.add("#E6E6FA");
        colors.add("#DA70D6");
        colors.add("#BA55D3");
        colors.add("#D2691E");
        colors.add("#F4A460");
        colors.add("#FFDEAD");
        colors.add("#CD853F");
        colors.add("#800000");

    }

    //////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public String getColor() {
        return mColor;
    }

    public void setColor(String color) {
        this.mColor = color;
    }

    public void setSinif(String code) {
    }


}
