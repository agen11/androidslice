package com.brainatom.paramind.App.UI;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.brainatom.paramind.R;

public class CWizardFragmentKeywordGroup extends Fragment {

    int mPagePosition;

    ////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////


    public CWizardFragmentKeywordGroup() {
    }

    @SuppressLint("ValidFragment")
    public CWizardFragmentKeywordGroup(int PagePosition) {
        this.mPagePosition = PagePosition;
    }

    ////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        int layout_id = R.layout.fragment_wizard_add_keyword_s2;
        switch (mPagePosition) {
            case 0:
                layout_id = R.layout.fragment_wizard_add_keyword_s2;
                break;

            case 1:
                layout_id = R.layout.fragment_wizard_add_keyword_s3;
                break;

            case 2:
                layout_id = R.layout.fragment_wizard_add_keyword_s4;
                break;

        }

        return inflater.inflate(layout_id, container, false);
    }


}
