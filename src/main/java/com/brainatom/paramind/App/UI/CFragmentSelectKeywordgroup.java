package com.brainatom.paramind.App.UI;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.brainatom.paramind.App.Database.CAppDbHandler;
import com.brainatom.paramind.App.Database.CAppDbNewKeywordsGroup;
import com.brainatom.paramind.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class CFragmentSelectKeywordgroup extends Fragment {

    CAppDbHandler mDbHdl = null;

    private RecyclerView mRecyView;
    private RecyclerView.Adapter mAdaptor;
    private ArrayList<CAppDbNewKeywordsGroup> mKwGroup = null;
    private FloatingActionButton mFab;
    private CRecvDataAdapterSelectKeyworGroup mAdaptorPrac;
    private ArrayList<String> mArrUserSelectedKeywordGroupName = null;
    SetSendUserChoice itSendIntentData = null;


    //////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////


    public CFragmentSelectKeywordgroup() {
        // Required empty public constructor
    }


    //////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_select_keywordgroup, container, false);

        try {
            getDataFromDB();

            //getDBTextStringFordisplay(view);

        } catch (IOException e) {
            e.printStackTrace();
        }

        // link UI element
        mRecyView = view.findViewById(R.id.id_recvwSelectKeywordGroup);
        //mRecyView.setHasFixedSize(true);

        // layout manager
        mRecyView.setLayoutManager(new LinearLayoutManager(this.getActivity().getBaseContext()));

        // supply data to adaptor which control fundamental items
        mAdaptorPrac = new CRecvDataAdapterSelectKeyworGroup(mKwGroup, getActivity());
        mAdaptor = mAdaptorPrac;
        mRecyView.setAdapter(mAdaptor);


        // setlistener
        mFab = view.findViewById(R.id.id_ButFloatSelect);
        mFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDeliverChoice();
            }
        });

        mArrUserSelectedKeywordGroupName = new ArrayList<>();

        return view;
    }

    //////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////


    public interface SetSendUserChoice {
        public void setDeliverUserChoice(ArrayList<String> arrString);
    }



    public void setReferenceToSendUserChoice(SetSendUserChoice ref) {
        itSendIntentData = ref;
    }

    public void getDataFromDB() throws IOException {
        String strDb = "";

        getDbHdlInst();
        mKwGroup = mDbHdl.getAllDataItemsOfKeywordGroup();

        return;
    }





    //////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////

    private void setDeliverChoice() {
        List<CAppDbNewKeywordsGroup> res = mAdaptorPrac.getListItems();
        for (CAppDbNewKeywordsGroup elem: res) {
            if(elem.mIsSelected) {
                Log.d("Item", "Keyword group " + elem.mKeywordGroupName + " is selected!");
                mArrUserSelectedKeywordGroupName.add(elem.mKeywordGroupName);
            }
        }

        itSendIntentData.setDeliverUserChoice(mArrUserSelectedKeywordGroupName);

        getFragmentManager().popBackStack();
    }



    private void getDbHdlInst() {
        if (mDbHdl == null) {
            //mDbHdl = new CAppDbHandler(getActivity(), null, null, 1 );
            mDbHdl = ((MainActivity)getActivity()).getDbInstance();
        }
        mDbHdl.getDbInstance();
    }



}
