package com.brainatom.paramind.App.UI;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.brainatom.paramind.App.Database.CAppDbHandler;
import com.brainatom.paramind.App.Database.CAppDbNewKeywordsGroup;
import com.brainatom.paramind.Platform.Type.CPfString;
import com.brainatom.paramind.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class CFragmentWizardAddKeywordContainer extends Fragment {

    private static final String mTAG = "KeywordWizard";

    private CWizardFragmentAdapter mAdapter;

    //private CWizardFragmentKeywordGroup mSectionPageAdapter;
    //private CFragmentWizardPageAdapter mSectionPageAdapter;

    private ViewPager mViewPager = null;
    private ViewPagerAdapter mViewPagerAdapter;
    //ViewPager.OnPageChangeListener mViewlistener;

    private Button mNextBut;
    private Button mPrevBut;
    private int mCurPage = 0;


    View mView = null;
    Context mContext;


    String mKeywordGroupName = "";
    ArrayList<String> mFullmatchKeyword = new ArrayList<>();
    ArrayList<String> mPartmatchKeyword = new ArrayList<>();

    CAppDbHandler mDbHdl = null;

    ////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////

    public CFragmentWizardAddKeywordContainer() {
        // Required empty public constructor
    }


    ////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (mView != null) {
            return mView;
        }

        mView = inflater.inflate(R.layout.fragment_wizard_add_keyword_container, container, false);

        setLoadDataOfViewpagerAdapter();

        setBuildViewpager();

        setBuildButtonPair();

        return mView;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }


    //////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////


    // adapter related
    private class ViewPagerAdapter extends FragmentPagerAdapter {

        private int WIZARD_PAGES_COUNT = 3;

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return new CWizardFragmentKeywordGroup(position);
        }

        @Override
        public int getCount() {
            return WIZARD_PAGES_COUNT;
        }

    }


    // indicator related
    private class CInWizardPageChangeListener implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageScrollStateChanged(int position) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onPageScrolled(int position, float positionOffset,
                                   int positionOffsetPixels) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onPageSelected(int position) {
        }

    }


    ////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////

    private void setLoadDataOfViewpagerAdapter() {
        List<Fragment> list = new ArrayList<>();
        list.add(new CFragmentWizardAddKeywordS1());
        list.add(new CFragmentWizardAddKeywordS2());
        list.add(new CFragmentWizardAddKeywordS3());
        list.add(new CFragmentWizardAddKeywordS4());
        mAdapter = new CWizardFragmentAdapter(getChildFragmentManager(), list);
    }

    private void setBuildViewpager() {
        // adding content manager
        mViewPager = (ViewPager)mView.findViewById(R.id.id_Viewpager_keyword_V1);
        mViewPager.setAdapter(mAdapter);

        mViewPager.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });

        mViewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener(){
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);

                if(position > mCurPage) {
                    if (setSyncForwardButton()) return;
                    mCurPage = mViewPager.getCurrentItem();

                }
                else if (position < mCurPage) {
                    if (setSyncBackwardButton()) return;
                    mCurPage = mViewPager.getCurrentItem();
                }
            }
        });


        mViewPager.addOnPageChangeListener(new CInWizardPageChangeListener());
    }

    private void setBuildButtonPair() {
        mNextBut = (Button)mView.findViewById(R.id.id_butRight_keyword_v1);
        mPrevBut = (Button)mView.findViewById(R.id.id_butLeft_keyword_v1);


        mNextBut.setText("Create");
        mNextBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = getChildFragmentManager().findFragmentByTag("android:switcher:" + R.id.id_Viewpager_keyword_V1 + ":" + mCurPage);
                if (mViewPager.getCurrentItem() == 1 && fragment != null) {
                    if (!getCheckedKeywordGroupName((CFragmentWizardAddKeywordS2) fragment)) return;
                }
                else if (mViewPager.getCurrentItem() == 2 && fragment != null) {
                    if (!getFullmatchKeywords((CFragmentWizardAddKeywordS3) fragment)) return;
                }
                else if (mViewPager.getCurrentItem() == 3 && fragment != null) {
                    if (!getPartmatchKeywords((CFragmentWizardAddKeywordS4) fragment)) return;
                    // DB update

                    try {
                        setInsertNewKeywordGroup();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                setStepForward();

            }
        });

        mPrevBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setStepBack();
            }
        });
    }


    private boolean getCheckedKeywordGroupName(CFragmentWizardAddKeywordS2 fragment) {
        String name = fragment.getDataS2();
        if (name.length() == 0) {
            Toast.makeText(getContext(), "The keyword group name is needed before proceed.", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (!name.matches("[a-zA-Z0-9]+_*")) {
            Toast.makeText(getContext(), "Only letter,number,underline can be entered, at least one letter or number is needed.", Toast.LENGTH_SHORT).show();
            return false;
        }
        else {
            mKeywordGroupName = name;
        }
        return true;
    }

    private boolean getFullmatchKeywords(CFragmentWizardAddKeywordS3 fragment) {
        ArrayList<String> arrStrElements = fragment.getDataS3();
        if (arrStrElements.size() == 0) {
            Toast.makeText(getContext(), "No keyword specified on full match.", Toast.LENGTH_SHORT).show();
        }
        else {
            // tobe
            for (String kw: arrStrElements) {
                if (!kw.matches("[a-zA-Z0-9]+_*")) {
                    Toast.makeText(getContext(), "Only letter,number,underline are supported by now, please check your input.", Toast.LENGTH_SHORT).show();
                    return false;
                }
            }
        }
        mFullmatchKeyword = arrStrElements;
        return true;
    }

    private boolean getPartmatchKeywords(CFragmentWizardAddKeywordS4 fragment) {
        ArrayList<String> arrStrElements = fragment.getDataS4();
        if (arrStrElements.size() == 0 && (mFullmatchKeyword.size() == 0) ) {
            Toast.makeText(getContext(), "No content found in full match or partial match input, at least one of which should be set.", Toast.LENGTH_SHORT).show();
            return false;
        }
        else {
            // tobe
            for (String kw: arrStrElements) {
                if (!kw.matches("[a-zA-Z0-9]+_*")) {
                    Toast.makeText(getContext(), "Only letter,number,underline are supported by now, please check your input.", Toast.LENGTH_SHORT).show();
                    return false;
                }
            }
        }
        mPartmatchKeyword = arrStrElements;
        return true;
    }

    private void setSetupViewPager(ViewPager viewPager) {
        CFragmentWizardPageAdapter adaptor = new CFragmentWizardPageAdapter(getActivity().getSupportFragmentManager());

        adaptor.setAddFragment(new CFragmentWizardAddKeywordS1(), "TAB1");
        adaptor.setAddFragment(new CFragmentWizardAddKeywordS2(), "TAB2");
        adaptor.setAddFragment(new CFragmentWizardAddKeywordS3(), "TAB3");
        adaptor.setAddFragment(new CFragmentWizardAddKeywordS4(), "TAB4");

        mViewPager.setAdapter(adaptor);
    }

    private boolean setSyncBackwardButton() {
        if (mCurPage == 1) {
            mPrevBut.setVisibility(View.INVISIBLE);
            mNextBut.setVisibility(View.VISIBLE);
            mNextBut.setText("Create");
        }
        else if (mCurPage == 0) {
            mNextBut.setText("Create");
            return true;
        }
        return false;
    }

    private boolean setSyncForwardButton() {
        if (mCurPage == 0) {
            mPrevBut.setVisibility(View.VISIBLE);
            mNextBut.setVisibility(View.VISIBLE);
            mNextBut.setText("Next");
        }
        if (mCurPage == mAdapter.getCount() - 2) {
            mPrevBut.setVisibility(View.VISIBLE);
            mNextBut.setVisibility(View.VISIBLE);
            mNextBut.setText("Finish");
        }
        else if (mCurPage == mAdapter.getCount() - 1) {
            return true;
        }
        else {
            mNextBut.setText("Next");
        }
        return false;
    }

    private void setStepForward() {
        if (mCurPage == 0) {
            mPrevBut.setVisibility(View.VISIBLE);
            mNextBut.setVisibility(View.VISIBLE);
            mNextBut.setText("Next");
        }
        if (mCurPage == mAdapter.getCount() - 2) {
            mPrevBut.setVisibility(View.VISIBLE);
            mNextBut.setVisibility(View.VISIBLE);
            mNextBut.setText("Finish");
        }
        else if (mCurPage == mAdapter.getCount() - 1) {
            mPrevBut.setVisibility(View.INVISIBLE);
            mNextBut.setVisibility(View.VISIBLE);
            mNextBut.setText("Create");
            mCurPage = 0;

            // finalize all and update
            Toast.makeText(getContext(), "New interest group " + mKeywordGroupName + " has been updated.", Toast.LENGTH_SHORT).show();
            mViewPager.setCurrentItem(mCurPage);
            return;
        }
        else {
            mNextBut.setText("Next");
        }

        mCurPage = mViewPager.getCurrentItem() + 1;
        mViewPager.setCurrentItem(mCurPage);
    }

    private void setInsertNewKeywordGroup() throws IOException {
        CPfString mStr = new CPfString();
        CAppDbNewKeywordsGroup newKeywordsGroup = new CAppDbNewKeywordsGroup(
                mKeywordGroupName, mFullmatchKeyword, mPartmatchKeyword, 1);
        getDbHdlInst();
        mDbHdl.setAddKeywordGroup(newKeywordsGroup);
        mDbHdl.close();
    }

    private void setStepBack() {
        if (setSyncBackwardButton()) return;

        mCurPage = mViewPager.getCurrentItem() - 1;
        mViewPager.setCurrentItem(mCurPage);
    }

    private void getDbHdlInst() {
        if (mDbHdl == null) {
            mDbHdl = ((MainActivity)getActivity()).getDbInstance();
        }
        mDbHdl.getDbInstance();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////




}
