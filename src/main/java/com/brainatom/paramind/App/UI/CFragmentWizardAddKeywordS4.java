package com.brainatom.paramind.App.UI;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.brainatom.paramind.Platform.Type.CPfString;
import com.brainatom.paramind.Platform.UI.CPfUiAndroidClickableLinkManager;
import com.brainatom.paramind.R;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class CFragmentWizardAddKeywordS4 extends Fragment implements IPfUiData{

    private TextView tvPartMatch;

    CPfUiAndroidClickableLinkManager mPartmatchLinkMng = null;
    private EditText mEtPartMatch;

    View mView = null;


    CPfUiAndroidMsg mMsg = null;
    CPfString mStr = null;


    /////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////

    public CFragmentWizardAddKeywordS4() {
        // Required empty public constructor
        mMsg = new CPfUiAndroidMsg();
        mStr = new CPfString();
    }




    /////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // id_InputKeywordsParmatchV1
        // id_ButAddKeywordPartmatchV1
        // id_tvPartmatchKeywordsV2

        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_wizard_add_keyword_s4, container, false);

        mEtPartMatch = (EditText)mView.findViewById(R.id.id_InputKeywordsParmatchV1);

        tvPartMatch = (TextView)mView.findViewById(R.id.id_tvPartmatchKeywordsV2);
        mPartmatchLinkMng = new CPfUiAndroidClickableLinkManager((TextView)mView.findViewById(R.id.id_tvPartmatchKeywordsV2), "");

        final Button butAddPartmatch = (Button)mView.findViewById(R.id.id_ButAddKeywordPartmatchV1);
        butAddPartmatch.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        setAddPartMatchKeyword(v);
                    }
                }
        );


        return mView;
    }

    @Override
    public Object getData() {
        return (Object)mPartmatchLinkMng.getArrListText();
    }


    /////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////
    public ArrayList getDataS4() {
        return mPartmatchLinkMng.getArrListText();
    }


    /////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////

    private void setAddPartMatchKeyword(View v) {
        String curEtFm = mEtPartMatch.getText().toString();
        if (curEtFm.length() == 0) {
            String title = "No input found";
            String content = "Please give the part match word first!";

            mMsg.setPopupTipMsgBox(title, content, this);
            return;
        }
        else if ((!curEtFm.matches("[a-zA-Z0-9_]+"))) {
            String title = "Not supported character";
            String content = "Only letter, number, underline are supported now!";

            mMsg.setPopupTipMsgBox(title, content, this);
            return;
        }

        mPartmatchLinkMng.setAddStringToLinkManager(curEtFm);
        mEtPartMatch.setText("");
    }


}
