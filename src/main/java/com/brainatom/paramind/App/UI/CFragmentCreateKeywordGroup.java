package com.brainatom.paramind.App.UI;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.brainatom.paramind.App.Database.CAppDbHandler;
import com.brainatom.paramind.App.Database.CAppDbNewKeywordsGroup;
import com.brainatom.paramind.Platform.Type.CPfString;
import com.brainatom.paramind.Platform.UI.CPfUiAndroidClickableLinkManager;
import com.brainatom.paramind.R;

import java.io.IOException;
import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class CFragmentCreateKeywordGroup extends Fragment {
    private TextView tvFullMatch;
    private TextView tvPartMatch;

    private EditText etSearchName;
    private EditText etFullMatch;
    private EditText etPartMatch;

    CPfUiAndroidClickableLinkManager mFullmatchLinkMng = null;
    CPfUiAndroidClickableLinkManager mPartmatchLinkMng = null;

    CAppDbHandler mDbHdl = null;

    CPfUiAndroidMsg mMsg = null;
    CPfString mStr = null;

    View mview;

    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////

    public CFragmentCreateKeywordGroup() {
        // Required empty public constructor
        mMsg = new CPfUiAndroidMsg();
        mStr = new CPfString();
    }

    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (mview != null) {
            return mview;
        }

        View view = inflater.inflate(R.layout.fragment_mng_add_keyword, container, false);
        mview = view;

        // init data container
        tvFullMatch = (TextView)view.findViewById(R.id.id_tvFullmatchKeywords);
        tvPartMatch = (TextView)view.findViewById(R.id.id_tvPartmatchKeywords);

        mFullmatchLinkMng = new CPfUiAndroidClickableLinkManager((TextView)view.findViewById(R.id.id_tvFullmatchKeywords), "");
        mPartmatchLinkMng = new CPfUiAndroidClickableLinkManager((TextView)view.findViewById(R.id.id_tvPartmatchKeywords), "");

        etSearchName = (EditText)view.findViewById(R.id.id_InputNameOfKeywordGroup);
        etFullMatch = (EditText)view.findViewById(R.id.id_InputKeywordsFullmatch);
        etPartMatch = (EditText)view.findViewById(R.id.id_InputKeywordsPartmatch);

        // button init
        final Button butAddFullmatch = (Button)view.findViewById(R.id.id_ButAddKeywordFullmatch);
        final Button butAddPartmatch = (Button)view.findViewById(R.id.id_ButAddKeywordPartmatch);
        final Button butCreateKwGroup = (Button)view.findViewById(R.id.id_ButCreateNewKeywordGroup);
        final Button butSeeKwGroup = (Button)view.findViewById(R.id.id_ButCheckAllKeywordGroup);

        butAddFullmatch.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        setAddFullMatchKeyword(v);
                    }
                }
        );

        butAddPartmatch.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        setAddPartMatchKeyword(v);
                    }
                }
        );
        
        butCreateKwGroup.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            setCreateKeywordGroup(v);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        //setCreateKeywordGroupSim(v);
                    }
                }
        );

        butSeeKwGroup.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        setSeeAllKeywordGroup(v);
                    }
                }
        );

        // required action for return on model demand
        return view;
    }



    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////

    private void setSeeAllKeywordGroup(View v) {
        try {
            setSwitchFragment();
        } catch (Exception e) {}

    }

    private void setSwitchFragment() {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.id_frameOfMainActivity, new CFragmentDispKeywordGroup());
        ft.addToBackStack(null);
        ft.commit();
    }

    private void setCreateKeywordGroup(View v) throws IOException {
        String keywordGroupName = setCheckNameFormat(v);
        if (keywordGroupName == null) return;

        String strFullmatch = tvFullMatch.getText().toString();
        String strPartmatch = tvPartMatch.getText().toString();
        if (!setCheckSearchKeywordList(strFullmatch, strPartmatch)) return;

        setInsertDataToTableKeyword(keywordGroupName, strFullmatch, strPartmatch);
    }

    private void setCreateKeywordGroupSim(View v) throws IOException {
        String keywordGroupName = "woie";
        String strFullmatch = "owejosd cljsdj";
        String strPartmatch = "";

        setInsertDataToTableKeyword(keywordGroupName, strFullmatch, strPartmatch);
    }

    private void setInsertDataToTableKeyword(String keywordGroupName, String strFullmatch, String strPartmatch) throws IOException {
        getDbHdlInst();
        CAppDbNewKeywordsGroup newKeywordsGroup = new CAppDbNewKeywordsGroup(
            keywordGroupName,
            mStr.setAddJoinedString(strFullmatch),
            mStr.setAddJoinedString(strPartmatch), 1);
        getDbHdlInst();
        mDbHdl.setAddKeywordGroup(newKeywordsGroup);
        mDbHdl.closeInstance();
    }

    private void getDbHdlInst() {
        if (mDbHdl == null) {
            mDbHdl = ((MainActivity)getActivity()).getDbInstance();
        }
        mDbHdl.getDbInstance();
    }



    private boolean setCheckSearchKeywordList(String strFullmatch, String strPartmatch) {
        if (strFullmatch.length()==0 && strPartmatch.length()==0) {
            String title = "No keywords specified?";
            String content = "Please add your interest to the either of the two keyword boxes";

            mMsg.setPopupTipMsgBox(title, content, this);
            return false;
        }
        return true;
    }

    @Nullable
    private String setCheckNameFormat(View v) {
        String keywordGroupName = etSearchName.getText().toString();
        return getIfNameLegal(keywordGroupName);
    }

    @Nullable
    private String getIfNameLegal(String strName) {
        if (strName.length() == 0) {
            setTipNoNameInput();
            return null;
        }
        else if (!strName.matches("[a-zA-Z0-9]*")) {
            String title = "Only letter and number are allowed here?";
            String content = "Please check the name of current keyword search";

            mMsg.setPopupTipMsgBox(title, content, this);
            return null;
        }
        return strName;
    }

    private void setTipNoNameInput() {
        String title = "Name missing?";
        String content = "Please give the name for current keyword search";

        mMsg.setPopupTipMsgBox(title, content, this);
    }

    private void setAddFullMatchKeyword(View v) {
        //Toast.makeText(this.getContext(), "Add full match keywords!", Toast.LENGTH_SHORT).show();
        String curEtFm = etFullMatch.getText().toString();
        if (curEtFm.length() == 0) {
            String title = "No input found";
            String content = "Please give the full match word first!";

            mMsg.setPopupTipMsgBox(title, content, this);
            return;
        }

        CPfUiAndroidClickableLinkManager lnMng = mFullmatchLinkMng;
        setAddStringToLinkManager(curEtFm, lnMng);

        etFullMatch.setText("");
    }

    private void setAddStringToLinkManager(String curEtFm, CPfUiAndroidClickableLinkManager lnMng) {
        if (mStr.ifContainSpace(curEtFm)) {
            ArrayList<String> lsString = mStr.setAddJoinedString(curEtFm);
            for (String elem: lsString) {
                lnMng.setAddNewToLinks(elem);
            }
        }
        else {
            lnMng.setAddNewToLinks(curEtFm);
        }
    }

    private void setAddPartMatchKeyword(View v) {
        //Toast.makeText(this.getContext(), "Add part match keywords!", Toast.LENGTH_SHORT).show();
        String curEtPm = etPartMatch.getText().toString();
        if (curEtPm.length() == 0) {
            String title = "No input found";
            String content = "Please give the partial match word first!";

            mMsg.setPopupTipMsgBox(title, content, this);
            return;
        }

        CPfUiAndroidClickableLinkManager lnMng = mPartmatchLinkMng;
        setAddStringToLinkManager(curEtPm, lnMng);
        etPartMatch.setText("");
    }



}
