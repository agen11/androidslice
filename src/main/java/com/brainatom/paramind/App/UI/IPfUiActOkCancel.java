package com.brainatom.paramind.App.UI;

interface IPfUiActOkCancel extends IPfUiAct {
    public boolean setExecActOnOk(Object data);
    public boolean setExecActOnCancel();
}
