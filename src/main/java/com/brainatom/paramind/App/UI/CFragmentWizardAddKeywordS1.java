package com.brainatom.paramind.App.UI;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.brainatom.paramind.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class CFragmentWizardAddKeywordS1 extends Fragment {

    private static final String mTAG = "KeywordWizard";

    private CFragmentWizardPageAdapter mSectionPageAdapter;

    private ViewPager mViewPager;

    View mView;
    Context mContext;


    ////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////

    public CFragmentWizardAddKeywordS1() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_wizard_add_keyword_s1, container, false);

        // btView.setOnClickListener(new View.OnClickListener() {
        //     @Override
        //     public void onClick(View v) {
        //         FragmentTransaction ft = ((FragmentActivity)mContext).getSupportFragmentManager().beginTransaction();
        //         ft.replace(R.id.id_mainfragment, new CFragmentWizardAddKeywordContainer());
        //         ft.addToBackStack(null);
        //         ft.commit();
        //     }
        // });

        return mView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }



    ////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////



}
