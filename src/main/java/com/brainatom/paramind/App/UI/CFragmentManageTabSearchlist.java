package com.brainatom.paramind.App.UI;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.brainatom.paramind.R;


public class CFragmentManageTabSearchlist extends Fragment {
    private CTabSearchlistPageAdapor mTabSearchlistPageAdapor;
    private ViewPager mViewPager;
    View mView = null;


    ////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////
    public CFragmentManageTabSearchlist() {
        // Required empty public constructor
    }


    ////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (mView == null) {
            mView = inflater.inflate(R.layout.fragment_mng_tab_searchlist, container, false);
        }

        Activity activity = getActivity();
        mTabSearchlistPageAdapor = new CTabSearchlistPageAdapor(((FragmentActivity) activity).getSupportFragmentManager());

        mViewPager = (ViewPager)mView.findViewById(R.id.id_viewpager_searchlist);
        setSetupViewPager(mViewPager);

        TabLayout tabLayout = (TabLayout)mView.findViewById(R.id.id_TabSearchlist);
        tabLayout.setupWithViewPager(mViewPager);

        mViewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener(){
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                if(position == 0) {
                    IAppFragRecvUser obj = (IAppFragRecvUser)getChildFragmentManager().getFragments().get(0);
                    if (obj != null) {
                        try {
                            obj.setReloadData();
                        }
                        catch (Exception e) {}
                    }

                }
            }
        });

        return mView;
    }


    ////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////
    private void setSetupViewPager(ViewPager viewPager) {
        Activity activity = getActivity();
        CTabSearchlistPageAdapor adaptor = new CTabSearchlistPageAdapor(getChildFragmentManager());

        // load fragment in concealed way
        adaptor.setAddFragment(new CFragmentDispSearchlistV1(), "My searches");
        adaptor.setAddFragment(new CFragmentWizardAddSearchContainer(), "New search");

        viewPager.setAdapter(adaptor);
    }



}
