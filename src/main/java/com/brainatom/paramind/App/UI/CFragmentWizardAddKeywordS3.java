package com.brainatom.paramind.App.UI;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.brainatom.paramind.Platform.Type.CPfString;
import com.brainatom.paramind.Platform.UI.CPfUiAndroidClickableLinkManager;
import com.brainatom.paramind.R;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class CFragmentWizardAddKeywordS3 extends Fragment implements IPfUiData{

    private TextView tvFullMatch;

    CPfUiAndroidClickableLinkManager mFullmatchLinkMng = null;
    private EditText mEtFullMatch;

    View mView = null;


    CPfUiAndroidMsg mMsg = null;
    CPfString mStr = null;


    ///////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////


    public CFragmentWizardAddKeywordS3() {
        // Required empty public constructor
        mMsg = new CPfUiAndroidMsg();
        mStr = new CPfString();
    }


    //////////////////////////////////////////////////////////////////////////////////


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_wizard_add_keyword_s3, container, false);

        mEtFullMatch = (EditText)mView.findViewById(R.id.id_InputKeywordsFullmatchV1);

        tvFullMatch = (TextView)mView.findViewById(R.id.id_tvFullMatchingKeywordsV3);
        mFullmatchLinkMng = new CPfUiAndroidClickableLinkManager((TextView)mView.findViewById(R.id.id_tvFullMatchingKeywordsV3), "");


        final Button butAddFullmatch = (Button)mView.findViewById(R.id.id_ButAddKeywordFullmatchV1);
        butAddFullmatch.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        setAddFullMatchKeyword(v);
                    }
                }
        );


        return mView;

    }


    @Override
    public Object getData() {
        return (Object)mFullmatchLinkMng.getArrListText();
    }


    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////

    public ArrayList getDataS3() {
        return mFullmatchLinkMng.getArrListText();
    }


    ///////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////

    private void setAddFullMatchKeyword(View v) {
        //Toast.makeText(this.getContext(), "Add full match keywords!", Toast.LENGTH_SHORT).show();
        String curEtFm = mEtFullMatch.getText().toString();
        if (curEtFm.length() == 0 ) {
            String title = "No input found";
            String content = "Please give the full match word first!";

            mMsg.setPopupTipMsgBox(title, content, this);
            return;
        }
        else if ((!curEtFm.matches("[a-zA-Z0-9_]+"))) {
            String title = "Not supported character";
            String content = "Only letter, number, underline are supported now!";

            mMsg.setPopupTipMsgBox(title, content, this);
            return;
        }


        mFullmatchLinkMng.setAddStringToLinkManager(curEtFm);
        mEtFullMatch.setText("");
    }



}
