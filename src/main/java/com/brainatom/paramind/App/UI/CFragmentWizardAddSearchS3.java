package com.brainatom.paramind.App.UI;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.brainatom.paramind.Platform.Type.CPfString;
import com.brainatom.paramind.Platform.UI.CPfUiAndroidClickableLinkManager;
import com.brainatom.paramind.R;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class CFragmentWizardAddSearchS3 extends Fragment implements IPfUiData{


    private TextView tvUrl;

    CPfUiAndroidClickableLinkManager mUrllistLinkMng = null;
    private EditText mEtUrl;

    View mView = null;


    CPfUiAndroidMsg mMsg = null;
    CPfString mStr = null;


    //////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////


    public CFragmentWizardAddSearchS3() {
        // Required empty public constructor
        mMsg = new CPfUiAndroidMsg();
        mStr = new CPfString();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_wizard_add_search_s3, container, false);

        mEtUrl = (EditText)mView.findViewById(R.id.id_etInputUrlV2);

        tvUrl = (TextView)mView.findViewById(R.id.id_tvUrlList);
        mUrllistLinkMng = new CPfUiAndroidClickableLinkManager(tvUrl, "");


        final Button butAddUrl = (Button)mView.findViewById(R.id.id_ButAddNewUrlV2);
        butAddUrl.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        setAddUrlTolist(v);
                    }
                }
        );

        return mView;
    }

    @Override
    public Object getData() {
        return (Object) mUrllistLinkMng.getArrListText();
    }

    public ArrayList<String> getDataS3() {
        return mUrllistLinkMng.getArrListText();
    }

    ///////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////
    private void setAddUrlTolist(View v) {
        //Toast.makeText(this.getContext(), "Add full match keywords!", Toast.LENGTH_SHORT).show();
        String curEtFm = mEtUrl.getText().toString();
        if (curEtFm.length() == 0 ) {
            String title = "No input found";
            String content = "Please give the url first!";

            mMsg.setPopupTipMsgBox(title, content, this);
            return;
        }
        else if (curEtFm.matches(" +")) {
            String title = "Not supported character";
            String content = "Space is detected!";

            mMsg.setPopupTipMsgBox(title, content, this);
            return;
        }


        mUrllistLinkMng.setAddStringToLinkManager(curEtFm);
        mEtUrl.setText("");
    }


}
