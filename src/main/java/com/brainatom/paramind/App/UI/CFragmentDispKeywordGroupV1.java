package com.brainatom.paramind.App.UI;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.brainatom.paramind.App.Database.CAppDbHandler;
import com.brainatom.paramind.App.Database.CAppDbNewKeywordsGroup;
import com.brainatom.paramind.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class CFragmentDispKeywordGroupV1 extends Fragment implements IAppFragRecvUser{


    private RecyclerView mRecyView;
    private ArrayList<CAppDbNewKeywordsGroup> mKwGroup = null;
    private CRecvDataAdapterKeyworGroupV1 mAdaptorPrac;

    // data supplier
    private CAppDbHandler mDbHdl = null;

    // selection related data
    private ArrayList<String> mArrUserSelectedKeywordGroupName = null;




    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////
    // model

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_disp_keyword_group_v1,
                container, false);

       // data piping
        try {
            getDataFromDB();
            //getDBTextStringFordisplay(view);
        } catch (IOException e) {
            e.printStackTrace();
        }


        // recyclerview elements
        setInitializeRecyviewObj(view);

        // others
        mArrUserSelectedKeywordGroupName = new ArrayList<>();

        // menu
        setHasOptionsMenu(true);

        return view;
    }

    private void setInitializeRecyviewObj(View view) {
        // get handler of link UI element
        mRecyView = view.findViewById(R.id.id_recvwDispKeywordgroupV1);
        mRecyView.setHasFixedSize(false);

        // layout manager
        mRecyView.setLayoutManager(new LinearLayoutManager(
                this.getActivity().getBaseContext()));

        // supply data to adaptor
        mAdaptorPrac = new CRecvDataAdapterKeyworGroupV1(mKwGroup, getActivity());
        mRecyView.setAdapter( mAdaptorPrac );
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        getDbHdl();
        super.setUserVisibleHint(isVisibleToUser);

        setReloadData();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if ( menu != null ) {
            if ( !menu.hasVisibleItems() ) {
                getActivity().getMenuInflater().inflate(R.menu.menu_edit_listitem_searchtargetgroup, menu);
            }
        }
        return;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.id_keyword_group_item_remove:
                setCb_RemoveItemOfKeywordGroup();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////

    public boolean setReloadData() {
        try {
            getDataFromDB();

            //mKwGroup.clear();
            mAdaptorPrac.setListItems(mKwGroup);

            mAdaptorPrac.notifyDataSetChanged();
        } catch (IOException e) {}
        return true;
    }

    public void getDataFromDB() throws IOException {
        getDbHdlInst();
        mKwGroup = mDbHdl.getAllDataItemsOfKeywordGroup();
        return;
    }




    public void setCb_RemoveItemOfKeywordGroup() {
        List<CAppDbNewKeywordsGroup> res = mAdaptorPrac.getListItems();
        for(CAppDbNewKeywordsGroup elem: res) {
            if(elem.mIsSelected) {
                mArrUserSelectedKeywordGroupName.add(elem.mKeywordGroupName);
            }
        }

        // delete it from Db
        getDbHdl();
        for (String item: mArrUserSelectedKeywordGroupName) {
            mDbHdl.setDeleteKeywordGroupitemViaQueryViaName(item);
            for (CAppDbNewKeywordsGroup elem: mKwGroup) {
                if (elem.getKeywordGroupName().equals(item)) {
                    mKwGroup.remove(elem);
                    break;
                }

            }
        }

        mAdaptorPrac.notifyDataSetChanged();
    }

    public CAppDbHandler getDbHdl() {
        return mDbHdl;
    }

    public void setDbHdl(CAppDbHandler mDbHdl) {
        this.mDbHdl = mDbHdl;
    }



    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////

    private void getDbHdlInst() {
        if (mDbHdl == null) {
            mDbHdl = ((MainActivity)getActivity()).getDbInstance();
        }
        mDbHdl.getDbInstance();
    }


}
