package com.brainatom.paramind.App.UI;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.brainatom.paramind.App.Database.CAppDbHandler;
import com.brainatom.paramind.App.Database.CAppDbNewKeywordsGroup;
import com.brainatom.paramind.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CFragmentWizardAddSearchS4 extends Fragment implements IPfUiData{

    CAppDbHandler mDbHdl = null;

    private RecyclerView mRecyView;
    private CRecvDataAdapterSelectKeyworGroup mAdaptorPrac;
    private ArrayList<CAppDbNewKeywordsGroup> mKwGroup = null;

    private ArrayList<String> mArrUserSelectedKeywordGroupName = null;



    //////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////

    public CFragmentWizardAddSearchS4() {
        // Required empty public constructor
    }

    //////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_wizard_add_search_s4, container, false);

        try {
            getDataFromDB();

            //getDBTextStringFordisplay(view);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // link UI element
        mRecyView = view.findViewById(R.id.id_recvwSelectKeywordGroupV1);
        //mRecyView.setHasFixedSize(true);

        // layout manager
        mRecyView.setLayoutManager(new LinearLayoutManager(this.getActivity().getBaseContext()));

        // supply data to adaptor which control fundamental items
        mAdaptorPrac = new CRecvDataAdapterSelectKeyworGroup(mKwGroup, getActivity());
        mRecyView.setAdapter(mAdaptorPrac);



        mArrUserSelectedKeywordGroupName = new ArrayList<>();

        return view;
    }

    @Override
    public Object getData() {
        return (Object)getDataS4();
    }




    //////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////

    public ArrayList<String> getDataS4() {
        List<CAppDbNewKeywordsGroup> res = mAdaptorPrac.getListItems();
        for (CAppDbNewKeywordsGroup elem: res) {
            if(elem.mIsSelected) {
                Log.d("Item", "Keyword group " + elem.mKeywordGroupName + " is selected!");
                mArrUserSelectedKeywordGroupName.add(elem.mKeywordGroupName);
            }
        }

        return mArrUserSelectedKeywordGroupName;
    }

    public void getDataFromDB() throws IOException {
        String strDb = "";
        getDbHdlInst();
        mKwGroup = mDbHdl.getAllDataItemsOfKeywordGroup();
        return;
    }



    //////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////
    private void getDbHdlInst() {
        if (mDbHdl == null) {
            //mDbHdl = new CAppDbHandler(getActivity(), null, null, 1 );
            mDbHdl = ((MainActivity)getActivity()).getDbInstance();
        }
        mDbHdl.getDbInstance();
    }



}
