package com.brainatom.paramind.App.UI;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.speech.tts.TextToSpeech;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.brainatom.paramind.App.Database.CAppDbHandler;
import com.brainatom.paramind.App.Database.CAppDbNewKeywordsGroup;
import com.brainatom.paramind.App.Database.CAppDbNewSearchTargetGroup;
import com.brainatom.paramind.App.Network.CAppNetJsMsgRspMatchedSection;
import com.brainatom.paramind.App.Network.CAppNetRestRespDefault;
import com.brainatom.paramind.App.Network.CAppNetRestRespDefaultV1;
import com.brainatom.paramind.App.Network.CAppNetRestRespDefaultV2;
import com.brainatom.paramind.App.Network.IAppNetRestUserRequest;
import com.brainatom.paramind.App.Setting.CAppGlobalSetting;
import com.brainatom.paramind.App.Setting.EnumStateMachineOfAppTransceiver;
import com.brainatom.paramind.App.Web.CAppWebRetrofit;
import com.brainatom.paramind.App.Web.CAppWebTransceiver;
import com.brainatom.paramind.Platform.Web.CPfRetrofitJsonModel;
import com.brainatom.paramind.Platform.Web.CPfRetrofitJsonModelDeserializer;
import com.brainatom.paramind.Platform.File.CPfFileOperationInternal;
import com.brainatom.paramind.Platform.Nlp.CPfNlpSuit;
import com.brainatom.paramind.Platform.Type.CPfString;
import com.brainatom.paramind.Platform.UI.CPfUiElementaryTag;
import com.brainatom.paramind.Platform.UI.CPfUiTagView;
import com.brainatom.paramind.Platform.Web.CPfWebRestRetrofit;
import com.brainatom.paramind.R;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * A simple {@link Fragment} subclass.
 */
public class CFragmentHome extends Fragment implements IPfNlPVoiceToText{



// CFragmentHome_
// CFragmentHome_interface_
// CFragmentHome_interface_UI_
// CFragmentHome_test_
// CFragmentHome_member_
// CFragmentHome_init_
// CFragmentHome_core_
// CFragmentHome_element_
// CFragmentHome_private_
// CFragmentHome_data_
// CFragmentHome_utility_
// CFragmentHome_reference_
// CFragmentHome_others_
// CFragmentHome_tobe_









// CFragmentHome_{{{

// CFragmentHome_interface_{{{

// }}}

// CFragmentHome_interface_UI_{{{

// }}}

// CFragmentHome_test_{{{

// }}}

// CFragmentHome_member_{{{
    private View mView;
    private CPfUiTagView mTgvTagGroup;

    private ArrayList<CAppTagDataContainer> mTag = null;

    private TextToSpeech                            mTextToSpeech = null;
    private SpeechRecognizer                        mSpeechRecognizer = null;

    RecyclerView mRecyView;
    CRecvDataAdapterSearchResultDefaultview         mDataAdapter;
    SearchView                                      mSearchView = null;
    String                                          mSearchState = "NotStarted";
    // data loaded from buffered results
    ArrayList<CAppRecvItemSearchResult>             mList = new ArrayList<>();
    FloatingActionButton                            mFab = null;

    // rest testing
    private TextView mTextViewResult;
    private IAppNetRestUserRequest mJsonPlaceHolderApi;
    private Button mButStartClassicSearchEngine;
    private Button mButBeginSearchRequest;
    private Button mButRetrieveResultsOfSearch;


// }}}

// CFragmentHome_init_{{{

    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////

    public CFragmentHome() {
        // Required empty public constructor
    }


// }}}

// CFragmentHome_core_{{{

    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setBuildBasicUiFramework(inflater, container);


        setBuildBackendOfUiViewsV1();


        setSimulateRestSearch();


        // data loading
        if (setLoadResponseDataFromDisk()) return mView;


        //setBuildTestingRecvObject();


        //setBuildVoiceEnv();


        return mView;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (menu != null) {
            if (!menu.hasVisibleItems()) {
                setBuildTextListener(menu);
            }
        }
        else {
            menu.clear();
            setBuildTextListener(menu);
        }
    }


    // complete search based on context and voice input
    @Override
    public boolean setProcessResponse(ArrayList<String> candidates) {
        if (candidates.size() > 0  && (mSearchState.equals("Triggered")) ) {
            String primaryCanPredicted = candidates.get(0);

            // get current string in the input control
            String curIn = mSearchView.getQuery().toString();
            if (CPfString.ifSpaceOnlyV2(curIn)) {
                mSearchView.setQuery(primaryCanPredicted, false);
            }
            else {
                if(curIn.charAt( curIn.length() - 1 ) != ' ') {
                    mSearchView.setQuery( curIn + " " + primaryCanPredicted, false);
                }
                else {
                    mSearchView.setQuery( curIn + primaryCanPredicted, false);
                }
            }

            Snackbar.make(mView, "Get feedback" + primaryCanPredicted, Snackbar.LENGTH_LONG).setAction("Action", null).show();
        }
        else {
            Snackbar.make(mView, "Not in edit way", Snackbar.LENGTH_LONG).setAction("Action", null).show();
        }
        return false;
    }





// }}}

// CFragmentHome_element_{{{

// }}}

// CFragmentHome_private_{{{

    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////////////////////////
    // framework
    private void setBuildBasicUiFramework(LayoutInflater inflater, ViewGroup container) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_show_home, container, false);

        setHasOptionsMenu(true);
    }



    //////////////////////////////////////////////////////////////////////////////////
    // backend V1

    private void setBuildBackendOfUiViewsV1() {
        mTextViewResult = (TextView) mView.findViewById(R.id.id_tvTestDisplayer);

        setBuildBackendOfSearchEngineViewV1();
    }

    private void setBuildBackendOfSearchEngineViewV1() {
        mButStartClassicSearchEngine = (Button)mView.findViewById(R.id.id_ButStartingEngineV1);
        mButStartClassicSearchEngine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    setLaunchSearchRequestV1(v);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        mButBeginSearchRequest = (Button)mView.findViewById(R.id.id_ButPostSearch);
        mButBeginSearchRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    setLaunchSearchRequest(v);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        mButRetrieveResultsOfSearch = (Button)mView.findViewById(R.id.id_ButGetSearchResult);
        mButRetrieveResultsOfSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    setRetrieveSearchResult(v);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void setLaunchSearchRequestV1(View v) throws IOException {
        CAppDbHandler dbHdl = new CAppDbHandler(getActivity(), null, null, 1);
        ArrayList<CAppDbNewKeywordsGroup> kwGroup = dbHdl.getAllDataItemsOfKeywordGroup();
        ArrayList<CAppDbNewSearchTargetGroup> targetGroup = dbHdl.getAllDataItemsOfSearchTargetGroup();

        // if need to update it
        //setSendMsgUpdateAllInterests(targetGroup, kwGroup);


        //CAppWebTransceiver mWebAct = new CAppWebTransceiver("https://en.wikipedia.org/wiki/Machine_learning");
        CAppWebTransceiver mWebAct = new CAppWebTransceiver(getContext(), targetGroup, kwGroup, 2);
        mWebAct.execute("");


        Toast.makeText(this.getContext(), "Your search engine is working now!", Toast.LENGTH_SHORT).show();
    }

    private void setLaunchSearchRequest(View v) throws IOException {
        //setSearchOverObj();
        CAppDbHandler dbHdl = new CAppDbHandler(getActivity(), null, null, 1);
        CAppWebTransceiver mWebAct = new CAppWebTransceiver(
            getContext(),
            dbHdl.getAllDataItemsOfSearchTargetGroup(), dbHdl.getAllDataItemsOfKeywordGroup(),
            2);
        mWebAct.setWorkFlow(EnumStateMachineOfAppTransceiver.RETRO_REQUESTSEARCH);
        mWebAct.execute("");

        Toast.makeText(this.getContext(), "Your search engine is working now!", Toast.LENGTH_SHORT).show();
    }

    private void setRetrieveSearchResult(View v) throws IOException {
        setFetchResult();
        Toast.makeText(this.getContext(), "Search results are on the way now!", Toast.LENGTH_SHORT).show();
    }

    private boolean setLoadResponseDataFromDisk() {
        Map<String, CAppNetJsMsgRspMatchedSection> objcatTwin = getSearchResultsFromBufferFile();
        if (objcatTwin == null) {
            return true;
        }

        setFillupDataContainer(objcatTwin);
        return false;
    }

    // reading data from buffered search result file
    @Nullable
    private Map<String, CAppNetJsMsgRspMatchedSection> getSearchResultsFromBufferFile() {
        CPfFileOperationInternal fo = new CPfFileOperationInternal();
        String searchRes = null;
        Map<String, CAppNetJsMsgRspMatchedSection> objcatTwin = null;

        try {
            searchRes = fo.readFile("bufsearchresult", getContext());

            Gson gson = new Gson();
            Type type = new TypeToken<Map<String, CAppNetJsMsgRspMatchedSection>>(){}.getType();
            objcatTwin = gson.fromJson(searchRes, type);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        Log.d("SearchResults", "Search results are accessed");

        return objcatTwin;
    }


    // inflate data containers for further processing
    private void setFillupDataContainer(Map<String, CAppNetJsMsgRspMatchedSection> objcatTwin) {
        mList = new ArrayList<>();

        // convert data to list mode
        for(Map.Entry<String, CAppNetJsMsgRspMatchedSection> entry: objcatTwin.entrySet()) {
            String key = entry.getKey();
            CAppNetJsMsgRspMatchedSection value = entry.getValue();

            CAppRecvItemSearchResult item = new CAppRecvItemSearchResult();

            String content = value.getmContent();
            String[] lsRes = content.split(" ", -1);
            int iUpperLimitOfDispSection = CAppGlobalSetting.getMaxNrOfWordsInDisplay();
            if(iUpperLimitOfDispSection < lsRes.length) {
                StringBuilder nsb = new StringBuilder();
                for (int i=0; i < iUpperLimitOfDispSection; i++) {
                    nsb.append(lsRes[i] + " ");
                }
                item.setItemContent(nsb.toString());
            }
            else {
                item.setItemContent(content);
            }
            item.setItemContentFull(content);

            item.setDatetime(value.getmTime());
            item.setBelongUrl(value.getmUrl());


            String testurl = value.getmIconUrl();
            item.setHeadImageUrl(testurl);

            //item.setHeadImageUrl(strUrlOfIcon);
            mList.add(item);
        }
    }



    //////////////////////////////////////////////////////////////////////////////////
    // rest search framework

    private void setSimulateRestSearch() {
        // search request simulation{{{
        boolean bIfContinue = true;

        try {
            mJsonPlaceHolderApi = CAppWebRetrofit.setBuildDefaultRetrofitObjectV2();
        } catch (IOException e) {
            e.printStackTrace();
            bIfContinue = false;
        }


        // http actions
        if (bIfContinue) {
            //setPostSearchRequest();
            //getSearchResult();
            //getSearchResultByCommon();
            //getSearchResultByCommonV1();

            //setPostUserSetting();
            //setPostStoreUserData();
            //setSearchOverObj();
            //setFetchResult();
        }

        //}}}
    }


    private void setPostSearchRequest() {
        Call<CAppNetRestRespDefault> call = mJsonPlaceHolderApi.setSearchPostDefault("root", setBuildPostDataV1());
        //Call<CAppNetRestRespDefault> call = mJsonPlaceHolderApi.setSearchPostV1("cljewoiclxjksljiowejoi");
        //Call<CAppNetRestRespDefault> call = mJsonPlaceHolderApi.setSearchPostV1("root", "cljewoiclxjksljiowejoi");
        //Call<CAppNetRestRespDefault> call = mJsonPlaceHolderApi.setSearchPost("root", "Oakland", "Content");
        //Call<CAppNetRestRespDefault> call = mJsonPlaceHolderApi.setSearchPostDefault(setBuildPostDataV1());
        //Call<CAppNetRestRespDefault> call = mJsonPlaceHolderApi.setSearchPostDefault("root");

        call.enqueue(new Callback<CAppNetRestRespDefault>() {
            @Override
            public void onResponse(Call<CAppNetRestRespDefault> call, Response<CAppNetRestRespDefault> response) {
                if (!response.isSuccessful()) {
                    mTextViewResult.setText("Code: " + response.code());
                    return;
                }

                //String res = response.body().toString();
                CAppNetRestRespDefault posts = response.body();
            }

            @Override
            public void onFailure(Call<CAppNetRestRespDefault> call, Throwable t) {
                mTextViewResult.setText(t.getMessage());
            }
        });
    }


    // deserialize fixing json obj
    private void getSearchResult() {
        Call<CAppNetRestRespDefaultV1> call = mJsonPlaceHolderApi.getSearchResultDefaultV1("root");
        //Call<CAppNetRestPostBasicSearchReq> call = mJsonPlaceHolderApi.getSearchResultDefault("root");


        call.enqueue(new Callback<CAppNetRestRespDefaultV1>() {
            @Override
            public void onResponse(Call<CAppNetRestRespDefaultV1> call, Response<CAppNetRestRespDefaultV1> response) {
                if (!response.isSuccessful()) {
                    mTextViewResult.setText("Code: " + response.code());
                    return;
                }

                CAppNetRestRespDefaultV1 posts = response.body();


                // String json = response.body().string();
                // //String json = response.body().toString();
                // Gson gson = new Gson();
                // CAppNetRestRespDefaultV2 obj = gson.fromJson(json, CAppNetRestRespDefaultV2.class);
            }

            @Override
            public void onFailure(Call<CAppNetRestRespDefaultV1> call, Throwable t) {

                mTextViewResult.setText(t.getMessage());
            }
        });
    }


    // deserialize dictionary json obj
    private void getSearchResultByCommon() {
        Call<ResponseBody> call = mJsonPlaceHolderApi.getSearchResultDefaultV2("root");
        //Call<CAppNetRestPostBasicSearchReq> call = mJsonPlaceHolderApi.getSearchResultDefault("root");


        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (!response.isSuccessful()) {
                    mTextViewResult.setText("Code: " + response.code());
                    return;
                }

                try {
                    String posts = response.body().string();
                    Gson gson = new Gson();
                    CAppNetRestRespDefaultV2 obj = gson.fromJson(posts, CAppNetRestRespDefaultV2.class);
                } catch (IOException e) {
                    e.printStackTrace();
                }


                // String json = response.body().string();
                // //String json = response.body().toString();
                // Gson gson = new Gson();
                // CAppNetRestRespDefaultV2 obj = gson.fromJson(json, CAppNetRestRespDefaultV2.class);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                mTextViewResult.setText(t.getMessage());
            }
        });
    }


    // deserialize any json obj
    private void getSearchResultByCommonV1() {
        Call<ResponseBody> call = mJsonPlaceHolderApi.getSearchResultDefaultV2("root");
        //Call<CAppNetRestPostBasicSearchReq> call = mJsonPlaceHolderApi.getSearchResultDefault("root");


        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (!response.isSuccessful()) {
                    mTextViewResult.setText("Code: " + response.code());
                    return;
                }

                try {
                    String posts = response.body().string();
                    JSONObject jsonObject;
                    JSONArray jsonArrObject;
                    try {
                        boolean bIfArrayObj = true;
                        if (bIfArrayObj) {
                            jsonArrObject = new JSONArray(posts);

                            Log.d("Result", jsonArrObject.toString() );
                        }
                        else {
                            jsonObject = new JSONObject(posts);
                            String res = jsonObject.getString("root");
                            Log.d("Result", res);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        return;
                    }


                    // Gson gson = new Gson();
                    // CAppNetRestRespDefaultV2 obj = gson.fromJson(posts, CAppNetRestRespDefaultV2.class);
                } catch (IOException e) {
                    e.printStackTrace();
                }


                // String json = response.body().string();
                // //String json = response.body().toString();
                // Gson gson = new Gson();
                // CAppNetRestRespDefaultV2 obj = gson.fromJson(json, CAppNetRestRespDefaultV2.class);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                mTextViewResult.setText(t.getMessage());
            }
        });
    }


    private void setPostUserSetting() {
        Call<ResponseBody> call = mJsonPlaceHolderApi.setPostUserSetting("root");

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (!response.isSuccessful()) {
                    mTextViewResult.setText("Code: " + response.code());
                    return;
                }

                try {
                    String posts = response.body().string();
                    JSONObject jsonObject;
                    JSONArray jsonArrObject;
                    try {
                        boolean bIfArrayObj = true;
                        if (bIfArrayObj) {
                            jsonArrObject = new JSONArray(posts);

                            Log.d("Result", jsonArrObject.toString() );
                        }
                        else {
                            jsonObject = new JSONObject(posts);
                            String res = jsonObject.getString("root");
                            Log.d("Result", res);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        return;
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                mTextViewResult.setText(t.getMessage());
            }
        });
    }


    private void setPostStoreUserData() {
        Call<ResponseBody> call = mJsonPlaceHolderApi.setPostStoreUserData("root");

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (!response.isSuccessful()) {
                    mTextViewResult.setText("Code: " + response.code());
                    return;
                }

                try {
                    String posts = response.body().string();
                    JSONObject jsonObject;
                    JSONArray jsonArrObject;
                    try {
                        boolean bIfArrayObj = true;
                        if (bIfArrayObj) {
                            jsonArrObject = new JSONArray(posts);

                            Log.d("Result", jsonArrObject.toString() );
                        }
                        else {
                            jsonObject = new JSONObject(posts);
                            String res = jsonObject.getString("root");
                            Log.d("Result", res);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        return;
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                mTextViewResult.setText(t.getMessage());
            }
        });
    }


    // core functions
    private void setSearchOverObj() {
        Call<ResponseBody> call = mJsonPlaceHolderApi.setPostSearchOverObj("root", setBuildSearchRequest());

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (!response.isSuccessful()) {
                    mTextViewResult.setText("Code: " + response.code());
                    return;
                }

                try {
                    String posts = response.body().string();
                    JSONObject jsonObject;
                    JSONArray jsonArrObject;
                    try {
                        boolean bIfArrayObj = true;
                        if (bIfArrayObj) {
                            jsonArrObject = new JSONArray(posts);

                            Log.d("Result", jsonArrObject.toString() );
                        }
                        else {
                            jsonObject = new JSONObject(posts);
                            String res = jsonObject.getString("root");
                            Log.d("Result", res);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        return;
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                mTextViewResult.setText(t.getMessage());
            }
        });
    }


    private void setFetchResult() {
        Call<ResponseBody> call = mJsonPlaceHolderApi.getFetchResult("root");

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (!response.isSuccessful()) {
                    mTextViewResult.setText("Code: " + response.code());
                    return;
                }

                try {
                    String posts = response.body().string();
                    JSONObject jsonObject;
                    JSONArray jsonArrObject;
                    try {
                        boolean bIfArrayObj = true;
                        if (bIfArrayObj) {
                            jsonArrObject = new JSONArray(posts);

                            Log.d("Result", jsonArrObject.toString() );
                        }
                        else {
                            jsonObject = new JSONObject(posts);
                            String res = jsonObject.getString("root");
                            Log.d("Result", res);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        return;
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                mTextViewResult.setText(t.getMessage());
            }
        });
    }

    // simulating body data for posting
    @NonNull
    private HashMap<String, String> setBuildPostDataV1() {
        HashMap<String, String> fields = new HashMap<>();
        fields.put("userId", "25");
        fields.put("title", "New Title");
        return fields;
    }

    @NonNull
    private HashMap<String, String> setBuildFetchResultRequest() {
        HashMap<String, String> fields = new HashMap<>();

        fields.put("userId", "");
        fields.put("Cookie", "");
        fields.put("SessionId", "");
        fields.put("SearchId", "");
        return fields;
    }

    @NonNull
    private HashMap<String, String> setBuildSearchRequest() {
        HashMap<String, String> fields = new HashMap<>();

        fields.put("userId", "");
        fields.put("Cookie", "");
        fields.put("SessionId", "");
        fields.put("DataForSearch", "");

        return fields;
    }



    //////////////////////////////////////////////////////////////////////////////////
    // recv disp on search results

    private void setBuildTestingRecvObject() {
        //mRecyView = mView.findViewById(R.id.id_recvSearchResult_default);
        mRecyView.setLayoutManager(new LinearLayoutManager( this.getActivity() ));
        //mRecyView.setLayoutManager(new LinearLayoutManager( this.getActivity(), LinearLayoutManager.VERTICAL, false ));
        //mRecyView.setLayoutManager(new LinearLayoutManager(this));

        mDataAdapter = new CRecvDataAdapterSearchResultDefaultview(mList, getActivity());
        mRecyView.setAdapter(mDataAdapter);
    }


    //////////////////////////////////////////////////////////////////////////////////
    // tag color testing

    private void setTestColorTag() {
        //mTgvTagGroup = (CPfUiTagView)mView.findViewById(R.id.id_taggroup);

        ArrayList<CPfUiElementaryTag> lsTagContainer = new ArrayList<>();

        mTag = new ArrayList<CAppTagDataContainer>();
        mTag.add(new CAppTagDataContainer("romario"));
        mTag.add(new CAppTagDataContainer("kansikaao"));
        mTag.add(new CAppTagDataContainer("eoi2389"));
        mTag.add(new CAppTagDataContainer("oeiwoew839"));
        mTag.add(new CAppTagDataContainer("woei23"));
        mTag.add(new CAppTagDataContainer("9jwejwoie"));
        mTag.add(new CAppTagDataContainer("2938jdijowjeo"));

        CPfUiElementaryTag tag;
        for (int i = 0; i < mTag.size(); i++) {
            tag = new CPfUiElementaryTag(mTag.get(i).getName(), Color.parseColor(mTag.get(i).getColor()), false);

            // tag = new CPfUiElementaryTag(mTag.get(i).getName());
            // tag.setLayoutColor(Color.parseColor(mTag.get(i).getColor()));
            // tag.setRadius(10f);
            // tag.setDeletable(false);

            lsTagContainer.add(tag);
        }

        mTgvTagGroup.addTags(lsTagContainer);
    }


    ///////////////////////////////////////////////////////////////////////////////////
    // NLP
    private void setActivateSpeechRecognizer() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);

        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM );
        intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 1);
        intent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, getActivity().getApplication().getPackageName());

        mSpeechRecognizer.startListening(intent);
    }


    private void setBuildVoiceEnv() {
        //mFab = (FloatingActionButton) mView.findViewById(R.id.id_FabVoice);
        final IPfNlPVoiceToText ifReference = this;
        mFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String parentAct = getActivity().getClass().getSimpleName();
                if (parentAct.equals("MainActivity")) {
                    MainActivity mainAct = (MainActivity) getActivity();


                    CPfNlpSuit ret = mainAct.setBuildBondOfNLP(ifReference);
                    if(!ret.mState) {
                        Snackbar.make(view, "Not able to get main resources of microphone", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                        return;
                    }
                    mSpeechRecognizer = ret.getSpeechRecognizer();
                    mTextToSpeech = ret.getTextToSpeech();

                }
                setActivateSpeechRecognizer();
            }
        });
    }


    private void setBuildTextListener(Menu menu) {
        getActivity().getMenuInflater().inflate(R.menu.menu_homeitems, menu);
        MenuItem menuItem = menu.findItem(R.id.id_searchtarget_group_item_search);

        mSearchView = (SearchView) MenuItemCompat.getActionView(menuItem);
        mSearchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSearchState = "Triggered";
            }
        });

        mSearchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                mSearchState = "NotStarted";
                return false;
            }
        });

        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (mDataAdapter != null) {
                    mDataAdapter.getFilter().filter(newText);
                }

                return true;
            }
        });

        return;
    }




// }}}

// CFragmentHome_data_{{{

// }}}

// CFragmentHome_utility_{{{

// }}}

// CFragmentHome_reference_{{{

// }}}

// CFragmentHome_others_{{{

// }}}

// CFragmentHome_tobe_{{{

// }}}

// }}}






}
