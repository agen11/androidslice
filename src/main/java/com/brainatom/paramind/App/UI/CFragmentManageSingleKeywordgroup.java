package com.brainatom.paramind.App.UI;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.brainatom.paramind.App.Database.CAppDbHandler;
import com.brainatom.paramind.App.Database.CAppDbNewKeywordsGroup;
import com.brainatom.paramind.Platform.Type.CPfString;
import com.brainatom.paramind.R;
import com.daimajia.swipe.util.Attributes;

import java.io.IOException;
import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class CFragmentManageSingleKeywordgroup extends Fragment{
    private ArrayList<CAppRecvItemKeywordGroupFullmatch> mDataFmSet = null;
    private ArrayList<CAppRecvItemKeywordGroupPartmatch> mDataPmSet = null;

    CRecvDataAdapterFullmatchItemV1 mFmadapter = null;
    CRecvDataAdapterPartmatchItemV1 mPmadapter = null;

    private String mNameOfKwGroup;

    private TextView mTvNameOfKwGroup;

    private RecyclerView mFmKwRecyclerView;
    private RecyclerView mPmKwRecyclerView;

    private TextView mTvEmptyFmTextView;
    private TextView mTvEmptyPmTextView;

    private ImageView mIvFmKwAddnew;
    private ImageView mIvPmKwAddnew;

    private Button mButDeleteGroup;

    private View mView;

    private CIHandlerOnAddNewToFmSet mHandlerOnProcessNewFm;
    private CIHandlerOnAddNewToPmSet mHandlerOnProcessNewPm;

    CPfUiAndroidMsg mMsg = null;
    CAppDbHandler mDbHdl = null;

    CPfString mStrFac = null;

    //////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////
    public CFragmentManageSingleKeywordgroup() {
        // Required empty public constructor
    }


    //////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_mng_single_keywordgroup, container, false);


        setLinkRecvElement();
        //setLoadingData();
        setSelectiveDisp();


        // adaptor building
        mFmadapter = new CRecvDataAdapterFullmatchItemV1(this.getContext(), mDataFmSet);
        mFmadapter.setMode(Attributes.Mode.Single);

        mPmadapter = new CRecvDataAdapterPartmatchItemV1(this.getContext(), mDataPmSet);
        mPmadapter.setMode(Attributes.Mode.Single);

        // link adapter with recy view
        mFmKwRecyclerView.setAdapter(mFmadapter);
        mPmKwRecyclerView.setAdapter(mPmadapter);

        mTvNameOfKwGroup.setText(mNameOfKwGroup);
        mFmadapter.setGroupName(mNameOfKwGroup);
        mPmadapter.setGroupName(mNameOfKwGroup);


        mIvFmKwAddnew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String title = "Please give a new keyword for full matching";
                String content = "";

                getMsgUi();
                getInstOfNewFullMatch();
                mMsg.setPopupEditBox(title, content, getContext(), mHandlerOnProcessNewFm);
            }
        });

        mIvPmKwAddnew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String title = "Please give a new keyword for partial matching";
                String content = "";

                getMsgUi();
                getInstOfNewPartMatch();
                mMsg.setPopupEditBox(title, content, getContext(), mHandlerOnProcessNewPm);
            }
        });

        mButDeleteGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDbInstance();
                mDbHdl.setDeleteKeywordGroupitemViaQueryViaName(mNameOfKwGroup);
                mDbHdl.closeInstance();

                getFragmentManager().popBackStack();
            }
        });

        //setAddGestureListener();

        return mView;
    }


    @NonNull
    private String setFormFullmatchString() {
        String strFullmatch = "";
        for (int i = 0; i < mDataFmSet.size(); i++) {
            strFullmatch += mDataFmSet.get(i).getKeywordName() + ",";
        }

        if (strFullmatch.length() > 0 && strFullmatch.charAt(strFullmatch.length() - 1) == ',') {
            strFullmatch = strFullmatch.substring(0, strFullmatch.length() - 1);
        }
        return strFullmatch;
    }


    @NonNull
    private String setFormPartmatchString() {
        String strPartmatch = "";
        for (int i = 0; i < mDataPmSet.size(); i++) {
            strPartmatch += mDataPmSet.get(i).getKeywordName() + ",";
        }

        if (strPartmatch.length() > 0 && strPartmatch.charAt(strPartmatch.length() - 1) == ',') {
            strPartmatch = strPartmatch.substring(0, strPartmatch.length() - 1);
        }
        return strPartmatch;
    }


    public class CIHandlerOnAddNewToFmSet implements IPfUiActOkCancel{

        @Override
        public boolean setExecActOnOk(Object data) {
            getDbInstance();

            String strFullmatch = getFullmatchString((String) data);
            String strPartmatch = getPartMatchString((String) data);

            CAppDbNewKeywordsGroup newKeywordsGroup2 = new CAppDbNewKeywordsGroup(
                mNameOfKwGroup,
                getStrFac().setAddJoinedStringByToken(strFullmatch, ","),
                getStrFac().setAddJoinedStringByToken(strPartmatch, ","), 1);
            mDbHdl.setUpdateKeywordGroupFm(newKeywordsGroup2);

            try {
                setCheckPointOfDBResult(mDbHdl);
            } catch (IOException e) {
                e.printStackTrace();
            }

            //mDbHdl.close();
            mDbHdl.closeInstance();

            mDataFmSet.add(new CAppRecvItemKeywordGroupFullmatch((String)data));
            mFmadapter.setArrFmKeywords(mDataFmSet);
            mFmadapter.notifyDataSetChanged();

            setSelectiveDisp();

            return true;
        }

        @Override
        public boolean setExecActOnCancel() {
            return false;
        }

        @Override
        public boolean setExecAct() {
            return false;
        }
    }


    public class CIHandlerOnAddNewToPmSet implements IPfUiActOkCancel{

        @Override
        public boolean setExecActOnOk(Object data) {
            getDbInstance();

            String strFullmatch = getFullmatchString((String) data);
            String strPartmatch = getPartMatchString((String) data);

            CAppDbNewKeywordsGroup newKeywordsGroup2 = new CAppDbNewKeywordsGroup(
                mNameOfKwGroup,
                getStrFac().setAddJoinedStringByToken(strFullmatch, ","),
                getStrFac().setAddJoinedStringByToken(strPartmatch, ","), 1);
            mDbHdl.setUpdateKeywordGroupPm(newKeywordsGroup2);

            try {
                setCheckPointOfDBResult(mDbHdl);
            } catch (IOException e) {
                e.printStackTrace();
            }

            mDbHdl.closeInstance();

            mDataPmSet.add(new CAppRecvItemKeywordGroupPartmatch((String)data));
            mPmadapter.setArrPmKeywords(mDataPmSet);
            mPmadapter.notifyDataSetChanged();

            setSelectiveDisp();

            return true;
        }

        @Override
        public boolean setExecActOnCancel() {
            return false;
        }

        @Override
        public boolean setExecAct() {
            return false;
        }
    }

    @NonNull
    private String getFullmatchString(String data) {
        String strCurFmString = setFormFullmatchString();
        return (strCurFmString.length() > 0) ? strCurFmString + "," + data : data + "," + data;
    }

    private String getPartMatchString(String data) {
        String strCurPmString = setFormPartmatchString();
        return (strCurPmString.length() > 0) ? strCurPmString + "," + data : data;
    }


    //////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////

    // for simulation only
    private void setLoadingData() {
        // data loading
        mDataFmSet = new ArrayList<>();
        loadData();
    }

    private void setLoadFullmatchData() {

    }

    private void setLoadPartmatchData() {

    }

    public void loadData() {
        setLoadDummyData();
    }

    private void setLoadDummyData() {
        for (int i = 0; i <= 20; i++) {
            mDataFmSet.add(new CAppRecvItemKeywordGroupFullmatch("Romario " + i));
        }
    }

    private void setSelectiveDisp() {
        // selective showing
        if(mDataFmSet.isEmpty()){
            mFmKwRecyclerView.setVisibility(View.GONE);
            mTvEmptyFmTextView.setVisibility(View.VISIBLE);
        }
        else{
            mFmKwRecyclerView.setVisibility(View.VISIBLE);
            mTvEmptyFmTextView.setVisibility(View.GONE);
        }


        // selective showing
        if(mDataPmSet.isEmpty()){
            mPmKwRecyclerView.setVisibility(View.GONE);
            mTvEmptyPmTextView.setVisibility(View.VISIBLE);
        }
        else{
            mPmKwRecyclerView.setVisibility(View.VISIBLE);
            mTvEmptyPmTextView.setVisibility(View.GONE);
        }
    }


    private void setLinkRecvElement() {
        mTvNameOfKwGroup = (TextView) mView.findViewById(R.id.id_sKeywordGroupName);

        // fm
        mTvEmptyFmTextView = (TextView) mView.findViewById(R.id.id_empty_view_fmkw);

        mFmKwRecyclerView = (RecyclerView) mView.findViewById(R.id.id_recv_fmkeyword_mark);
        mFmKwRecyclerView.setLayoutManager(new LinearLayoutManager(this.getActivity()));


        // pm
        mTvEmptyPmTextView = (TextView) mView.findViewById(R.id.id_empty_view_pmkw);

        mPmKwRecyclerView = (RecyclerView) mView.findViewById(R.id.id_recv_pmkeyword_mark);
        mPmKwRecyclerView.setLayoutManager(new LinearLayoutManager(this.getActivity()));


        mIvFmKwAddnew = (ImageView)mView.findViewById(R.id.id_IvAddNewToFullmatch);
        mIvPmKwAddnew = (ImageView)mView.findViewById(R.id.id_IvAddNewToPartmatch);

        mButDeleteGroup = (Button)mView.findViewById(R.id.id_ButDeleteGroupOfKeyword);
    }


    private void setAddGestureListener() {
        final GestureDetector gestureDetector = new GestureDetector(getActivity(),
                new GestureDetector.SimpleOnGestureListener() {
                    @Override
                    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                        Toast.makeText(getContext(), "Get your message!", Toast.LENGTH_SHORT).show();
                        return super.onFling(e1, e2, velocityX, velocityY);
                    }

                    @Override
                    public boolean onDown(MotionEvent e) {
                        Toast.makeText(getContext(), "Get your message!", Toast.LENGTH_SHORT).show();
                        return true;
                    }
                }
        );

        mView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return gestureDetector.onTouchEvent(event);
            }
        });
    }


    private void setCheckPointOfDBResult(CAppDbHandler dbHdl) throws IOException {
        ArrayList<CAppDbNewKeywordsGroup> result1 = dbHdl.getAllDataItemsOfKeywordGroup();
        Log.d("DBKeyword", "check result");
    }


    //////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////


    public String getNameOfKwGroup() {
        return mNameOfKwGroup;
    }

    public void setNameOfKwGroup(String mNameOfKwGroup) {
        this.mNameOfKwGroup = mNameOfKwGroup;
    }

    public ArrayList<CAppRecvItemKeywordGroupFullmatch> getmDataFmSet() {
        return mDataFmSet;
    }

    public void setDataFmSet(ArrayList<CAppRecvItemKeywordGroupFullmatch> mDataFmSet) {
        this.mDataFmSet = mDataFmSet;
    }

    public ArrayList<CAppRecvItemKeywordGroupPartmatch> getmDataPmSet() {
        return mDataPmSet;
    }

    public void setmDataPmSet(ArrayList<CAppRecvItemKeywordGroupPartmatch> mDataPmSet) {
        this.mDataPmSet = mDataPmSet;
    }

    public TextView getmTvEmptyPmTextView() {
        return mTvEmptyPmTextView;
    }

    public void setmTvEmptyPmTextView(TextView mTvEmptyPmTextView) {
        this.mTvEmptyPmTextView = mTvEmptyPmTextView;
    }

    public RecyclerView getmPmKwRecyclerView() {
        return mPmKwRecyclerView;
    }

    public void setmPmKwRecyclerView(RecyclerView mPmKwRecyclerView) {
        this.mPmKwRecyclerView = mPmKwRecyclerView;
    }

    private CPfUiAndroidMsg getMsgUi() {
        if(mMsg == null) {
            mMsg = new CPfUiAndroidMsg();
        }
        return mMsg;
    }

    private void getInstOfNewFullMatch() {
        if(mHandlerOnProcessNewFm == null) {
            mHandlerOnProcessNewFm = new CIHandlerOnAddNewToFmSet();
        }
    }

    private void getInstOfNewPartMatch() {
        if(mHandlerOnProcessNewPm == null) {
            mHandlerOnProcessNewPm = new CIHandlerOnAddNewToPmSet();
        }
    }

    private CAppDbHandler getDbInstance() {
        if (mDbHdl == null) {
            mDbHdl = ((MainActivity)getActivity()).getDbInstance();
        }
        mDbHdl.getDbInstance();
        return mDbHdl;
    }

    private CPfString getStrFac() {
        if(mStrFac == null) {
            mStrFac = new CPfString();
        }
        return mStrFac;
    }


}
