package com.brainatom.paramind.App.UI;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.brainatom.paramind.App.Network.CAppNetJsMsgRspMatchedSection;
import com.brainatom.paramind.App.Setting.CAppGlobalSetting;
import com.brainatom.paramind.Platform.File.CPfFileOperationInternal;
import com.brainatom.paramind.Platform.Web.CPfWebAnalyzer;
import com.brainatom.paramind.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class CFragmentResultview extends Fragment {

    private RecyclerView mRecyViewFullset;
    private RecyclerView.Adapter mAdaptor;

    private RecyclerView mRecyViewAllmatchSet;
    private RecyclerView mRecyViewLatestSet;
    private RecyclerView mRecyViewSiteSet;
    private RecyclerView mRecyViewInterestSet;

    private ArrayList<CAppRecvItemSearchResult> mAListSearchResult = null;

    /////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////

    public CFragmentResultview() {
        // Required empty public constructor
    }

    /////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mng_resultview,
            container, false);

        // menu
        setHasOptionsMenu(true);

        if (setLoadingSearchingData(view)) return view;


        setBuildFullsetRecv(view);
        setBuildFullmatchRecv(view);
        setBuildLatestmatchRecv(view);
        setBuildSitesetRecv(view);
        setBuildInterestsetRecv(view);

        return view;
    }

    private void setBuildFullsetRecv(View view) {
        mRecyViewFullset = view.findViewById(R.id.id_recvwFullsetSearchResult);

        mRecyViewFullset.setLayoutManager(new LinearLayoutManager( this.getActivity(), LinearLayoutManager.HORIZONTAL, false ));

        mAdaptor = new CRecvDataAdapterSearchResult(mAListSearchResult, getActivity());
        mRecyViewFullset.setAdapter(mAdaptor);
    }

    private void setBuildFullmatchRecv(View view) {
        mRecyViewAllmatchSet = view.findViewById(R.id.id_recvwSearchResultAllmatch);

        mRecyViewAllmatchSet.setLayoutManager(new LinearLayoutManager( this.getActivity(), LinearLayoutManager.HORIZONTAL, false ));

        mAdaptor = new CRecvDataAdapterSearchResult(mAListSearchResult, getActivity());
        mRecyViewAllmatchSet.setAdapter(mAdaptor);
    }

    private void setBuildLatestmatchRecv(View view) {
        mRecyViewLatestSet = view.findViewById(R.id.id_recvwSearchResultLatest);

        mRecyViewLatestSet.setLayoutManager(new LinearLayoutManager( this.getActivity(), LinearLayoutManager.HORIZONTAL, false ));

        mAdaptor = new CRecvDataAdapterSearchResult(mAListSearchResult, getActivity());
        mRecyViewLatestSet.setAdapter(mAdaptor);
    }

    private void setBuildSitesetRecv(View view) {
        mRecyViewSiteSet = view.findViewById(R.id.id_recvwSearchResultBySite);

        mRecyViewSiteSet.setLayoutManager(new LinearLayoutManager( this.getActivity(), LinearLayoutManager.HORIZONTAL, false ));

        mAdaptor = new CRecvDataAdapterSearchResult(mAListSearchResult, getActivity());
        mRecyViewSiteSet.setAdapter(mAdaptor);
    }

    private void setBuildInterestsetRecv(View view) {
        mRecyViewInterestSet = view.findViewById(R.id.id_recvwSearchResultByInterest);

        mRecyViewInterestSet.setLayoutManager(new LinearLayoutManager( this.getActivity(), LinearLayoutManager.HORIZONTAL, false ));

        mAdaptor = new CRecvDataAdapterSearchResult(mAListSearchResult, getActivity());
        mRecyViewInterestSet.setAdapter(mAdaptor);
    }


    private boolean setLoadingSearchingData(View view) {
        // reading file
        Map<String, CAppNetJsMsgRspMatchedSection> objcatTwin = getSearchResultsFromBufferFile();
        if (objcatTwin == null) {
            return true;
        }

        setFillupDataContainer(objcatTwin);
        return false;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if ( menu != null ) {
            if ( !menu.hasVisibleItems() ) {
                getActivity().getMenuInflater().inflate(R.menu.menu_edit_listitem_searchtargetgroup, menu);
            }
        }
        return;
    }


    /////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////

    private void setFillupDataContainer(Map<String, CAppNetJsMsgRspMatchedSection> objcatTwin) {
        mAListSearchResult = new ArrayList<>();
        CPfWebAnalyzer webAnalyzer = new CPfWebAnalyzer();

        for(Map.Entry<String, CAppNetJsMsgRspMatchedSection> entry: objcatTwin.entrySet()) {
            String key = entry.getKey();
            CAppNetJsMsgRspMatchedSection value = entry.getValue();

            CAppRecvItemSearchResult item = new CAppRecvItemSearchResult();

            String content = value.getmContent();
            String[] lsRes = content.split(" ", -1);
            int iUpperLimitOfDispSection = CAppGlobalSetting.getMaxNrOfWordsInDisplay();
            if(iUpperLimitOfDispSection < lsRes.length) {
                StringBuilder nsb = new StringBuilder();
                for (int i=0; i < iUpperLimitOfDispSection; i++) {
                    nsb.append(lsRes[i] + " ");
                }
                item.setItemContent(nsb.toString());
            }
            else {
                item.setItemContent(content);
            }
            item.setItemContentFull(content);

            item.setDatetime(value.getmTime());
            item.setBelongUrl(value.getmUrl());


            String testurl = value.getmIconUrl();
            item.setHeadImageUrl(testurl);

            //item.setHeadImageUrl(strUrlOfIcon);
            mAListSearchResult.add(item);
        }
    }

    @Nullable
    private Map<String, CAppNetJsMsgRspMatchedSection> getSearchResultsFromBufferFile() {
        CPfFileOperationInternal fo = new CPfFileOperationInternal();
        String searchRes = null;
        Map<String, CAppNetJsMsgRspMatchedSection> objcatTwin = null;
        try {
            searchRes = fo.readFile("bufsearchresult", getContext());

            Gson gson = new Gson();
            Type type = new TypeToken<Map<String, CAppNetJsMsgRspMatchedSection>>(){}.getType();
            objcatTwin = gson.fromJson(searchRes, type);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        Log.d("SearchResults", "Search results are accessed");
        return objcatTwin;
    }

}



