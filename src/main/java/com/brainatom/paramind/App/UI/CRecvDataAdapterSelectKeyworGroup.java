package com.brainatom.paramind.App.UI;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.brainatom.paramind.App.Database.CAppDbNewKeywordsGroup;
import com.brainatom.paramind.R;

import java.util.ArrayList;
import java.util.List;


public class CRecvDataAdapterSelectKeyworGroup extends RecyclerView.Adapter<CRecvDataAdapterSelectKeyworGroup.ViewHolder> {

    private List<CAppDbNewKeywordsGroup> listItems;

    private Context context;


    ///////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////

    public CRecvDataAdapterSelectKeyworGroup(List<CAppDbNewKeywordsGroup> listItems, Context context) {
        this.listItems = listItems;
        this.context = context;
    }

    public CRecvDataAdapterSelectKeyworGroup(ArrayList<CAppDbNewKeywordsGroup> mRecvData, Context context) {
        this.listItems = mRecvData;
        this.context = context;
    }


    ///////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        // link elementary item of recyclerview
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recvitem_select_keyword_group, viewGroup, false);

        // link disp element with data with UI data controller
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int i) {
        final int position = i;
        final CAppDbNewKeywordsGroup dataKwGroup = listItems.get(i);
        final int iCheckedColor = Color.parseColor("#4db051");
        viewHolder.position = i;

        setUpdateUiElementWithDataBase(viewHolder, dataKwGroup);

        viewHolder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean curState = !viewHolder.cbItemCheckBox.isChecked();

                viewHolder.cbItemCheckBox.setChecked(curState);
                //viewHolder.mRecvItemView.setBackgroundColor(curState?iCheckedColor : Color.TRANSPARENT);

                listItems.get(position).setSelected(curState);
            }
        });

        viewHolder.cbItemCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                int position = viewHolder.position;
                //buttonView.setChecked(!isChecked);
                listItems.get(position).setSelected(isChecked);
            }
        });

    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }




    ///////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////



    // build connection to xml from here
    // link data to elementary item of UI of each data item
    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView tvItemTitle;
        public TextView tvItemDetail;
        public CheckBox cbItemCheckBox;

        public View mView;

        public int position;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            mView = itemView;

            tvItemTitle = (TextView) itemView.findViewById(R.id.id_tvSelKeywordGroupName);
            tvItemDetail = (TextView) itemView.findViewById(R.id.id_tvSelFullmatchWord);
            cbItemCheckBox = (CheckBox) itemView.findViewById(R.id.id_ChbSelectItem);
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////
    public void setUpdateUiElementWithDataBase(@NonNull ViewHolder viewHolder, CAppDbNewKeywordsGroup dataKwGroup) {
        viewHolder.tvItemTitle.setText(dataKwGroup.getKeywordGroupName());

        ArrayList<String> arrFm = dataKwGroup.getFullmatchKeywordsV1();
        String text = null;
        for (String elem: arrFm) {
            text += elem;
        }
        viewHolder.tvItemDetail.setText(text);
    }

    public List<CAppDbNewKeywordsGroup> getListItems() {
        return listItems;
    }


}
