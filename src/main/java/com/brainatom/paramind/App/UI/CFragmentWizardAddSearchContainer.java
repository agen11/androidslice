package com.brainatom.paramind.App.UI;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.brainatom.paramind.App.Database.CAppDbHandler;
import com.brainatom.paramind.App.Database.CAppDbNewKeywordsGroup;
import com.brainatom.paramind.App.Database.CAppDbNewSearchTargetGroup;
import com.brainatom.paramind.Platform.Type.CPfString;
import com.brainatom.paramind.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class CFragmentWizardAddSearchContainer extends Fragment {

    private static final String mTAG = "SearchWizard";

    private CWizardFragmentAdapter mAdapter;


    private ViewPager mViewPager = null;
    private ViewPagerAdapter mViewPagerAdapter;

    private Button mNextBut;
    private Button mPrevBut;
    private int mCurPage = 0;


    View mView = null;
    Context mContext;


    String mSearchGroupName = "";
    ArrayList<String> mUrllist = new ArrayList<>();
    ArrayList<String> mLinkedinlist = new ArrayList<>();

    CAppDbHandler mDbHdl = null;


    /////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////

    public CFragmentWizardAddSearchContainer() {
        // Required empty public constructor
    }

    /////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (mView != null) {
            return mView;
        }

        mView = inflater.inflate(R.layout.fragment_wizard_add_search_container, container, false);

        setLoadDataOfViewpagerAdapter();

        setBuildViewpager();

        setBuildButtonPair();

        return mView;

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }


    /////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////

    // adapter related
    private class ViewPagerAdapter extends FragmentPagerAdapter {

        private int WIZARD_PAGES_COUNT = 3;

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return new CWizardFragmentKeywordGroup(position);
        }

        @Override
        public int getCount() {
            return WIZARD_PAGES_COUNT;
        }

    }


    // indicator related
    private class CInWizardPageChangeListener implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageScrollStateChanged(int position) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onPageScrolled(int position, float positionOffset,
                                   int positionOffsetPixels) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onPageSelected(int position) {
        }

    }


    ////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////

    private void setLoadDataOfViewpagerAdapter() {
        List<Fragment> list = new ArrayList<>();
        list.add(new CFragmentWizardAddSearchS1());
        list.add(new CFragmentWizardAddSearchS2());
        list.add(new CFragmentWizardAddSearchS3());
        list.add(new CFragmentWizardAddSearchS4());
        mAdapter = new CWizardFragmentAdapter(getChildFragmentManager(), list);
    }

    private void setBuildViewpager() {
        // adding content manager
        mViewPager = (ViewPager)mView.findViewById(R.id.id_Viewpager_search_V1);
        mViewPager.setAdapter(mAdapter);

        mViewPager.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });

        mViewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener(){
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);

                if(position > mCurPage) {
                    if (setSyncForwardButton()) return;
                    mCurPage = mViewPager.getCurrentItem();

                }
                else if (position < mCurPage) {
                    if (setSyncBackwardButton()) return;
                    mCurPage = mViewPager.getCurrentItem();
                }
            }


            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                super.onPageScrolled(position, positionOffset, positionOffsetPixels);
                Log.d("TabSearch", "page scroll" + mViewPager.getCurrentItem());
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                super.onPageScrollStateChanged(state);
                Log.d("TabSearch", "state change" + mViewPager.getCurrentItem());
            }
        });


        mViewPager.addOnPageChangeListener(new CInWizardPageChangeListener());
    }

    private void setBuildButtonPair() {
        mNextBut = (Button)mView.findViewById(R.id.id_butRight_search_v1);
        mPrevBut = (Button)mView.findViewById(R.id.id_butLeft_search_v1);


        mNextBut.setText("Create");
        mNextBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = getChildFragmentManager().findFragmentByTag("android:switcher:" + R.id.id_Viewpager_search_V1 + ":" + mCurPage);
                if (mViewPager.getCurrentItem() == 0 && fragment != null) {
                    try {
                        if (!getCheckedPreliminary((CFragmentWizardAddSearchS1) fragment)) return;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                else if (mViewPager.getCurrentItem() == 1 && fragment != null) {
                    if (!getCheckedSearchGroupName((CFragmentWizardAddSearchS2) fragment)) return;
                }
                else if (mViewPager.getCurrentItem() == 2 && fragment != null) {
                    if (!getUrllist((CFragmentWizardAddSearchS3) fragment)) return;
                }
                else if (mViewPager.getCurrentItem() == 3 && fragment != null) {
                    if (!getLinkedinlist((CFragmentWizardAddSearchS4) fragment)) return;

                    // DB update
                    try {
                        setInsertNewSearchGroup();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                setStepForward();

            }
        });

        mPrevBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setStepBack();
            }
        });
    }

    private boolean getCheckedPreliminary(CFragmentWizardAddSearchS1 fragment) throws IOException {
        ArrayList<CAppDbNewKeywordsGroup> arrKwGroup = fragment.getDataS1();
        if (arrKwGroup.size() == 0) {
            Toast.makeText(getContext(), "At least one interest group is needed before continue.", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    private boolean getCheckedSearchGroupName(CFragmentWizardAddSearchS2 fragment) {
        String name = fragment.getDataS2();
        if (name.length() == 0) {
            Toast.makeText(getContext(), "The search group name is needed before proceed.", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (!name.matches("[a-zA-Z0-9]+_*")) {
            Toast.makeText(getContext(), "Only letter,number,underline can be entered, at least one letter or number is needed.", Toast.LENGTH_SHORT).show();
            return false;
        }
        else {
            mSearchGroupName = name;
        }
        return true;
    }

    private boolean getUrllist(CFragmentWizardAddSearchS3 fragment) {
        ArrayList<String> arrStrElements = fragment.getDataS3();
        if (arrStrElements.size() == 0) {
            Toast.makeText(getContext(), "No url is given for search .", Toast.LENGTH_SHORT).show();
            return false;
        }
        else {
            // tobe
            for (String kw: arrStrElements) {
                if (kw.matches(" +")) {
                    Toast.makeText(getContext(), "No space is allowed here.", Toast.LENGTH_SHORT).show();
                    return false;
                }
            }
        }
        mUrllist = arrStrElements;
        return true;
    }

    private boolean getLinkedinlist(CFragmentWizardAddSearchS4 fragment) {
        ArrayList<String> arrStrElements = fragment.getDataS4();
        if (arrStrElements.size() == 0 && (mUrllist.size() == 0) ) {
            Toast.makeText(getContext(), "No interest group is linked with current search division, at least one of which should be set.", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if ( arrStrElements.size() > 1 ) {
            Toast.makeText(getContext(), "Now only 1 interest group could be linked to your search division.", Toast.LENGTH_SHORT).show();
            return false;
        }

        mLinkedinlist = arrStrElements;
        return true;
    }

    private void setSetupViewPager(ViewPager viewPager) {
        CFragmentWizardPageAdapter adaptor = new CFragmentWizardPageAdapter(getActivity().getSupportFragmentManager());

        adaptor.setAddFragment(new CFragmentWizardAddSearchS1(), "TAB1");
        adaptor.setAddFragment(new CFragmentWizardAddSearchS2(), "TAB2");
        adaptor.setAddFragment(new CFragmentWizardAddSearchS3(), "TAB3");
        adaptor.setAddFragment(new CFragmentWizardAddSearchS4(), "TAB4");

        mViewPager.setAdapter(adaptor);
    }

    private boolean setSyncBackwardButton() {
        if (mCurPage == 1) {
            mPrevBut.setVisibility(View.INVISIBLE);
            mNextBut.setVisibility(View.VISIBLE);
            mNextBut.setText("Create");
        }
        else if (mCurPage == 0) {
            mNextBut.setText("Create");
            return true;
        }
        return false;
    }

    private boolean setSyncForwardButton() {
        if (mCurPage == 0) {
            mPrevBut.setVisibility(View.VISIBLE);
            mNextBut.setVisibility(View.VISIBLE);
            mNextBut.setText("Next");
        }
        if (mCurPage == mAdapter.getCount() - 2) {
            mPrevBut.setVisibility(View.VISIBLE);
            mNextBut.setVisibility(View.VISIBLE);
            mNextBut.setText("Finish");
        }
        else if (mCurPage == mAdapter.getCount() - 1) {
            return true;
        }
        else {
            mNextBut.setText("Next");
        }
        return false;
    }

    private void setStepForward() {
        if (mCurPage == 0) {
            mPrevBut.setVisibility(View.VISIBLE);
            mNextBut.setVisibility(View.VISIBLE);
            mNextBut.setText("Next");
        }
        if (mCurPage == mAdapter.getCount() - 2) {
            mPrevBut.setVisibility(View.VISIBLE);
            mNextBut.setVisibility(View.VISIBLE);
            mNextBut.setText("Finish");
        }
        else if (mCurPage == mAdapter.getCount() - 1) {
            mPrevBut.setVisibility(View.INVISIBLE);
            mNextBut.setVisibility(View.VISIBLE);
            mNextBut.setText("Create");
            mCurPage = 0;

            // finalize all and update
            Toast.makeText(getContext(), "New search group " + mSearchGroupName + " has been recorded.", Toast.LENGTH_SHORT).show();
            mViewPager.setCurrentItem(mCurPage);
            return;
        }
        else {
            mNextBut.setText("Next");
        }

        mCurPage = mViewPager.getCurrentItem() + 1;
        mViewPager.setCurrentItem(mCurPage);
    }

    private void setInsertNewSearchGroup() throws IOException {
        CPfString mStr = new CPfString();
        CAppDbNewSearchTargetGroup newSearchTargetGroup = new CAppDbNewSearchTargetGroup(
            mSearchGroupName, mUrllist, mLinkedinlist, 1);
        getDbHdlInst();
        mDbHdl.setAddSearchTargetGroup(newSearchTargetGroup);
        mDbHdl.close();
    }

    private void setStepBack() {
        if (setSyncBackwardButton()) return;

        mCurPage = mViewPager.getCurrentItem() - 1;
        mViewPager.setCurrentItem(mCurPage);
    }

    private void getDbHdlInst() {
        if (mDbHdl == null) {
            //mDbHdl = new CAppDbHandler(getActivity(), null, null, 1 );
            mDbHdl = ((MainActivity)getActivity()).getDbInstance();
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////

}
