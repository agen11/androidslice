package com.brainatom.paramind.App.UI;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.brainatom.paramind.App.Database.CAppDbHandler;
import com.brainatom.paramind.App.Database.CAppDbNewKeywordsGroup;
import com.brainatom.paramind.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class CFragmentDispKeywordGroup extends Fragment {

    CAppDbHandler mDbHdl = null;

    private RecyclerView mRecyView;
    private RecyclerView.Adapter mAdaptor;
    private ArrayList<CAppDbNewKeywordsGroup> mKwGroup = null;

    private CRecvDataAdapterKeyworGroup mAdaptorPrac;
    private ArrayList<String> mArrUserSelectedKeywordGroupName = null;


    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////

    public CFragmentDispKeywordGroup() {
        // Required empty public constructor
    }


    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_disp_keyword_group,
            container, false);

        setHasOptionsMenu(true);

        try {
            getDataFromDB();

            //getDBTextStringFordisplay(view);

        } catch (IOException e) {
            e.printStackTrace();
        }


        // link UI element
        mRecyView = view.findViewById(R.id.id_recvwDispKeywordgroup);
        mRecyView.setHasFixedSize(true);

        // layout manager
        mRecyView.setLayoutManager(new LinearLayoutManager(
            this.getActivity().getBaseContext()));

        // supply data to adaptor
        mAdaptorPrac = new CRecvDataAdapterKeyworGroup(mKwGroup, getActivity());
        mAdaptor = mAdaptorPrac;
        mRecyView.setAdapter(mAdaptor);

        mArrUserSelectedKeywordGroupName = new ArrayList<>();

        return view;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if ( menu != null ) {
            if ( !menu.hasVisibleItems() ) {
                getActivity().getMenuInflater().inflate(R.menu.menu_edit_listitem_searchtargetgroup, menu);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.id_keyword_group_item_remove:
                setCb_RemoveItemOfKeywordGroup();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////

    public void getDataFromDB() throws IOException {
        getDbHdlInst();
        String strDb = "";

        getDbHdlInst();
        mKwGroup = mDbHdl.getAllDataItemsOfKeywordGroup();

        mDbHdl.closeInstance();
        return;
    }

    public void setCb_RemoveItemOfKeywordGroup() {
        getDbHdlInst();
        List<CAppDbNewKeywordsGroup> res = mAdaptorPrac.getListItems();
        for(CAppDbNewKeywordsGroup elem: res) {
            if(elem.mIsSelected) {
                mArrUserSelectedKeywordGroupName.add(elem.mKeywordGroupName);
            }
        }

        // delete it from Db
        for (String item: mArrUserSelectedKeywordGroupName) {
            mDbHdl.setDeleteKeywordGroupitemViaQueryViaName(item);
            for (CAppDbNewKeywordsGroup elem: mKwGroup) {
                if (elem.getKeywordGroupName().equals(item)) {
                    mKwGroup.remove(elem);
                    break;
                }

            }
        }

        mAdaptorPrac.notifyDataSetChanged();
        mDbHdl.closeInstance();
    }

    private void getDbHdlInst() {
        if (mDbHdl == null) {
            mDbHdl = ((MainActivity)getActivity()).getDbInstance();
        }
        mDbHdl.getDbInstance();
    }

}
