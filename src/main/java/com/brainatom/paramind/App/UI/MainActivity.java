package com.brainatom.paramind.App.UI;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.speech.tts.TextToSpeech;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.brainatom.paramind.App.Database.CAppDbHandler;
import com.brainatom.paramind.App.Database.CAppDbNewKeywordsGroup;
import com.brainatom.paramind.App.Database.CAppDbNewSearchTargetGroup;
import com.brainatom.paramind.Platform.Nlp.CPfNlpSuit;
import com.brainatom.paramind.Platform.Type.CPfString;
import com.brainatom.paramind.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;


public class MainActivity extends AppCompatActivity {

    private CPfNlpSuit mNlpSuit = null;
    private TextToSpeech mTextToSpeech;
    private SpeechRecognizer mSpeechRecognizer;
    private IPfNlPVoiceToText mIPfNlPVoiceToText = null;

    private FrameLayout mFragment;
    //private CFragmentWizardAddKeywordContainer mCFragmentHome;
    private CFragmentHome mCFragmentHome;
    private CFragmentCreateKeywordGroup mCFragmentManageKeyword;
    private CFragmentManageSearchlist mCFragmentManageSearchlist;
    private CFragmentResultview mCFragmentResultview;
    private Fragment mCurFrag = null;

    private CFragmentManageTabKeyword mCFragmentManageTabKeyword;
    private CFragmentManageTabSearchlist mCFragmentManageTabSearchlist;

    private boolean mbApplySimulationData = false;
    private boolean mbFlushSimulationData = false;

    private CAppDbHandler mDbHdl = null;

    ///////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.id_navigationOfMainActivity);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);



        getFragmentView();

        //setInstantiateAllFragmentsTab();

        //setCurFragmentToHome(mCFragmentHome);

        getDbInstance();

        mbApplySimulationData = true;
        mbFlushSimulationData = true;
        //mbFlushSimulationData = false;
        try {
            setDeploySimulationEnv();
        } catch (IOException e) {
            e.printStackTrace();
        }

        setInitConverterOfTextToSpeech();

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }



    ///////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////

    public CPfNlpSuit setBuildBondOfNLP(IPfNlPVoiceToText obj) {
        mIPfNlPVoiceToText = obj;
        if( (mSpeechRecognizer == null) || (mTextToSpeech == null) ) {
            CPfNlpSuit result = new CPfNlpSuit();
            result.mState = false;
            return result;

        }
        else {
            if(mNlpSuit == null) {
                mNlpSuit = new CPfNlpSuit(mTextToSpeech, mSpeechRecognizer);
            }
            mNlpSuit.setSpeechRecognizer(mSpeechRecognizer);
            mNlpSuit.setTextToSpeech(mTextToSpeech);
            mNlpSuit.mState = true;

            return mNlpSuit;
        }
    }

    public boolean setBuildBondOfNLPSimple(IPfNlPVoiceToText obj, CPfNlpSuit receipt) {
        mIPfNlPVoiceToText = obj;
        if( (mSpeechRecognizer == null) || (mTextToSpeech == null) ) {
            return false;
        }
        else {
            receipt.setSpeechRecognizer(mSpeechRecognizer);
            receipt.setTextToSpeech(mTextToSpeech);
            return true;
        }
    }


    public void setActivateSpeechRecognizer() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);

        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM );
        intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 1);
        intent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, getApplication().getPackageName());

        mSpeechRecognizer.startListening(intent);
    }



    ///////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
        = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            //setInstantiateAllFragmentsTab();

            switch (item.getItemId()) {
                case R.id.navigation_home:
                    setCurFragmentToHome(mCFragmentHome);
                    return true;

                case R.id.id_Myinterest:
                    setCurFragmentToTabKeyword(mCFragmentManageTabKeyword);
                    return true;

                case R.id.id_Searchlist:
                    setCurFragmentToTabSearchlist(mCFragmentManageTabSearchlist);
                    return true;

                case R.id.id_findings:
                    setCurFragmentToResultview(mCFragmentResultview);
                    return true;
            }
            return false;
        }
    };



    ///////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////
    private void setDeploySimulationEnv() throws IOException {
        if (mbApplySimulationData) {
            CAppDbHandler dbHdl = getDbInstance();
            CPfString strFac = new CPfString();

            setDeployKeywordGroup(dbHdl, strFac);


            // setCheckPointOfDBResult(mDbHdl);
            // setTestSingleColumnUpdate(mDbHdl, strFac);
            // setTestFullColumnUpdate(mDbHdl, strFac);


            setDeploySearchGroup(dbHdl, strFac);

            dbHdl.close();
        }
    }

    private void setTestFullColumnUpdate(CAppDbHandler dbHdl, CPfString strFac) throws IOException {
        String keywordGroupName = "PrimarySimKwGroup";
        String strFullmatch = "mmmdrf";
        String strPartmatch = "";

        CAppDbNewKeywordsGroup newKeywordsGroup = new CAppDbNewKeywordsGroup(
            keywordGroupName,
            strFac.setAddJoinedStringByToken(strFullmatch, ","),
            strFac.setAddJoinedStringByToken(strPartmatch, ","), 1);
        dbHdl.setUpdateKeywordGroup(newKeywordsGroup);

        setCheckPointOfDBResult(dbHdl);
    }

    private void setTestSingleColumnUpdate(CAppDbHandler dbHdl, CPfString strFac) throws IOException {
        String keywordGroupName = "PrimarySimKwGroup";
        String strFullmatch = "mmmdrf";
        String strPartmatch = "";

        CAppDbNewKeywordsGroup newKeywordsGroup2 = new CAppDbNewKeywordsGroup(
            keywordGroupName,
            strFac.setAddJoinedStringByToken(strFullmatch, ","),
            strFac.setAddJoinedStringByToken(strPartmatch, ","), 1);
        dbHdl.setUpdateKeywordGroupFm(newKeywordsGroup2);

        setCheckPointOfDBResult(dbHdl);
    }

    private void setCheckPointOfDBResult(CAppDbHandler dbHdl) throws IOException {
        ArrayList<CAppDbNewKeywordsGroup> result1 = dbHdl.getAllDataItemsOfKeywordGroup();
        Log.d("DBKeyword", "check result");
    }

    private void setDeployKeywordGroup(CAppDbHandler dbHdl, CPfString strFac) {
        try {
            ArrayList<CAppDbNewKeywordsGroup> kwGroup = dbHdl.getAllDataItemsOfKeywordGroup();

            if(kwGroup.size() == 0) {
                setBuildSimulatedKeywordGroup(dbHdl, strFac);
            }
            else if (mbFlushSimulationData) {
                dbHdl.setRemoveAllFromKeywordGroup();
                setBuildSimulatedKeywordGroup(dbHdl, strFac);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void setDeploySearchGroup(CAppDbHandler dbHdl, CPfString strFac) {
        try {
            ArrayList<CAppDbNewSearchTargetGroup> searchTargetGroup = dbHdl.getAllDataItemsOfSearchTargetGroup();

            if(searchTargetGroup.size() == 0) {
                setBuildSimulatedSearchTargetGroup(dbHdl, strFac);
            }
            else if (mbFlushSimulationData) {
                dbHdl.setRemoveAllFromSearchTargetGroup();
                setBuildSimulatedSearchTargetGroup(dbHdl, strFac);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void setBuildSimulatedSearchTargetGroup(CAppDbHandler dbHdl, CPfString strFac) throws IOException {
        CAppDbNewKeywordsGroup kgObj = mDbHdl.getKeywordGroupByName("PrimarySimKwGroup");
        String strKeywordsGroupId = Integer.toString(kgObj.getId());

        String strSearchTargets = "https://en.wikipedia.org/wiki/Machine_learning";
        String strSearchTargetGroupName = "DefaultSearchTarget";


        ArrayList<String> object = new ArrayList<>();
        object.add("https://en.wikipedia.org/wiki/Machine_learning");
        object.add("https://searchenterpriseai.techtarget.com/definition/machine-learning-ML");
        CAppDbNewSearchTargetGroup newSearchTargetGroup = new CAppDbNewSearchTargetGroup(
            strSearchTargetGroupName, object, strFac.setAddJoinedString(strKeywordsGroupId), 1);

        dbHdl.setAddSearchTargetGroup(newSearchTargetGroup);
    }

    private void setBuildSimulatedKeywordGroup(CAppDbHandler dbHdl, CPfString strFac) throws IOException {
        String keywordGroupName = "PrimarySimKwGroup";
        String strFullmatch = "machine,learning,classification,data,regression";
        String strPartmatch = "selfcircle,etrack,mutualgravity";

        CAppDbNewKeywordsGroup newKeywordsGroup = new CAppDbNewKeywordsGroup(
            keywordGroupName,
            strFac.setAddJoinedStringByToken(strFullmatch, ","),
            strFac.setAddJoinedStringByToken(strPartmatch, ","), 1);

        dbHdl.setAddKeywordGroup(newKeywordsGroup);
    }

    private void getFragmentView() {
        mFragment = (FrameLayout)findViewById(R.id.id_frameOfMainActivity);
    }

    private void setInstantiateAllFragments() {
        //mCFragmentHome = new CFragmentWizardAddKeywordContainer();
        mCFragmentHome = new CFragmentHome();
        mCFragmentManageKeyword = new CFragmentCreateKeywordGroup();
        mCFragmentManageSearchlist = new CFragmentManageSearchlist();
        mCFragmentResultview = new CFragmentResultview();
    }

    private void setInstantiateAllFragmentsTab() {
        if (mCFragmentManageKeyword == null) mCFragmentManageKeyword = new CFragmentCreateKeywordGroup();
        if (mCFragmentManageSearchlist == null) mCFragmentManageSearchlist = new CFragmentManageSearchlist();

        if (mCFragmentHome == null) mCFragmentHome = new CFragmentHome();
        if (mCFragmentResultview == null) mCFragmentResultview = new CFragmentResultview();

        if (mCFragmentManageTabKeyword == null) mCFragmentManageTabKeyword = new CFragmentManageTabKeyword();
        if (mCFragmentManageTabSearchlist == null) mCFragmentManageTabSearchlist = new CFragmentManageTabSearchlist();

    }

    // pack fragment to activity
    private void setCurFragmentToTabKeyword(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        setRemoveCurFragment(fragmentTransaction);

        if (mCFragmentManageTabKeyword == null) mCFragmentManageTabKeyword = new CFragmentManageTabKeyword();
        fragmentTransaction.replace(R.id.id_mainfragment, mCFragmentManageTabKeyword);
        mCurFrag = mCFragmentManageTabKeyword;

        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }


    private void setCurFragmentToTabSearchlist(Fragment fragment) {
        //String name = fragment.getClass().getSimpleName();

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        setRemoveCurFragment(fragmentTransaction);

        if (mCFragmentManageTabSearchlist == null) mCFragmentManageTabSearchlist = new CFragmentManageTabSearchlist();
        fragmentTransaction.replace(R.id.id_mainfragment, mCFragmentManageTabSearchlist);
        mCurFrag = mCFragmentManageTabSearchlist;

        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    private void setCurFragmentToHome(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        setRemoveCurFragment(fragmentTransaction);

        if (mCFragmentHome == null) mCFragmentHome = new CFragmentHome();
        fragmentTransaction.replace(R.id.id_mainfragment, mCFragmentHome);
        mCurFrag = mCFragmentHome;

        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    private void setCurFragmentToResultview(Fragment fragment) {
        //String name = fragment.getClass().getSimpleName();

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        setRemoveCurFragment(fragmentTransaction);

        if (mCFragmentResultview == null) mCFragmentResultview = new CFragmentResultview();
        fragmentTransaction.replace(R.id.id_mainfragment, mCFragmentResultview);
        mCurFrag = mCFragmentHome;

        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    private void setCurFragment(Fragment fragment) {
        String name = fragment.getClass().getSimpleName();

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        if (name.equals("CFragmentManageTabKeyword")) {
            setRemoveCurFragment(fragmentTransaction);
            fragmentTransaction.replace(R.id.id_mainfragment, mCFragmentManageTabKeyword);
            mCurFrag = mCFragmentManageTabKeyword;
        }
        else if (name.equals("CFragmentManageTabSearchlist")) {
            setRemoveCurFragment(fragmentTransaction);
            fragmentTransaction.replace(R.id.id_mainfragment, mCFragmentManageTabSearchlist);
            mCurFrag = mCFragmentManageTabSearchlist;
        }
        else {
            setRemoveCurFragment(fragmentTransaction);
            fragmentTransaction.replace(R.id.id_mainfragment, mCFragmentHome);
            mCurFrag = mCFragmentHome;
        }

        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    private void setRemoveCurFragment(FragmentTransaction fragmentTransaction) {
        if (mCurFrag != null) {
            fragmentTransaction.remove(mCurFrag);
        }
    }

    private void setInitConverterOfTextToSpeech() {

        mTextToSpeech = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(mTextToSpeech.getEngines().size() == 0) {
                    Toast.makeText(MainActivity.this, "No engine installed!", Toast.LENGTH_SHORT).show();
                    Log.d("NoSupportI", "No engine");
                    finish();
                }
                else {
                    mTextToSpeech.setLanguage(Locale.US);
                    //setSpeakInQueue("Ready for start");
                    setInitEngineOfSpeechRecognizer();
                }
            }
        });

    }


    private void setInitEngineOfSpeechRecognizer() {
        if ( mSpeechRecognizer.isRecognitionAvailable(this) ) {
            mSpeechRecognizer = SpeechRecognizer.createSpeechRecognizer(this);


            mSpeechRecognizer.setRecognitionListener(new RecognitionListener() {
                @Override
                public void onReadyForSpeech(Bundle params) {

                }

                @Override
                public void onBeginningOfSpeech() {

                }

                @Override
                public void onRmsChanged(float rmsdB) {

                }

                @Override
                public void onBufferReceived(byte[] buffer) {

                }

                @Override
                public void onEndOfSpeech() {

                }

                @Override
                public void onError(int error) {
                    Log.d("ErrorOnV2t", Integer.toString(error));

                }

                @Override
                public void onResults(Bundle results) {
                    ArrayList<String> echo = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
                    setActOnVoiceText( echo );

                }

                @Override
                public void onPartialResults(Bundle partialResults) {

                }

                @Override
                public void onEvent(int eventType, Bundle params) {

                }
            });

            setSpeakInQueue("Now I am ready to accept your command");
        }
    }


    private void setSpeakInQueue(String message) {
        if(Build.VERSION.SDK_INT >= 21) {
            mTextToSpeech.speak(message, TextToSpeech.QUEUE_ADD, null, null);
        }
        else {
            mTextToSpeech.speak(message, TextToSpeech.QUEUE_ADD, null);
        }
    }


    private void setSpeakInFlush(String message) {
        if(Build.VERSION.SDK_INT >= 21) {
            mTextToSpeech.speak(message, TextToSpeech.QUEUE_FLUSH, null, null);
        }
        else {
            mTextToSpeech.speak(message, TextToSpeech.QUEUE_FLUSH, null);
        }
    }

    private void setActOnVoiceText( ArrayList<String> candidates ) {
        if (mIPfNlPVoiceToText != null) {
            boolean res = mIPfNlPVoiceToText.setProcessResponse(candidates);
        }
    }


    //////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////


    public CAppDbHandler getDbInstance() {
        if (mDbHdl == null) {
            mDbHdl = new CAppDbHandler(this, null, null, 1 );
        }
        mDbHdl.getDbInstance();
        return mDbHdl;
    }
}
