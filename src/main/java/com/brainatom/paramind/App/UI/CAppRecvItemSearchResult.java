package com.brainatom.paramind.App.UI;

public class CAppRecvItemSearchResult {
    private String itemContent;
    private String itemContentFull;
    private String headImageUrl;
    private String datetime;
    private String belongUrl;

    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////

    public CAppRecvItemSearchResult() {
    }


    public CAppRecvItemSearchResult(String itemContent, String imageUrl) {
        this.itemContent = itemContent;
        this.headImageUrl = imageUrl;
    }

    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////

    public String getItemContent() {
        return itemContent;
    }

    public void setItemContent(String itemContent) {
        this.itemContent = itemContent;
    }

    public String getHeadImageUrl() {
        return headImageUrl;
    }

    public void setHeadImageUrl(String headImageUrl) {
        this.headImageUrl = headImageUrl;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getBelongUrl() {
        return belongUrl;
    }

    public void setBelongUrl(String belongUrl) {
        this.belongUrl = belongUrl;
    }

    public String getItemContentFull() {
        return itemContentFull;
    }

    public void setItemContentFull(String itemContentFull) {
        this.itemContentFull = itemContentFull;
    }
}

