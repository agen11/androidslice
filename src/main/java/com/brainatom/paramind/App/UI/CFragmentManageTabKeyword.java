package com.brainatom.paramind.App.UI;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.brainatom.paramind.R;


public class CFragmentManageTabKeyword extends Fragment {
    private CTabKeywordPageAdaptor mTabKeywordPageAdaptor = null;
    private ViewPager mViewPager = null;
    View mView = null;


    ////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////

    public CFragmentManageTabKeyword() {
        // Required empty public constructor
    }


    ////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (mView == null) {
            mView = inflater.inflate(R.layout.fragment_mng_tab_keyword, container, false);
        }


        Activity activity = getActivity();
        mTabKeywordPageAdaptor = new CTabKeywordPageAdaptor(((FragmentActivity) activity).getSupportFragmentManager());

        mViewPager = (ViewPager)mView.findViewById(R.id.id_viewpager_keyword);
        setSetupViewPager(mViewPager);
        TabLayout tabLayout = (TabLayout) mView.findViewById(R.id.id_TabKeyword);
        tabLayout.setupWithViewPager(mViewPager);


        mViewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener(){
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                Log.d("pagechange", "listener");
                if(position == 0) {

                    try {
                        IAppFragRecvUser obj = (IAppFragRecvUser)getChildFragmentManager().getFragments().get(0);
                        if (obj != null) {
                            obj.setReloadData();
                        }
                    }
                    catch (Exception e) {}

                }
            }
        });


        return mView;
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
    }

    ////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////
    private void setSetupViewPager(ViewPager viewPager) {
        //Activity activity = getActivity();
        CTabKeywordPageAdaptor adaptor = new CTabKeywordPageAdaptor( getChildFragmentManager() );
        adaptor.setAddFragment(new CFragmentDispKeywordGroupV1(), "My interest");
        adaptor.setAddFragment(new CFragmentWizardAddKeywordContainer(), "New interest");

        viewPager.setAdapter(adaptor);

    }

}
