package com.brainatom.paramind.App.Network;

import java.util.Map;

public class CAppNetRestPostBasicSearchReq {
    private String      userId;
    private Map<String, String>         contentData;

    /////////////////////////////////////////////////////
    /////////////////////////////////////////////////////
    /////////////////////////////////////////////////////
    /////////////////////////////////////////////////////
    /////////////////////////////////////////////////////

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Map<String, String> getContentData() {
        return contentData;
    }

    public void setContentData(Map<String, String> contentData) {
        this.contentData = contentData;
    }
}
