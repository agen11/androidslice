package com.brainatom.paramind.App.Network;

import com.google.gson.annotations.SerializedName;

import java.util.Map;

// fixing name as test case
public class CAppNetRestRespDefaultV1 {
    @SerializedName("root")
    private String content;

    public CAppNetRestRespDefaultV1(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
