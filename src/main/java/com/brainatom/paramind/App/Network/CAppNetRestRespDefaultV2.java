package com.brainatom.paramind.App.Network;

import java.util.Map;

public class CAppNetRestRespDefaultV2 {
    // random dictionary
    private Map<String, String> content;


    public CAppNetRestRespDefaultV2(Map<String, String> content) {
        this.content = content;
    }

    public Map<String, String> getContent() {
        return content;
    }

    public void setContent(Map<String, String> content) {
        this.content = content;
    }
}
