package com.brainatom.paramind.App.Network;

import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
import java.util.List;

public class CAppNetJsTargetGroupList {
    @SerializedName("SearchTargetGroupList")
    List<CAppNetJsTargetGroup> mArrTargetGroup = null;


    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////

    public CAppNetJsTargetGroupList() {
        mArrTargetGroup = new ArrayList<>();
    }

    public CAppNetJsTargetGroupList(List<CAppNetJsTargetGroup> tglist) {
        mArrTargetGroup = tglist;
    }


    public List<CAppNetJsTargetGroup> getArrTargetGroup() {
        return mArrTargetGroup;
    }

    public void setmArrTargetGroup(List<CAppNetJsTargetGroup> mArrTargetGroup) {
        this.mArrTargetGroup = mArrTargetGroup;
    }
}
