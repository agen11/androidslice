package com.brainatom.paramind.App.Network;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface IAppNetRestUserRequest {



    //////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////

    @POST("searchReq/{userid}")
    Call<CAppNetRestRespDefault> setSearchPostDefault(
            @Path(value = "userid", encoded = true) String userId
    );


    @FormUrlEncoded
    @POST("searchReq/{userid}")
    Call<CAppNetRestRespDefault> setSearchPost(
            @Path(value = "userid", encoded = true) String userId,
            @Field("title") String title,
            @Field("content") String content
            );

    @POST("searchReq/root")
    Call<CAppNetRestRespDefault> setSearchPostV1(
            @Body String title
            );
}
