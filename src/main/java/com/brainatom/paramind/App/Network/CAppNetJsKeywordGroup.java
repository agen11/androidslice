package com.brainatom.paramind.App.Network;

import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
import java.util.List;



public class CAppNetJsKeywordGroup {
    @SerializedName("KeywordGroupName")
    public String mKeywordGroupName;
    @SerializedName("FullmatchKeyword")
    public List<String> mFullmatchKeywords;
    @SerializedName("PartialMatchKeyword")
    public List<String> mPartmatchKeywords;


    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////


    public CAppNetJsKeywordGroup(String name, List<String> fmKeywordgroup, List<String> pmKeywordgroup) {
        mKeywordGroupName = name;
        mFullmatchKeywords = fmKeywordgroup;
        mPartmatchKeywords = pmKeywordgroup;
    }

}
