package com.brainatom.paramind.App.Network;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Map;

public class CAppNetRestRespDefault {

    @SerializedName("uploading")
    @Expose
    //private String data;
    //private CSimData data;
    //private CSimDataV1 data;
    private Map<String, String> uploading;

    // public HashMap<String, String> mRes;


    // public CAppNetRestRespDefault(HashMap<String, String> mRes) {
    //     this.mRes = mRes;
    // }

    // public HashMap<String, String> getRes() {
    //     return mRes;
    // }

    // public void setRes(HashMap<String, String> res) {
    //     this.mRes = res;
    // }

// private String mState;
    // private String mDetails;

    //
    //
    // ///////////////////////////////////////////////////
    // ///////////////////////////////////////////////////
    // ///////////////////////////////////////////////////
    // ///////////////////////////////////////////////////
    // ///////////////////////////////////////////////////

    // public String getState() {
    //     return mState;
    // }

    // public void setState(String state) {
    //     this.mState = state;
    // }

    // public String getDetails() {
    //     return mDetails;
    // }

    // public void setDetails(String details) {
    //     this.mDetails = details;
    // }


    public Map<String, String> getUploading() {
        return uploading;
    }

    public void setUploading(Map<String, String> fields) {
        this.uploading = fields;
    }
}


class CSimData {
    private String result;

    public CSimData(String result) {
        this.result = result;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}


class CSimDataV1 {
    private Map<String, String> fields;

    public CSimDataV1(Map<String, String> fields) {
        this.fields = fields;
    }

    public Map<String, String> getFields() {
        return fields;
    }

    public void setFields(Map<String, String> fields) {
        this.fields = fields;
    }
}






