package com.brainatom.paramind.App.Network;

import com.brainatom.paramind.App.Setting.CAppGlobalSetting;
import com.brainatom.paramind.Platform.Network.CPfSocketClient;

public class CAppSocketClient extends CPfSocketClient {
    public CAppSocketClient() {
        port =    CAppGlobalSetting.getNetworkServerDefaultPort();
        addr =    CAppGlobalSetting.getNetworkServerDefaultAddr();
    }
}
