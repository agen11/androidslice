package com.brainatom.paramind.App.Network;

import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
import java.util.List;

public class CAppNetJsKeywordGroupList {
    @SerializedName("KeywordGroupList")
    List<CAppNetJsKeywordGroup> mArrKeywordGroup = null;


    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////


    public CAppNetJsKeywordGroupList() {
        mArrKeywordGroup = new ArrayList<>();
    }

    public CAppNetJsKeywordGroupList(List<CAppNetJsKeywordGroup> kwlist) {
        mArrKeywordGroup = kwlist;
    }


    public List<CAppNetJsKeywordGroup> getArrKeywordGroup() {
        return mArrKeywordGroup;
    }

    public void setmArrKeywordGroup(List<CAppNetJsKeywordGroup> mArrKeywordGroup) {
        this.mArrKeywordGroup = mArrKeywordGroup;
    }
}
