package com.brainatom.paramind.App.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;

import com.brainatom.paramind.App.Setting.CAppGlobalSetting;
import com.brainatom.paramind.Platform.Time.CPfBaseTime;
import com.brainatom.paramind.Platform.Data.CPfDataCollection;

import java.io.IOException;
import java.util.ArrayList;

public class CAppDbHandler extends SQLiteOpenHelper {
    private static final String mTAG = "MindDb";

    // ------------------------------------------------------------------ //
    // DB
    private  static final int DATABASE_VERSION = 1;
    private  static final String DATABASE_NAME = "paramind.db";

    // ------------------------------------------------------------------ //
    // TABLE
    public static final String TABLE_KEYWORD_GROUP = "KeywordGroup";
    public static final String TABLE_SEARCH_GROUP = "NetUrlTarget";


    public boolean ifValid() {
        return ( mDb == null || !mDb.isOpen() ) ? false : true;
    }

    public void getDbInstance() {
        if(mDb == null || !mDb.isOpen()) {
            mDb = getWritableDatabase();
        }
    }

    public void closeInstance() {
        if(mDb != null) {
            mDb.close();
            mDb = null;
        }
    }


}


