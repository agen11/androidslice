package com.brainatom.paramind.App.Database;

import android.util.Log;

import com.brainatom.paramind.Platform.Data.CPfDataCollection;

import java.io.IOException;
import java.util.ArrayList;

// interface data class
public class CAppDbNewSearchTargetGroup {
    private static final String mTAG = "DbSearchTargetGroup";

    public int mId;
    public String mSearchTargetGroupName;
    public ArrayList<String> mSearchUrlTarget;
    public ArrayList<String> mLinkedinKeywordsGroupId;
    public ArrayList<CAppDbNewKeywordsGroup> mLinkedinKeywordsGroup;
    public int mState;

    public String mTimeCreate;
    public String mTimeModification;

    public boolean isSelected;

    private CAppDbHandler mDbHdl = null;


    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////



    public CAppDbNewSearchTargetGroup() {

    }

    public CAppDbNewSearchTargetGroup(String searchTargetGroupName, ArrayList<String> searchUrlTarget, ArrayList<String> keywordsGroup, int state) {
        mSearchTargetGroupName = searchTargetGroupName;
        mSearchUrlTarget = searchUrlTarget;
        mLinkedinKeywordsGroupId = keywordsGroup;
        mState = state;
    }

    public CAppDbNewSearchTargetGroup(String searchTargetGroupName, ArrayList<String> searchUrlTarget, ArrayList<String> keywordsGroup, int state, CAppDbHandler dbHandler) {
        mSearchTargetGroupName = searchTargetGroupName;
        mSearchUrlTarget = searchUrlTarget;
        mLinkedinKeywordsGroupId = keywordsGroup;
        mState = state;

        mDbHdl = dbHandler;
    }


    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////

    public int getId() {
        return mId;
    }

    public void setId(int mId) {
        this.mId = mId;
    }

    public String getSearchTargetGroupName() {
        return mSearchTargetGroupName;
    }

    public void setSearchTargetGroupName(String searchTargetGroupName) {
        mSearchTargetGroupName = searchTargetGroupName;
    }

    public byte[] getSearchUrlTarget() {
        return CPfDataCollection.setArraylistStringToByte(mSearchUrlTarget);
    }

    public ArrayList<String> getSearchUrlTargetV1() {
        return mSearchUrlTarget;
    }

    public void setSearchUrlTarget(ArrayList<String> searchUrlTarget) {
        mSearchUrlTarget = searchUrlTarget;
    }

    public byte[] getKeywordsGroup() {
        return CPfDataCollection.setArraylistStringToByte(mLinkedinKeywordsGroupId);
    }

    public ArrayList<String> getKeywordsGroupIdList() {
        return mLinkedinKeywordsGroupId;
    }

    public byte[] getKeywordsGroupIdListV1() {
        return CPfDataCollection.setArraylistStringToByte(mLinkedinKeywordsGroupId);
    }

    public void setKeywordsGroup(ArrayList<String> keywordsGroup) {
        mLinkedinKeywordsGroupId = keywordsGroup;
    }

    public void setKeywordsGroupV1(ArrayList<String> keywordsGroup) throws IOException {
        mLinkedinKeywordsGroupId = keywordsGroup;

        if(mDbHdl == null) {
            Log.d(mTAG, "No DB access");
            return;
        }

        for(String item : keywordsGroup) {
            CAppDbNewKeywordsGroup ret = mDbHdl.getDataOfKeywordGroupById(Integer.parseInt(item));
            if (ret == null) {
                mLinkedinKeywordsGroup.add(ret);
            }
            else {
                mLinkedinKeywordsGroup.add(null);
            }
        }

    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public int getState() {
        return mState;
    }

    public void setState(int mState) {
        this.mState = mState;
    }

    public String getTimeCreate() {
        return mTimeCreate;
    }

    public void setTimeCreate(String mTimeCreate) {
        this.mTimeCreate = mTimeCreate;
    }

    public String getTimeModification() {
        return mTimeModification;
    }

    public void setTimeModification(String mTimeModification) {
        this.mTimeModification = mTimeModification;
    }

}
