package com.brainatom.paramind.App.Database;

import com.brainatom.paramind.Platform.Data.CPfDataCollection;

import java.util.ArrayList;

// interface data service
public class CAppDbNewKeywordsGroup {
    public int mId;

    public String mKeywordGroupName;
    public ArrayList<String> mFullmatchKeywords;
    public ArrayList<String> mPartmatchKeywords;
    public int mState;
    public String mTimeCreate;
    public String mTimeModification;

    public boolean mIsSelected;

    public CAppDbNewKeywordsGroup() {

    }

    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////


    public CAppDbNewKeywordsGroup(String keywordGroupName, ArrayList<String> fullmatchKeywords, ArrayList<String> partmatchKeywords, int state) {
        mKeywordGroupName = keywordGroupName;
        mFullmatchKeywords = fullmatchKeywords;
        mPartmatchKeywords = partmatchKeywords;
        mState = state;

    }


    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////

    public int getId() {
        return mId;
    }

    public void setId(int mId) {
        this.mId = mId;
    }

    public String getKeywordGroupName() {
        return mKeywordGroupName;
    }

    public void setKeywordGroupName(String keywordGroupName) {
        mKeywordGroupName = keywordGroupName;
    }

    public byte[] getFullmatchKeywords() {
        return CPfDataCollection.setArraylistStringToByte(mFullmatchKeywords);
    }

    public ArrayList<String> getFullmatchKeywordsV1() {
        return mFullmatchKeywords;
    }

    public void setFullmatchKeywords(ArrayList<String> fullmatchKeywords) {
        mFullmatchKeywords = fullmatchKeywords;
    }

    public byte[] getPartmatchKeywords() {
        return CPfDataCollection.setArraylistStringToByte(mPartmatchKeywords);
    }

    public ArrayList<String> getPartmatchKeywordsV1() {
        return mPartmatchKeywords;
    }

    public void setPartmatchKeywords(ArrayList<String> partmatchKeywords) {
        mPartmatchKeywords = partmatchKeywords;
    }

    public boolean isSelected() {
        return mIsSelected;
    }

    public void setSelected(boolean selected) {
        mIsSelected = selected;
    }

    public int getState() {
        return mState;
    }

    public void setState(int mState) {
        this.mState = mState;
    }

    public String getTimeCreate() {
        return mTimeCreate;
    }

    public void setTimeCreate(String mTimeCreate) {
        this.mTimeCreate = mTimeCreate;
    }

    public String getTimeModification() {
        return mTimeModification;
    }

    public void setTimeModification(String mTimeModification) {
        this.mTimeModification = mTimeModification;
    }
}
