package com.brainatom.paramind.App.Web;

import com.brainatom.paramind.App.Network.IAppNetRestUserRequest;
import com.brainatom.paramind.App.Setting.CAppGlobalSetting;
import com.brainatom.paramind.Platform.Web.CPfRetrofitJsonModel;
import com.brainatom.paramind.Platform.Web.CPfRetrofitJsonModelDeserializer;
import com.brainatom.paramind.Platform.Web.CPfWebRestRetrofit;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CAppWebRetrofit extends CPfWebRestRetrofit {


    static public IAppNetRestUserRequest setBuildDefaultRetrofitObject() {
        Gson gson = new GsonBuilder().serializeNulls().create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://" + CAppGlobalSetting.getNetworkServerDefaultAddr() + ":" + CAppGlobalSetting.getWebAppServerDefaultPort() + "/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        return retrofit.create(IAppNetRestUserRequest.class);
    }

    static public IAppNetRestUserRequest setBuildDefaultRetrofitObjectV1() {
        Gson gson = new GsonBuilder().registerTypeAdapter(CPfRetrofitJsonModel.class, new CPfRetrofitJsonModelDeserializer()).create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://" + CAppGlobalSetting.getNetworkServerDefaultAddr() + ":" + CAppGlobalSetting.getWebAppServerDefaultPort() + "/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        return retrofit.create(IAppNetRestUserRequest.class);
    }

    static public IAppNetRestUserRequest setBuildDefaultRetrofitObjectV2() throws IOException {
        OkHttpClient okHttpClient = setCreateHttpClientOfLogIntercepter();

        Gson gson = new GsonBuilder().registerTypeAdapter(CPfRetrofitJsonModel.class, new CPfRetrofitJsonModelDeserializer()).create();
        //Gson gson = new GsonBuilder().serializeNulls().create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://" + CAppGlobalSetting.getNetworkServerDefaultAddr() + ":" + CAppGlobalSetting.getWebAppServerDefaultPort() + "/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(okHttpClient)
                .build();

        return retrofit.create(IAppNetRestUserRequest.class);
    }
}
