
package com.brainatom.paramind.App.Web;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.brainatom.paramind.App.Database.CAppDbHandler;
import com.brainatom.paramind.App.Database.CAppDbNewKeywordsGroup;
import com.brainatom.paramind.App.Database.CAppDbNewSearchTargetGroup;
import com.brainatom.paramind.App.Network.CAppSocketClient;
import com.brainatom.paramind.App.Network.IAppNetRestUserRequest;
import com.brainatom.paramind.App.Setting.CAppSettingOfWebAction;
import com.brainatom.paramind.App.Network.CAppNetJsMsgItemKeywordsGroup;
import com.brainatom.paramind.App.Network.CAppNetJsMsgItemSearchTargetGroup;
import com.brainatom.paramind.App.Network.CAppNetJsMsgItemSearchUrl;
import com.brainatom.paramind.App.Network.CAppNetJsMsgReq;
import com.brainatom.paramind.App.Network.CAppNetJsMsgRspMatchedSection;
import com.brainatom.paramind.App.Network.CAppNetJsMsgSearchUrlData;
import com.brainatom.paramind.App.Network.CAppNetJsMsgTargetSettingData;
import com.brainatom.paramind.App.Setting.EnumStateMachineOfAppTransceiver;
import com.brainatom.paramind.Platform.Time.CPfBaseTime;
import com.brainatom.paramind.Platform.File.CPfFileOperationInternal;
import com.brainatom.paramind.Platform.Web.CPfWebAnalyzer;
import com.brainatom.paramind.Platform.Web.CPfWebUrl;
import com.brainatom.paramind.R;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.brainatom.paramind.App.Setting.EnumStateMachineOfAppTransceiver.DEFAULT_SOCKET;


public class CAppWebTransceiver extends AsyncTask<String, Void, Void> {
    // processing buffer
    String mUrl = "";
    String mBufWebContentInString = "";

    ArrayList<CSearchTarget>        mDcAllWebPackage = null;
    HashMap<String, CAppNetJsMsgItemKeywordsGroup>  mInterest = null;

    String                          mWebpageHtml = "";
    StringBuilder                   mBuilder = new StringBuilder();

    Context mContext = null;

    // setting
    EnumStateMachineOfAppTransceiver mWorkFlow = DEFAULT_SOCKET;
    CAppSettingOfWebAction mSetting;

    // data cluster
    ArrayList<CAppDbNewSearchTargetGroup> mTargetGroup = null;
    ArrayList<CAppDbNewKeywordsGroup> mKwGroup = null;
    private Map<String, CAppNetJsMsgRspMatchedSection> mObjcat = null;

    Gson mGson = null;
    private IAppNetRestUserRequest mJsonPlaceHolderApi;

    //////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////

    // initiator
    public CAppWebTransceiver() {
    }

    public CAppWebTransceiver(Context context) {
        mContext = context;
    }

    public CAppWebTransceiver(String url) {
        mUrl = url;
    }

    public CAppWebTransceiver(Context context, ArrayList<CAppDbNewSearchTargetGroup> stg, ArrayList<CAppDbNewKeywordsGroup> kwg, int time) {
        mContext = context;
        mTargetGroup = stg;
        mKwGroup = kwg;

        if (time > 0) {
            mSetting = new CAppSettingOfWebAction(time);
        }
    }



    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////


    // core
    @Override
    protected Void doInBackground(String... strings) {
        // init
        mObjcat = null;

        switch (mWorkFlow) {
            case DEFAULT_SOCKET:
                setInitiateSearchRequestV1();
                break;

            case RETRO_REQUESTSEARCH:
                setInitiateSearchMove();
                break;

            case RETRO_GETRESULT:
                setRetriveSearchResults();
                break;

            case SOUP_MASSIVEGET:
                break;

            default:
                break;
        }

        return null;
    }

    private void setInitiateSearchMove() {
        setInitSoupMembers();

        setComposeWebDataOfSearchGroup();

        setComposeSearchRequestPackage();

        try {
            setSearchOverObj();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return;
    }

    private String setBuildSearchRequestSduV1() {
        String gsondata = mGson.toJson(new CSearchRequestData(mDcAllWebPackage, mInterest));
        mBufWebContentInString = setBuildSduOfAppLayer(gsondata);
        return mBufWebContentInString;
    }

    private void setComposeWebDataOfSearchGroup() {
        boolean bFirstTry = true;
        for(CAppDbNewSearchTargetGroup elem : mTargetGroup) {
            ArrayList<String> lsUrl = elem.mSearchUrlTarget;

            for (String url: lsUrl) {

                // sleep before next request
                if (!bFirstTry) {
                    CPfBaseTime.setSleepForSecond(mSetting.getSleepTimePerUrl());
                }
                else {
                    bFirstTry = false;
                }

                setComposeDataOfSingleUrl(elem, url);
            }
        }
    }

    private void setComposeSearchRequestPackage() {
        CSearchRequestData obj = new CSearchRequestData();
        mInterest = new HashMap<>();

        for (CAppDbNewKeywordsGroup kwgItem: mKwGroup) {
            CAppNetJsMsgItemKeywordsGroup kwgnet = new CAppNetJsMsgItemKeywordsGroup(
                    kwgItem.mKeywordGroupName,
                    kwgItem.mFullmatchKeywords, kwgItem.mPartmatchKeywords,
                    kwgItem.mTimeCreate );
            mInterest.put( kwgItem.mKeywordGroupName, kwgnet );
        }
        return;
    }

    private void setComposeDataOfSingleUrl(CAppDbNewSearchTargetGroup elem, String url) {
        // get page content
        //boolean mRes = getWebpageMethodV1(url);
        boolean res = getWebpageMethodV3ViaJsoup(url);
        if (!res) {
            Log.d("UrlRead", "Failed to read url" + url);
            return;
        }
        Log.d("UrlRead", "Success in reading url" + url);

        if (mWebpageHtml.length() > 0) {
            setStoreFetchedPage(elem, url);
        }
    }

    private void setStoreFetchedPage(CAppDbNewSearchTargetGroup elem, String url) {
        ArrayList<String> lsAssInterests = new ArrayList<>();
        lsAssInterests.add(elem.mSearchTargetGroupName);

        //setSearchObjOverHtml(socketClient, url);
        CSearchTarget target = new CSearchTarget(
            "", url, mWebpageHtml, lsAssInterests,
            CPfBaseTime.setConvertTimeStampToDefaultFormatV2() );

        mDcAllWebPackage.add(target);
    }

    private void setRetriveSearchResults() {
        return;
    }

    @Nullable
    private void setInitiateSearchRequestV1() {
        // testing case to collect data
        // setBuildSimDataOfTargetGroup();
        CAppSocketClient socketClient = setUploadUserFavorite();
        if (socketClient == null) return;

        setPutupSearchRequest(socketClient);

        //setTestCaseOnMsgBasics();

        return;
    }

    private void setPutupSearchRequest(CAppSocketClient socketClient) {
        // real search request
        setRequestUltimateSearch(socketClient);

        // save results
        setSaveResponse();
    }

    @Nullable
    private CAppSocketClient setUploadUserFavorite() {
        // pilot msg on preference
        CAppNetJsMsgTargetSettingData msgdataS = setCreateMsgOfUserFavorite();
        if (msgdataS == null) return null;

        CAppSocketClient socketClient = setSendMsgOfUserFavorite(msgdataS);
        return socketClient;
    }


    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////

    private void setSearchOverObj() throws IOException {
        getWebRetrofitInstance();

        Call<ResponseBody> call = mJsonPlaceHolderApi.setPostSearchOverObj(
                "root", setBuildSearchRequestSduV1());
        //Call<ResponseBody> call = mJsonPlaceHolderApi.setPostSearchOverObj("root", setBuildSearchRequest());

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (!response.isSuccessful()) {
                    return;
                }

                try {
                    String posts = response.body().string();
                    JSONObject jsonObject;
                    JSONArray jsonArrObject;
                    try {
                        boolean bIfArrayObj = true;
                        if (bIfArrayObj) {
                            jsonArrObject = new JSONArray(posts);

                            Log.d("Result", jsonArrObject.toString() );
                        }
                        else {
                            jsonObject = new JSONObject(posts);
                            String res = jsonObject.getString("root");
                            Log.d("Result", res);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        return;
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("Result", "failed to send search request");
            }

        });
    }

    private IAppNetRestUserRequest getWebRetrofitInstance() throws IOException {
        if (mJsonPlaceHolderApi == null) {
            mJsonPlaceHolderApi = CAppWebRetrofit.setBuildDefaultRetrofitObjectV2();
        }
        return mJsonPlaceHolderApi;
    }

    @NonNull
    private HashMap<String, String> setBuildSearchRequest() {
        HashMap<String, String> fields = new HashMap<>();

        fields.put("userId", "");
        fields.put("Cookie", "");
        fields.put("SessionId", "");
        fields.put("DataForSearch", "");

        return fields;
    }


    private void setRequestUltimateSearch(CAppSocketClient socketClient) {
        if(mGson == null) mGson = new Gson();

        boolean bFirstTry = true;
        for(CAppDbNewSearchTargetGroup elem : mTargetGroup) {
            ArrayList<String> lsUrl = elem.mSearchUrlTarget;

            for (String url: lsUrl) {

                // sleep before next request
                if (!bFirstTry) {
                    CPfBaseTime.setSleepForSecond(mSetting.getSleepTimePerUrl());
                }
                else {
                    bFirstTry = false;
                }


                // get page content
                //boolean mRes = getWebpageMethodV1(url);
                boolean res = getWebpageMethodV3ViaJsoup(url);
                if (!res) {
                    Log.d("UrlRead", "Failed to read url" + url);
                    continue;
                }
                Log.d("UrlRead", "Success in reading url" + url);



                if (mWebpageHtml.length() > 0) {
                    setSearchObjOverHtml(socketClient, url);
                }


            }
        }
    }

    private void setSearchObjOverHtml(CAppSocketClient socketClient, String url) {
        String strJsonMsgFinal = setGenerateSearchMsgData(url);

        // search request
        socketClient.setSendMsgToServerNoWaiting(strJsonMsgFinal);

        String size = Integer.toString(strJsonMsgFinal.length());
        Log.d("Client (req) msg len", size);


        // waiting for response
        //String srvResp = socketClient.setRecvMsgFromServer();
        String srvResp = socketClient.setRecvMsgFromServerV6();
        Log.d("Server resp:", srvResp + "len: " + Integer.toString(srvResp.length()));


        // parsing
        setParseServerSearchResults(srvResp);


        try {
            socketClient.setClearSocket();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @NonNull
    private String setGenerateSearchMsgData(String url) {
        String strJsonMsgData = setBuildSearchRequestPdu(url);
        return setBuildSduOfAppLayer(strJsonMsgData);
    }

    @NonNull
    private String setBuildSduOfAppLayer(Object strJsonMsgData) {
        // msg wrapper
        CAppNetJsMsgReq msgFinal = new CAppNetJsMsgReq("0.1",
            "RealSearch", strJsonMsgData, "root");
        //CAppNetJsMsgReq msgFinal = new CAppNetJsMsgReq("0.1", "ObjPage", (Object) mWebpageHtml, "testor");

        String strJsonMsgFinal = mGson.toJson(msgFinal);
        strJsonMsgFinal += "\n";
        //Log.d("msg len", size);
        return strJsonMsgFinal;
    }

    private String setBuildSearchRequestPdu(String url) {
        // page content package
        CAppNetJsMsgSearchUrlData msgdata = new CAppNetJsMsgSearchUrlData();
        CAppNetJsMsgItemSearchUrl singleItem = new CAppNetJsMsgItemSearchUrl(
                url, CPfBaseTime.setConvertTimeStampToDefaultFormatV2(), mWebpageHtml);
        msgdata.setAddDataItem(singleItem);

        return mGson.toJson(msgdata);
    }

    @NonNull
    private CAppSocketClient setSendMsgOfUserFavorite(CAppNetJsMsgTargetSettingData msgdataS) {
        Gson gson = new Gson();
        String strJsonMsgDataS = gson.toJson(msgdataS);
        CAppNetJsMsgReq msgCustomSearch = new CAppNetJsMsgReq("0.1",
            "TargetSetting", (Object)strJsonMsgDataS, "testor");


        String strJsonMsgFinalS = gson.toJson(msgCustomSearch);
        String sizeS = Integer.toString(strJsonMsgFinalS.length());
        strJsonMsgFinalS += "\n";


        // upload setting to server
        CAppSocketClient socketClient = new CAppSocketClient();
        socketClient.setSendMsgToServerNoWaitingSingle(strJsonMsgFinalS);
        Log.d("Client (req) msg len", sizeS);
        return socketClient;
    }

    @Nullable
    private CAppNetJsMsgTargetSettingData setCreateMsgOfUserFavorite() {
        // read setting data from DB
        CAppDbHandler dbHdl = new CAppDbHandler(mContext, null, null, 1 );
        ArrayList<CAppDbNewKeywordsGroup> kwGroup = null;
        ArrayList<CAppDbNewSearchTargetGroup> searchTargetGroup = null;

        try {
            searchTargetGroup = dbHdl.getAllDataItemsOfSearchTargetGroup();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            kwGroup = dbHdl.getAllDataItemsOfKeywordGroup();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (kwGroup.size() == 0 || searchTargetGroup.size() == 0) {
            Log.d("Alert", "No information stored for search!");
            return null;
        }

        //setCreateTargetList(searchTargetGroup);
        return setGenerateFavoriteSearchMsgOfUser(kwGroup, searchTargetGroup);
    }

    @NonNull
    private CAppNetJsMsgTargetSettingData setGenerateFavoriteSearchMsgOfUser(ArrayList<CAppDbNewKeywordsGroup> kwGroup, ArrayList<CAppDbNewSearchTargetGroup> searchTargetGroup) {
        // transmission env
        String timestamp = CPfBaseTime.setConvertTimeStampToDefaultFormatV2();


        CAppNetJsMsgTargetSettingData msgdataS = new CAppNetJsMsgTargetSettingData();


        List<CAppNetJsMsgItemKeywordsGroup> mKeywordGroupData = new ArrayList<>();
        List<CAppNetJsMsgItemSearchTargetGroup> mSearchTargetGroupData = new ArrayList<>();

        for (CAppDbNewKeywordsGroup kwgItem: kwGroup) {
            CAppNetJsMsgItemKeywordsGroup memberkw = new CAppNetJsMsgItemKeywordsGroup(
                kwgItem.getKeywordGroupName(), kwgItem.getFullmatchKeywordsV1(),
                kwgItem.getPartmatchKeywordsV1(), timestamp);

            mKeywordGroupData.add(memberkw);
        }
        for (CAppDbNewSearchTargetGroup stgItem: searchTargetGroup) {
            CAppNetJsMsgItemSearchTargetGroup memberst = new CAppNetJsMsgItemSearchTargetGroup(
                stgItem.getSearchTargetGroupName(), stgItem.getSearchUrlTargetV1(),
                stgItem.getKeywordsGroupIdList(), timestamp);

            mSearchTargetGroupData.add(memberst);
        }
        msgdataS.setmKeywordGroupData(mKeywordGroupData);
        msgdataS.setmSearchTargetGroupData(mSearchTargetGroupData);
        return msgdataS;
    }

    private void setSaveResponse() {
        Gson gson = new Gson();
        String strObjcat = gson.toJson(mObjcat);
        CPfFileOperationInternal fo = new CPfFileOperationInternal();
        fo.writeFile("bufsearchresult", strObjcat, mContext);
    }

    private void setCreateTargetList(ArrayList<CAppDbNewSearchTargetGroup> searchTargetGroup) {
        // fetch target list to soup with
        CAppDbNewSearchTargetGroup stGroupAll = new CAppDbNewSearchTargetGroup();
        stGroupAll.mSearchTargetGroupName = "UserTarget";

        stGroupAll.mSearchUrlTarget = new ArrayList<>();
        for (CAppDbNewSearchTargetGroup stgroup: searchTargetGroup) {
            for (String url:stgroup.mSearchUrlTarget) {
                stGroupAll.mSearchUrlTarget.add(url);
            }
        }
        mTargetGroup.add(stGroupAll);
    }

    private void setBuildSimDataOfTargetGroup() {
        mTargetGroup = new ArrayList<>();
        CAppDbNewSearchTargetGroup member = new CAppDbNewSearchTargetGroup();
        member.mSearchTargetGroupName = "SimData";

        String urlobj = new String("https://en.wikipedia.org/wiki/Machine_learning");
        member.mSearchUrlTarget = new ArrayList<>();
        member.mSearchUrlTarget.add(urlobj);

        String targetlist[] = "machine,learning,classification,data,regression".split(",");
        member.mLinkedinKeywordsGroupId = new ArrayList<>();
        member.mLinkedinKeywordsGroupId = new ArrayList<String>(Arrays.asList(targetlist));
        mTargetGroup.add(member);
    }

    private void setParseServerSearchResults(String srvResp) {
        try {
            JSONObject jsonObject = new JSONObject(srvResp);
            String readtext = jsonObject.toString();

            Type type = new TypeToken<Map<String, CAppNetJsMsgRspMatchedSection>>(){}.getType();
            Map<String, CAppNetJsMsgRspMatchedSection> objcat = mGson.fromJson(srvResp, type);
            HashMap<String, String> dcUrlIconAddr = new HashMap<>();
            CPfWebAnalyzer webAnalyzer = new CPfWebAnalyzer();


            for(Map.Entry<String, CAppNetJsMsgRspMatchedSection> entry: objcat.entrySet()) {
                CAppNetJsMsgRspMatchedSection elemval = entry.getValue();
                String baseUrl = CPfWebUrl.getBaseUrl(elemval.getmUrl());
                elemval.setmBaseUrl(baseUrl);


                if (dcUrlIconAddr.containsKey(baseUrl)) {
                    elemval.setmIconUrl(dcUrlIconAddr.get(baseUrl));
                }
                else {
                    String curUrlLink = webAnalyzer.setFetchIconFromUrl(elemval.getmUrl());
                    if (curUrlLink.isEmpty()) {
                        //curUrlLink = "drawable-v24/search.png";
                        curUrlLink = "drawable://" + R.drawable.search;
                    }
                    //curUrlLink = "drawable://" + R.drawable.search;
                    dcUrlIconAddr.put(baseUrl, curUrlLink);
                    elemval.setmIconUrl(curUrlLink);
                }
            }



            if (mObjcat == null) {
                mObjcat = objcat;
            }
            else {
                mObjcat.putAll(objcat);
            }
            //Map<String, CAppNetJsMsgRspMatchedSection> objcatTwin = mGson.fromJson(strObjcat, type);

            Log.d("Parsing", "result");

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setTestCaseOnMsgBasics() {
        //getWebpageMethodV1(mUrl);


        Gson test = new Gson();

        getWebpageMethodV2ViaJsoup();

        if (mWebpageHtml.length() > 0) {
            Gson gson = new Gson();

            CAppNetJsMsgReq msgFinal = new CAppNetJsMsgReq("0.1", "ObjPage", (Object) mWebpageHtml, "testor");
            String res = gson.toJson(msgFinal);
            String size = Integer.toString(res.length());
            Log.d("msg len", size);

            CAppNetJsMsgReq msgPilot = new CAppNetJsMsgReq("0.1", "PilotSize", size, "testor");
            String strMsgPilot = gson.toJson(msgPilot);
            CAppSocketClient socketClient = new CAppSocketClient();

            socketClient.setSendMsgToServerNoWaiting(strMsgPilot);
            Log.d("1st msg len", Integer.toString(strMsgPilot.length()));

            socketClient.setSendMsgToServerNoWaiting(res);
            Log.d("2nd msg len", size);
            //socketClient.execute((Runnable) msg);

        }


        // send msg to server

        // get server feedback
    }

    private boolean getWebpageMethodV2ViaJsoup() {
        try {
            mWebpageHtml = "";

            org.jsoup.nodes.Document doc = Jsoup.connect(mUrl).get();

            String title = doc.title();

            Elements links = doc.select("a[href]");
            // links.get(1).attributes.toString()

            mBuilder.append(title).append("\n");

            for (org.jsoup.nodes.Element link: links) {
                mBuilder.append("\n").append("Link: ").append(link.attr("href")).append("\n").append("Text: ").append(links.text());
            }

            //mWebpageHtml = doc.body().text();
            mWebpageHtml = doc.html();

            //getKeyElementsAsReference(doc);

            //getImageViaJsoup(doc);

            //Log.d("jsoup", mBuilder.toString());
            
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    private boolean getWebpageMethodV3ViaJsoup(String url) {
        try {
            mWebpageHtml = "";
            org.jsoup.nodes.Document doc = Jsoup.connect(url).get();
            mWebpageHtml = doc.html();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    private void getKeyElementsAsReference(org.jsoup.nodes.Document doc) {
        String strBody = doc.body().text();

        org.jsoup.nodes.Element divTag = doc.getElementById("sample");
        String content = divTag.toString();

        String strHtml = doc.html();
    }

    private void getImageViaJsoup(org.jsoup.nodes.Document doc) throws IOException {
        // get Image
        org.jsoup.nodes.Element img = doc.select("img").first();
        String imgSrc = img.absUrl("src");
        InputStream intream = new URL(imgSrc).openStream();
        Bitmap bitmap = BitmapFactory.decodeStream(intream);
    }

    private boolean getWebpageMethodV1(String strUrl) {
        if (strUrl.length()==0)
            return false;

        try {
            URL url = new URL(strUrl);
            try {
                mBufWebContentInString = getReadInputStringBuffer(
                    setRetrieveUrlStream(url)).toString();
                return true;

            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return true;
    }

    private InputStream setRetrieveUrlStream(URL url) throws IOException {
        url.openConnection();

        return (InputStream)url.getContent();
    }

    @NonNull
    private StringBuffer getReadInputStringBuffer(InputStream res) throws IOException {
        StringBuffer sb = new StringBuffer();
        String line = null;
        BufferedReader br = new BufferedReader(new InputStreamReader(res));

        while(( line = br.readLine()) != null ) {
            sb.append(line);
        }
        return sb;
    }

    private void setInitMembers() {
        setInitSoupMembers();
    }

    private void setInitSoupMembers() {
        if(mDcAllWebPackage != null) {
            mDcAllWebPackage.clear();
            mDcAllWebPackage = null;
        }
        mDcAllWebPackage = new ArrayList<>();

        if(mGson == null) mGson = new Gson();

        mBufWebContentInString = "";
    }

    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////

    public String getmUrl() {
        return mUrl;
    }

    public void setmUrl(String mUrl) {
        this.mUrl = mUrl;
    }

    public void setmKwGroup(ArrayList<CAppDbNewKeywordsGroup> mKwGroup) {
        this.mKwGroup = mKwGroup;
    }

    public void setmTargetGroup(ArrayList<CAppDbNewSearchTargetGroup> mTargetGroup) {
        this.mTargetGroup = mTargetGroup;
    }

    public CAppSettingOfWebAction getmSetting() {
        return mSetting;
    }

    public void setmSetting(CAppSettingOfWebAction mSetting) {
        this.mSetting = mSetting;
    }

    public EnumStateMachineOfAppTransceiver getWorkFlow() {
        return mWorkFlow;
    }

    public void setWorkFlow(EnumStateMachineOfAppTransceiver mWorkFlow) {
        this.mWorkFlow = mWorkFlow;
    }

}



class CSearchRequestData {
    @SerializedName("targetset")
    ArrayList<CSearchTarget>                                lsTargetset;

    @SerializedName("interest")
    HashMap<String, CAppNetJsMsgItemKeywordsGroup>          interest;


    ///////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////

    public CSearchRequestData() {
    }

    public CSearchRequestData(ArrayList<CSearchTarget> lsTargetset, HashMap<String, CAppNetJsMsgItemKeywordsGroup> interest) {
        this.lsTargetset = lsTargetset;
        this.interest = interest;
    }

    public ArrayList<CSearchTarget> getLsTargetset() {
        return lsTargetset;
    }

    public void setLsTargetset(ArrayList<CSearchTarget> lsTargetset) {
        this.lsTargetset = lsTargetset;
    }

    public HashMap<String, CAppNetJsMsgItemKeywordsGroup> getInterest() {
        return interest;
    }

    public void setInterest(HashMap<String, CAppNetJsMsgItemKeywordsGroup> interest) {
        this.interest = interest;
    }
}


class CSearchTarget {
    @SerializedName("reqid")
    public String       reqid;
    @SerializedName("url")
    public String       url;
    @SerializedName("content")
    public String       content;
    @SerializedName("associatedInterest")
    public ArrayList<String>       associatedInterest;
    @SerializedName("timestamp")
    public String       timestamp;

    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////


    public CSearchTarget() {
        this.reqid = "";
    }

    public CSearchTarget(String reqid, String url, String content, ArrayList<String> associatedInterest, String timestamp) {
        this.reqid = reqid;
        this.url = url;
        this.content = content;
        this.associatedInterest = associatedInterest;
        this.timestamp = timestamp;
    }

    public String getReqid() {
        return reqid;
    }

    public void setReqid(String reqid) {
        this.reqid = reqid;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public ArrayList<String> getAssociatedInterest() {
        return associatedInterest;
    }

    public void setAssociatedInterest(ArrayList<String> associatedInterest) {
        this.associatedInterest = associatedInterest;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}




